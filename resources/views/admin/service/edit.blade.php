@extends('admin.layouts.default')
{{-- Content --}}
@section('content')
    {{-- Edit Blog Form --}}
    <form class="form-horizontal" method="post"
          action="@if(isset($service)){{ URL::to('admin/services/edit/'.$service->id) }}
	        @else{{ URL::to('admin/services/create') }}@endif"
          autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>

        <div
                class="form-group {{{ $errors->has('name') ? 'has-error' : '' }}}">
            <div class="col-md-12">
                <label class="control-label" for="title"> Tên dịch vụ</label> <input
                        class="form-control" type="text" name="name" id="title"
                        value="{{{ Input::old('name', isset($service) ? $service->name : null) }}}"/>
                {!!$errors->first('name', '<label class="control-label">:message</label>')!!}
            </div>
        </div>
        <div
                class="form-group {{{ $errors->has('title') ? 'has-error' : '' }}}">
            <div class="col-md-12">
                <label class="control-label" for="title"> Tiêu đề</label> <input
                        class="form-control" type="text" name="title" id="title"
                        value="{{{ Input::old('title', isset($service) ? $service->title : null) }}}"/>
                {!!$errors->first('title', '<label class="control-label">:message</label>')!!}
            </div>
        </div>
        <div
                class="form-group {{{ $errors->has('icon') ? 'has-error' : '' }}}">
            <div class="col-md-12">
                <label class="control-label" for="title"> Biểu tượng</label> <input
                        class="form-control" type="text" name="icon" id="icon"
                        value="{{{ Input::old('icon', isset($service) ? $service->icon : null) }}}"/>
                {!!$errors->first('icon', '<label class="control-label">:message</label>')!!}
            </div>
        </div>
        <div
                class="form-group {{{ $errors->has('order') ? 'has-error' : '' }}}">
            <div class="col-md-12">
                <label class="control-label" for="title"> Thứ tự</label>
                <input class="form-control" type="number" name="order" id="order"
                       value="{{{ Input::old('order', isset($service) ? $service->order : 0) }}}"/>
                {!!$errors->first('order', '<label class="control-label">:message</label>')!!}
            </div>
        </div>
        <div
                class="form-group {{{ $errors->has('description') ? 'has-error' : '' }}}">
            <div class="col-md-12">
                <label class="control-label" for="title"> Mô tả</label>
                <textarea class="form-control" name="description" id="description" rows="4">{{{ Input::old('description', isset($service) ? $service->description : "") }}}</textarea>
            </div>
        </div>
        <div
                class="form-group {{{ $errors->has('content') ? 'has-error' : '' }}}">
            <div class="col-md-12">
                <label class="control-label" for="content">Nội dung: </label>
						<textarea class="form-control full-width tinymce" name="content"
                                  value="content"
                                  rows="10">{{{ Input::old('content', isset($service) ? $service->content : null) }}}</textarea>
                {!! $errors->first('content', '<label class="control-label">:message</label>')
                !!}
            </div>
        </div>
        <div class="checkbox">
            <label>
                <input name="deactive" type="checkbox" value="1" @if(isset($service->deactive)){{($service->deactive)?"checked":""}}@endif > Ẩn
            </label>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <button type="reset" class="btn btn-sm btn-warning close_popup">
                    <span class="glyphicon glyphicon-ban-circle"></span> Hủy
                </button>
                <button type="reset" class="btn btn-sm btn-default">
                    <span class="glyphicon glyphicon-remove-circle"></span> Khôi phục
                </button>
                <button type="submit" class="btn btn-sm btn-success">
                    <span class="glyphicon glyphicon-ok-circle"></span>
                    @if	(isset($service))
                        Lưu
                    @else
                        Đăng
                    @endif
                </button>
            </div>
        </div>
    </form>
@stop
@section('core_plugin_scripts')
    <script src="/assets/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>
@stop