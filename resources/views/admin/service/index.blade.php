@extends('admin.layouts.default') {{-- Web site Title --}}
@section('title') Dịch vụ @parent @stop
@section('page_title') Dịch vụ @stop

@section('content')
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-shopping-cart"></i>Quản lý dịch vụ
                    </div>
                    <div class="actions">
                        <a href="/admin/services/create" class="btn default yellow-stripe">
                            <i class="fa fa-plus"></i>
								<span class="hidden-480">
								Thêm dịch vụ </span>
                        </a>

                    </div>                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                            <thead>
                            <tr role="row" class="heading">
                                <th width="300">Tên dịch vụ</th>
                                <th>Icon</th>
                                <th>Tiêu đề</th>
                                <th>Thứ tự</th>
                                <th width="50"><i class="fa fa-gear"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($services))
                                @foreach($services as $service)
                                    <tr>
                                        <td>{!! $service->name !!}</td>
                                        <td align="center"><i class="fa-2x {!! $service->icon !!}"></i></td>
                                        <td>{!! $service->title !!}</td>
                                        <td>{!! $service->order !!}</td>
                                        <td><a href="{{url("/admin/services/edit/$service->id")}}"
                                               class="btn btn-xs blue">Sửa</a></td>
                                    </tr>
                                @endforeach
                                @else
                                <tr><td colspan="5" align="center">Không có dịch vụ nào</td></tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE CONTENT-->
    <div class="clearfix">
    </div>
@stop
@section('page_level_scrips')
@stop
@section('scriptss')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#datatable_ajax").DataTable({
                serverSide: true,
                processing: true,
                oLanguage: {sUrl: "{{url("assets/global/plugins/datatables/plugins/i18n/Vietnamese.json")}}"},
                ajax: {
                    url: "{{URL::to('admin/news/data')}}",
                    method: "post"
                },
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    },],
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("title", "Click vào đây đê sửa");
                    $(nRow).on("click", function () {
                        location.href = '{{url("admin/news/edit")}}/' + aData[0];
                    });
                    return nRow;
                }
            });
        });
    </script>
@stop