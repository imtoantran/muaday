@extends('admin.layouts.default') {{-- Web site Title --}}
@section('title') Quản lý trang @parent @stop
@section('page_title') Cập nhật trang @stop

@section('content')
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-shopping-cart"></i>Danh sách Trang
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">

                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                            <thead>
                            <tr role="row" class="heading">
                                <th>Id</th>
                                <th>Tiêu đề</th>
                                <th>Nội dung</th>
                                <th>Người đăng</th>
                                <th>Cập nhật</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE CONTENT-->
    <div class="clearfix">
    </div>
@stop
@section('page_level_scrips')
    <script src="/assets/global/scripts/datatable.js"></script>
    <script src="/assets/admin/pages/scripts/table-ajax.js"></script>
@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
                $("#datatable_ajax").DataTable({
                serverSide:true,
                processing:true,
                oLanguage:{sUrl:"{{url("assets/global/plugins/datatables/plugins/i18n/Vietnamese.json")}}"},
                ajax:{
                    url:"{{URL::to('admin/page/data')}}",
                    method:"post",
                },
                "columnDefs": [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("title","Click vào đây đê sửa");
                    $(nRow).on("click",function(){
                        location.href = '{{url("admin/page/edit")}}/' + aData[0];
                    });
                    return nRow;
                }
            });
        });
    </script>
@stop
