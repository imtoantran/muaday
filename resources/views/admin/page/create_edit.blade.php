@extends('admin.layouts.default')
{{-- Content --}}
@section('content')
    {{-- Edit Blog Form --}}
    <form class="form-horizontal" enctype="multipart/form-data" method="post"
          action="@if(isset($page)){{ URL::to('admin/page/edit/'.$page->id) }}@endif"
          autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
        <!-- ./ csrf token -->

        <div
                class="form-group {{{ $errors->has('title') ? 'has-error' : '' }}}">
            <div class="col-md-12">
                <label class="control-label" for="title"> Tiêu đề</label> <input
                        class="form-control" type="text" name="title" id="title"
                        value="{{{ Input::old('title', isset($page) ? $page->title : null) }}}"/>
                {!!$errors->first('title', '<label class="control-label">:message</label>')!!}
            </div>
        </div>
        <div
                class="form-group {{{ $errors->has('excerpt') ? 'has-error' : '' }}}">
            <div class="col-md-12">
                <label class="control-label" for="excerpt">Mô tả tóm tắt: </label>
						<textarea class="form-control full-width"
                                  name="excerpt"
                                  rows="8">{{{ Input::old('excerpt', isset($page) ? $page->excerpt : null) }}}</textarea>
                {!! $errors->first('excerpt', '<label class="control-label">:message</label>')
                !!}
            </div>
        </div>
        <div
                class="form-group {{{ $errors->has('content') ? 'has-error' : '' }}}">
            <div class="col-md-12">
                <label class="control-label" for="content">Nội dung: </label>
						<textarea class="form-control full-width tinymce" name="content"
                                  value="content"
                                  rows="10">{{{ Input::old('content', isset($page) ? $page->content : null) }}}</textarea>
                {!! $errors->first('content', '<label class="control-label">:message</label>')
                !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <button type="reset" class="btn btn-sm btn-warning close_popup">
                    <span class="glyphicon glyphicon-ban-circle"></span> Hủy
                </button>
                <button type="reset" class="btn btn-sm btn-default">
                    <span class="glyphicon glyphicon-remove-circle"></span> Khôi phục
                </button>
                <button type="submit" class="btn btn-sm btn-success">
                    <span class="glyphicon glyphicon-ok-circle"></span>
                    @if	(isset($page))
                        Lưu
                    @else
                        Đăng
                    @endif
                </button>
            </div>
        </div>
    </form>
@stop
@section('core_plugin_scripts')
<script src="/assets/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>
@stop