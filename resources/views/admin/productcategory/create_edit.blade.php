@extends('admin.layouts.default')
@section('title')
    {{isset($category)?"Cập nhật sdanh mục:: $category->name":"Thêm danh mục"}}
@stop
@section('page_title')
    {{isset($category)?"Cập nhật danh muc:: $category->name":"Thêm danh mục"}}
@stop

@section('content')
    <form action="@if(isset($category)){{ URL::to('admin/products/category/edit/'.$category->id) }}
	        @else{{ URL::to('admin/products/category/create') }}@endif" method="POST" class="form-horizontal" role="form">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
        @if(isset($category))<input type="hidden" name="id" value="{{$category->id}}"/>@endif
        <!-- ./ csrf token -->
        <div class="form-group">
            <label for="name" class="col-sm-3 control-label">Tên: </label>

            <div class="col-sm-9">
                <input type="text" name="name" id="name" class="form-control" value="{{{ isset($category)?$category->name:'' }}}" title="Tên danh mục"
                       placeholder="Nhập tên danh mục nhé"
                       required="required">
            </div>
        </div>


        <div class="form-group">
            <label for="excerpt" class="col-sm-3 control-label">Mô tả: </label>

            <div class="col-sm-9">
                <textarea name="description" id="description" class="form-control" required="required"
                          placeholder="Nhập mô tả ngắn gọn thôi nhé">{{{ isset($category)?$category->description:'' }}}</textarea>
            </div>
        </div>



        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="button" class="btn default" data-dismiss="modal">Hủy</button>
                <button type="submit" class="btn blue">Lưu</button>
            </div>
        </div>


    </form>
@stop