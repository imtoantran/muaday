@extends('admin.layouts.default') {{-- Web site Title --}}
@section('title') Sản phẩm :: @parent @stop
@section('page_title') Sản phẩm @stop
@section('content')
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-shopping-cart"></i>Danh sách sản phẩm
                    </div>
                    <div class="actions">
                        <a href="{{URL::to('admin/products/create')}}" class="btn default yellow-stripe">
                            <i class="fa fa-plus"></i>
								<span class="hidden-480">
								Thêm sản phẩm </span>
                        </a>

                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                            <thead>
                            <tr role="row" class="heading">
                                <th>
                                    #
                                </th>
                                <th >
                                    Tên
                                </th>
                                <th>
                                    Danh mục
                                </th>

                                <th>
                                    Giá
                                </th>
                                <th >
                                    Ngày đăng
                                </th>
                                <th><i class="fa fa-image"></i></th>
                                <th>
                                    <i class="fa fa-gear"></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE CONTENT-->
    <div class="clearfix">
    </div>
@stop
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#datatable_ajax").DataTable({
                serverSide:true,
                processing:true,
                oLanguage:{sUrl:"{{url("assets/global/plugins/datatables/plugins/i18n/Vietnamese.json")}}"},
                ajax:{
                    url:"{{URL::to('admin/products/data')}}",
                    method:"post",
                }
            });
        });
    </script>
@stop
