@extends('admin.layouts.default')
@section('title')
{{isset($product)?"$product->name":"Thêm sản phẩm"}}
@stop
@section('page_title')
{{isset($product)?"Cập nhật sản phẩm:: $product->name":"Thêm sản phẩm"}}
@stop
@section('content')
<form action="@if(isset($product)){{ URL::to('admin/products/edit/'.$product->id) }}
   @else{{ URL::to('admin/products/create') }}@endif" method="POST" id="product_form" class="form-horizontal"
   role="form">
   <!-- CSRF Token -->
   <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
   <!-- ./ csrf token -->
   <div class="form-group">
    <label for="name" class="col-sm-3 control-label">Tên: </label>

    <div class="col-sm-9">
        <input type="text" name="name" id="name" class="form-control"
        value="{{isset($product)?$product->name:''}}" title="Tên sản phẩm"
        placeholder="Nhập tên sản phẩm nhé"
        required="required">
    </div>
</div>
<div class="form-group">
    <label for="name" class="col-sm-3 control-label">Mã sản phẩm: </label>

    <div class="col-sm-9">
        <input type="text" name="sku" id="name" class="form-control"
        value="{{isset($product)?$product->sku:''}}" title="Mã sản phẩm"
        placeholder="Nhập mã sản phẩm nhé"
        required="required">
    </div>
</div>
<div class="form-group">
    <label for="price" class="col-sm-3 control-label">Giá: </label>

    <div class="col-sm-9">
        <input type="number" name="price" id="price" class="form-control"
        value="{{isset($product)?$product->price:''}}" title="Giá sản phẩm"
        placeholder="Chỉ nhập số thôi nhé"
        required="required">
    </div>
</div>
<div class="form-group">
    <label for="price" class="col-sm-3 control-label">Giá khuyến mãi: </label>

    <div class="col-sm-9">
        <input type="number" name="sale_price" id="sale_price" class="form-control"
        value="{{isset($product)?$product->sale_price:''}}" title="Giá khuyến mãi sản phẩm"
        placeholder="Chỉ nhập số thôi nhé">
    </div>
</div>
<div class="form-group">
    <label for="name" class="col-sm-3 control-label">Danh mục:</label>

    <div class="col-sm-9">
        <select name="category" id="category" class="form-control">
            <option value=""> -- Chọn dang mục sản phẩm --</option>
            @foreach($categories as $category)
            <option @if(isset($product)&&$product->product_category_id == $category->id) selected
               @endif value="{{$category->id}}">{{$category->name}}</option>
               @endforeach
           </select>
       </div>
   </div>
   <div class="form-group">
    <label for="excerpt" class="col-sm-3 control-label">Mô tả ngắn: </label>

    <div class="col-sm-9">
        <textarea name="excerpt" id="excerpt" class="form-control" required="required" rows="7"
        placeholder="Nhập mô tả ngắn gọn thôi nhé">{{isset($product)?$product->excerpt:''}}</textarea>
    </div>
</div>
<div class="form-group">
    <label for="description" class="col-sm-3 control-label">Mô tả đầy đủ: </label>

    <div class="col-sm-9">
        <textarea name="description" id="description" class="form-control tinymce" rows="12" 
        placeholder="Nhập mô tả đầy đủ nhé">{{isset($product)?$product->description:''}}</textarea>
    </div>
</div>
<div class="form-group">

    <div class="col-sm-9">
        <div class="product-image" style="display: flex">
            @if(isset($product)&&count($product->image))
            @foreach($product->image as $image)
            <input class="image-item" type="hidden" name="image[]" value="{{$image}}"/>
            @endforeach
            @endif

        </div>
    </div>
</div>
<div class="form-group">
    <label for="images" class="col-sm-3 control-label">Hình ảnh: </label>
    <div class='col-sm-9'>
        <div id="fileupload">
            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
            <!-- The table listing the files available for upload/download -->
            <div role="presentation" class="">
                <div class="files row">
                    @if(isset($product)&&count($product->image))
                    @foreach($product->image as $image)                            
                    <div class="template-download col-xs-6 col-sm-4 col-md-3 fade in">
                        <div class="upload-image-wrapper thumbnail">
                            <div class="upload-preview">
                                <span class="preview">                
                                    <a href="{!! url("/img/products/$image") !!}" title="{{$image}}" download="{{$image}}" data-gallery="">
                                        <img src="{!! url("/img/products/thumbnail/$image") !!}">
                                    </a>

                                </span>
                            </div>
                            <div class="name hidden">{{$image}}</div>

                            <div class="upload-control">

                                <button class="btn btn-xs btn-danger delete" data-type="DELETE" data-url="{{url("admin/products/upload?file=$image")}}">
                                    <i class="glyphicon glyphicon-trash"></i> Xóa
                                </button>
                                <input name="delete" value="1" class="toggle" type="checkbox">

                            </div>
                        </div>
                    </div>         
                    @endforeach
                    @endif                        

                </div></div>
                <div class="row fileupload-buttonbar">
                    <!-- The global progress information -->
                    <div class="col-lg-12 fileupload-progress fade">
                        <!-- The global progress bar -->
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar progress-bar-success" style="width:0%;">
                            </div>
                        </div>
                        <!-- The extended global progress information -->
                        <div class="progress-extended">
                           &nbsp;
                       </div>
                   </div>
                   <div class="col-lg-12">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-sm green fileinput-button">
                        <i class="fa fa-plus"></i>
                        <label for="file" class="label">Thêm... </label>
                        <input type="file" id="file" name="files[]" multiple="" class="hidden"></span>
                        <button type="submit" class="btn btn-sm blue start">
                            <i class="fa fa-upload"></i>
                            <span>Up hình </span>
                        </button>
                        <button type="reset" class="btn btn-sm warning cancel">
                            <i class="fa fa-ban-circle"></i>
                            <span>Hủy </span>
                        </button>
                        <button type="button" class="btn btn-sm red delete">
                            <i class="fa fa-trash"></i>
                            <span>Xóa </span>
                        </button>
                        <input type="checkbox" class="toggle">
                        <!-- The global file processing state -->
                        <span class="fileupload-process">
                        </span>
                    </div>
                </div>
            </div>                    

        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9 text-right">
            <button type="button" class="btn red" data-dismiss="modal">Hủy</button>
            <button type="submit" class="btn blue">Lưu</button>
        </div>
    </div>
</form>
@stop
@section('core_plugin_scripts')
<script src="/assets/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>
@stop
@section('page_level_script_plugins')
<script src="/assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js" type="text/javascript"></script>
<!-- The File Upload processing plugin -->
<script src="/assets/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="/assets/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<!--<script src="js/jquery.fileupload-audio.js"></script>-->
<!-- The File Upload video preview plugin -->
<!--<script src="js/jquery.fileupload-video.js"></script>-->
<!-- The File Upload validation plugin -->
<script src="/assets/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/assets/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js"></script>

@stop
@section('scripts')
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="template-upload col-xs-6 col-sm-4 col-md-3 fade" title="{%=file.name%}">
        <div class="upload-image-wrapper thumbnail">
            <div class="upload-preview">
                <span class="preview"></span>
            </div>
            <div class="upload-control">
                {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-xs btn-primary start" disabled>
                    <i class="fa fa-upload"></i>
                    <span>Up</span>
                </button>
                {% } %}
                {% if (!i) { %}
                <button class="btn btn-xs btn-danger cancel">
                    <i class="fa fa-times"></i> Xóa
                </button>
                {% } %}
            </div>
        </div>
    </div>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="template-download col-xs-6 col-sm-4 col-md-3 fade">
        <div class="upload-image-wrapper thumbnail">
            <div class="upload-preview">
                <span class="preview">
                    {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                    {% } %}
                </span>
            </div>
            <div class="name hidden">
                {% if (file.url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                <span>{%=file.name%}</span>
                {% } %}
            </div>
            {% if (file.error) { %}
            <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
            <div class="upload-control">
                {% if (file.deleteUrl) { %}
                <button class="btn btn-xs btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i> Xóa
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
                {% } else { %}
                <button class="btn btn-xs btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i> Xóa
                </button>
                {% } %}
            </div>
        </div>
    </div>
    {% } %}
</script>
<script>
    $(document).ready(function ($) {
            // jquery validate
            var handleValidation = function () {
                // for more info visit the official plugin documentation:
                // http://docs.jquery.com/Plugins/Validation

                var form3 = $('#product_form');
                var error3 = $('.alert-danger', form3);
                var success3 = $('.alert-success', form3);

                //IMPORTANT: update CKEDITOR textarea with actual content before submit
                form3.on('submit', function () {
                    tinyMCE.triggerSave();
                })

                form3.validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "", // validate all fields including form hidden input
                    rules: {
                        name: {
                            minlength: 2,
                            required: true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        options1: {
                            required: true
                        },
                        options2: {
                            required: true
                        },
                        select2tags: {
                            required: true
                        },
                        datepicker: {
                            required: true
                        },
                        occupation: {
                            minlength: 5
                        },
                        membership: {
                            required: true
                        },
                        service: {
                            required: true,
                            minlength: 2
                        },
                        markdown: {
                            required: true
                        },
                        editor1: {
                            required: true
                        },
                        editor2: {
                            required: true
                        }
                    },

                    messages: { // custom messages for radio buttons and checkboxes
                        membership: {
                            required: "Please select a Membership type"
                        },
                        service: {
                            required: "Please select  at least 2 types of Service",
                            minlength: jQuery.validator.format("Please select  at least {0} types of Service")
                        }
                    },

                    errorPlacement: function (error, element) { // render error placement for each input type
                        if (element.parent(".input-group").size() > 0) {
                            error.insertAfter(element.parent(".input-group"));
                        } else if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else if (element.parents('.radio-list').size() > 0) {
                            error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                        } else if (element.parents('.radio-inline').size() > 0) {
                            error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                        } else if (element.parents('.checkbox-list').size() > 0) {
                            error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                        } else if (element.parents('.checkbox-inline').size() > 0) {
                            error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                        } else {
                            error.insertAfter(element); // for other inputs, just perform default behavior
                        }
                    },

                    invalidHandler: function (event, validator) { //display error alert on form submit
                        success3.hide();
                        error3.show();
                        Metronic.scrollTo(error3, -200);
                    },

                    highlight: function (element) { // hightlight error inputs
                        $(element)
                                .closest('.form-group').addClass('has-error'); // set error class to the control group
                            },

                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element)
                                .closest('.form-group').removeClass('has-error'); // set error class to the control group
                            },

                            success: function (label) {
                                label
                                .closest('.form-group').removeClass('has-error'); // set success class to the control group
                            },

                            submitHandler: function (form) {
                                success3.show();
                                error3.hide();
                        form[0].submit(); // submit the form
                    }

                });

                //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
                $('.select2me', form3).change(function () {
                    form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                });

                // initialize select2 tags
                $("#select2_tags").change(function () {
                    form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                }).select2({
                    tags: ["red", "green", "blue", "yellow", "pink"]
                });

                //initialize datepicker
                $('.date-picker').datepicker({
                    rtl: Metronic.isRTL(),
                    autoclose: true
                });
                $('.date-picker .form-control').change(function () {
                    form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
                })
            }
            handleValidation;
            // file upload
            $(".product-image").on('click', '.delete', function (d) {
//                $(this).closest('div').remove();
});
            $("#product_form").on('submit', function () {
                $(".image-item").remove();

                $(".template-download .name").each(function (i, e) {
                    console.log($(e).text().trim());
                    input = $("<input />", {
                        name: "image[]",
                        value: $(e).text().trim(),
                        type: "hidden",
                        class: "image-item"
                    });
                    $(".product-image").prepend(input);
                });
                return true;
            });
            $("#fileupload").fileupload({
                url: "/admin/products/upload"
            })
        });
    </script>
    @stop
