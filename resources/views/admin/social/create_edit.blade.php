@extends('admin.layouts.default')
@section('title')
    Mạng xã hội
@stop
@section('content')

    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="contract_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form id="contract-form" action="" method="POST" role="form">
                        <input type="hidden" name="id">
                        <input type="hidden" name="facebook_id" value="<?php echo $user->facebook_id; ?>">
                        <input type="hidden" name="user_id" value="<?php echo $user->id ?>">

                        <div class="form-group">
                            <label for="">Ngày ký</label>
                            <input type="text" data-date-format="dd/mm/yyyy" aria-required="true"
                                   class="form-control date-picker" name="start_date" placeholder="Ngày ký">
                        </div>
                        <div class="form-group">
                            <label for="">Ngày kết thúc</label>
                            <input type="text" data-date-format="dd/mm/yyyy" aria-required="true"
                                   class="form-control date-picker" name="end_date" placeholder="Ngày kết thúc">
                        </div>
                        <div class="form-group">
                            <label for="">Mã hợp đồng</label>
                            <input type="text" class="form-control" name="contract_no" placeholder="Mã số hợp đồng">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn blue submit" data-user-id="<?php echo $user->id ?>"></button>
                    <button type="button" class="btn default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="portlet box blue">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <button id="add_contract" class="btn red btn-xs">
                                    Thêm mới <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-hover table-bordered" id="contract-list">
                    <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Ngày ký
                        </th>
                        <th>
                            Ngày kết thúc
                        </th>
                        <th>
                            Hợp đồng số
                        </th>
                        <th>

                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contracts as $contract)
                        <tr data-id="<?php echo '';// $contract->id ?>">
                            <td colspan="" rowspan="" headers="" width="5"></td>
                            <td colspan="" rowspan="" headers=""><?php echo '';//$contract->start_date; ?></td>
                            <td colspan="" rowspan="" headers=""><?php echo '';//$contract->end_date; ?></td>
                            <td colspan="" rowspan="" headers=""><?php echo '';//$contract->contract_no; ?></td>
                            <td colspan="" rowspan="" headers="" width="10">
                                <button type="button" class="btn btn-xs btn-danger delete"></i> Xóa</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        var TableEditable = function () {

            var handleTable = function () {

                function restoreRow(oTable, nRow) {
                    var aData = oTable.fnGetData(nRow);
                    var jqTds = $('>td', nRow);

                    for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                        oTable.fnUpdate(aData[i], nRow, i, false);
                    }

                    oTable.fnDraw();
                }

                function editRow(oTable, nRow) {
                    var aData = oTable.fnGetData(nRow);
                    var jqTds = $('>td', nRow);
                    jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                    jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                    jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
                    jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
                    jqTds[4].innerHTML = '<button class="btn btn-primary btn-xs edit" >Lưu</button>';
                }

                function saveRow(oTable, nRow) {
                    var jqInputs = $('input', nRow);
                    oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                    oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                    oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                    oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                    oTable.fnUpdate('<button class="btn btn-primary btn-xs edit" >Sửa</button>', nRow, 4, false);
                    oTable.fnUpdate('<button class="btn btn-danger btn-xs delete" >Xóa</button>', nRow, 5, false);
                    oTable.fnDraw();
                }

                function cancelEditRow(oTable, nRow) {
                    var jqInputs = $('input', nRow);
                    oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                    oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                    oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                    oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                    oTable.fnUpdate('<button class="btn btn-primary btn-xs edit" >Sửa</button>', nRow, 4, false);
                    oTable.fnDraw();
                }

                var table = $('#contract-list');
                var contract_modal = $("#contract_modal");
                var oTable = table.dataTable({

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
                    // So when dropdowns used the scrollable div should be removed.
                    //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                    "lengthMenu": [
                        [5, 15, 20, -1],
                        [5, 15, 20, "Tất cả"] // change per page values here
                    ],
                    // set the initial value
                    "pageLength": 10,

                    "language": {
                        "lengthMenu": " _MENU_ dòng"
                    },
                    "columnDefs": [{ // set default column settings
                        'orderable': true,
                        'targets': [0]
                    }, {
                        "searchable": true,
                        "targets": [0]
                    }],
                    "order": [
                        [0, "asc"]
                    ] // set first column as a default sort by asc
                });

                var tableWrapper = $("#contract-list_wrapper");


                var nEditing = null;
                var nNew = false;

                $('#add_contract').click(function (e) {
                    e.preventDefault();
                    $("#contract_modal .modal-title").html("Thêm hợp đồng");
                    $("#contract_modal .submit").html("Thêm");
                    $("#contract-form [name=id]").val('');
                    $("#contract-form [name=start_date]").val('');
                    $("#contract-form [name=end_date]").val('');
                    $("#contract-form [name=contract_no]").val('');
                    $("#contract_modal").modal();
                });
                $("#contract_modal").on('click', '.submit', function (event) {
                    event.preventDefault();
                    /* Act on the event */
                    if ($("[name=start_date]").val() == "") {
                        alert("Chưa chọn ngày ký");
                        $("[name=start_date]").focus();
                        return;
                    }
                    if ($("[name=end_date]").val() == "") {
                        $("[name=end_date]").focus();
                        alert("Chưa chọn ngày kết thúc hợp đồng");
                        return;
                    }
                    var data = $("#contract-form").serializeArray();
                    $.ajax({
                        url: '/admin/contract/edit',
                        type: 'POST',
                        dataType: 'JSON',
                        data: data,
                    })
                            .done(function (response) {
                                if (response.success) {
                                    contract_modal.modal("hide");
                                    //editRow(oTable, nRow);
                                    if (response.action == "add") {
                                        var aiNew = oTable.fnAddData(['', $("#contract-form [name=start_date]").val(), $("#contract-form [name=end_date]").val(), $("#contract-form [name=contract_no]").val(), '<button class="btn btn-xs btn-danger delete">Xóa</button>']);
                                        var nRow = oTable.fnGetNodes(aiNew[0]);
                                        $(nRow).data("id", response.id);
                                        nEditing = nRow;
                                        nNew = false;
                                        saveRow(oTable, nEditing);
                                    }
                                }
                                alert(response.message);
                            })
                            .fail(function (response) {
                                alert("Đã xảy ra lỗi")
                            })
                });
                table.on('click', 'tbody tr', function (e) {
                    e.preventDefault();
                    $("#contract_modal .submit").html("Lưu");
                    $("#contract-form [name=id]").val($(this).data("id"));
                    $("#contract-form [name=start_date]").val($(this).find("td").eq(1).text());
                    $("#contract-form [name=end_date]").val($(this).find("td").eq(2).text());
                    $("#contract-form [name=contract_no]").val($(this).find("td").eq(3).text());
                    $("#contract_modal").modal();
                });
                table.on('click', '.delete', function (e) {
                    e.preventDefault();
                    e.stopPropagation();

                    if (confirm("Chắc chắn xóa ?") == false) {
                        return;
                    }
                    var nRow = $(this).parents('tr')[0];
                    $.ajax({
                        url: '/admin/contract/delete',
                        type: 'post',
                        dataType: 'json',
                        data: {id: $(this).closest('tr').data("id")},
                    })
                            .done(function (response) {
                                if (response.success) {
                                    alert(response.message);
                                    oTable.fnDeleteRow(nRow);
                                }
                            })
                            .fail(function () {
                                alert("Có lỗi xảy ra");
                            })
                });

                table.on('click', '.cancel', function (e) {
                    e.preventDefault();
                    if (nNew) {
                        oTable.fnDeleteRow(nEditing);
                        nEditing = null;
                        nNew = false;
                    } else {
                        restoreRow(oTable, nEditing);
                        nEditing = null;
                    }
                });

                table.on('click', '.edit', function (e) {
                    e.preventDefault();

                    /* Get the row as a parent of the link that was clicked on */
                    var nRow = $(this).parents('tr')[0];

                    if (nEditing !== null && nEditing != nRow) {
                        /* Currently editing - but not this row - restore the old before continuing to edit mode */
                        restoreRow(oTable, nEditing);
                        editRow(oTable, nRow);
                        nEditing = nRow;
                    } else if (nEditing == nRow && this.innerHTML == "Lưu") {
                        /* Editing this row and want to save it */
                        saveRow(oTable, nEditing);
                        nEditing = null;
                        alert("Updated! Do not forget to do some ajax to sync with backend :)");
                    } else {
                        /* No edit in progress - let's start one */
                        editRow(oTable, nRow);
                        nEditing = nRow;
                    }
                });
            }

            return {

                //main function to initiate the module
                init: function () {
                    handleTable();
                }

            };

        }();
        TableEditable.init();
    </script>
@stop