@extends('admin.layouts.default') {{-- Web site Title --}}
@section('title') Facebook thành viên @parent @stop
@section('page_title') Facebook @stop

@section('content')
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-shopping-cart"></i>Danh sách facebook
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">

                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax">
                            <thead>
                            <tr role="row" class="heading">

                                <th>
                                    Tên
                                </th>
                                <th>
                                    Email
                                </th>


                                <th>
                                    Tham gia
                                </th>
                                <th>
                                    HĐ
                                </th>
                                <th>
                                    
                                </th>
                            </tr>
                            <tr role="row" class="filter">


                                <td>
                                    <input type="text" class="form-control form-filter input-sm"
                                           name="name">
                                </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="email">
                                </td>
                                <td></td>
                                <td>
                                    {{--<select name="contracted" class="form-control form-filter">--}}
                                        {{--<option value="all">Tất cả</option>--}}
                                        {{--<option value="contracted">Đa ký</option>--}}
                                        {{--<option value="uncontracted">Chưa ký</option>--}}
                                    {{--</select>--}}
                                </td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE CONTENT-->
    <div class="clearfix">
    </div>
@stop
@section('page_level_scrips')
    <script src="../../assets/global/scripts/datatable.js"></script>
    <script src="../../assets/admin/pages/scripts/table-ajax.js"></script>
@stop

@section('scripts')
    <script type="text/javascript">
        var oTable;
        $(document).ready(function () {
            TableAjax.init('{{URL::to('admin/social/data')}}');
        });
    </script>
@stop
