@extends('admin.layouts.default')
@section('title')
    Mạng xã hội
@stop
@section('modal')
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="modal fade" id="contract_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <form id="contract-form" action="" method="POST" role="form">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="id">
                <input type="hidden" name="social_id" value="{{$social->social_id}}">

                <div class="form-group">
                    <label for="">Ngày ký</label>
                    <input type="text" data-date-format="dd-mm-yyyy" aria-required="true"
                           class="form-control date-picker" name="start_date" placeholder="Ngày ký">
                </div>
                <div class="form-group">
                    <label for="">Ngày kết thúc</label>
                    <input type="text" data-date-format="dd-mm-yyyy" aria-required="true"
                           class="form-control date-picker" name="end_date" placeholder="Ngày kết thúc">
                </div>
                <div class="form-group">
                    <label for="">Số lượng</label>
                    <input type="number" class="form-control" name="contract_no" placeholder="Số lượng hợp đồng">
                </div>

            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn blue submit"></button>
            <button type="button" class="btn default" data-dismiss="modal">Đóng</button>
        </div>

    </div>
    <!-- /.modal -->
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
@stop
@section('content')

    <div class="table-toolbar">
        <div class="row">
            <div class="col-md-6">
                <div class="btn-group">
                    <button id="add_contract" class="btn red btn-danger">
                        <i class="fa fa-plus"></i> Thêm hợp đồng
                    </button>
                    <a href="{!! url("admin/social/history/$id") !!}" class="btn yellow"><i class="fa fa-list-alt"></i> Xem lịch sử chia sẻ</a>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-striped table-hover table-bordered" id="contract-list">
        <thead>
        <tr>
            <th>
                #
            </th>
            <th>
                Ngày ký
            </th>
            <th>
                Ngày kết thúc
            </th>
            <th>
                Số lượng
            </th>
            <th>

            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($contracts as $key=>$contract)
            @include('partials.admin.contract.item')
        @endforeach
        </tbody>
    </table>
@stop
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function ($) {
            var currentrow;
            var  contract_modal = $("#contract_modal");
            var table = $("#contract-list")
            $('#add_contract').click(function (e) {
                e.preventDefault();
                $("#contract_modal .modal-title").html("Thêm hợp đồng");
                $("#contract_modal .submit").html("Thêm");
                $("#contract-form [name=id]").val('');
                $("#contract-form [name=start_date]").val('');
                $("#contract-form [name=end_date]").val('');
                $("#contract-form [name=contract_no]").val('');
                $("#contract_modal").modal();
            });
            contract_modal.on('click', '.submit', function (event) {
                event.preventDefault();
                /* Act on the event */
                if ($("[name=start_date]").val() == "") {
                    alert("Chưa chọn ngày ký");
                    $("[name=start_date]").focus();
                    return;
                }
                if ($("[name=end_date]").val() == "") {
                    alert("Chưa chọn ngày kết thúc hợp đồng");
                    $("[name=end_date]").focus();
                    return;
                }
                var data = $("#contract-form").serializeArray();
                $.ajax({
                    url: '/admin/contract/edit',
                    type: 'POST',
                    dataType: 'JSON',
                    data: data
                }).done(function (response) {
                    if (response.success) {
                        contract_modal.modal("hide");
                        if (response.action == "add") {
                            table.find("tbody").append(response.contract);
                        }else{
                            currentrow.replaceWith(response.contract);
                        }
                    }
                    alert(response.message);
                }).fail(function (response) {
                    alert("Đã xảy ra lỗi")
                })
            });
            table.on('click', 'tbody tr', function (e) {
                currentrow = $(this);
                e.preventDefault();
                $("#contract_modal .submit").html("Lưu");
                $("#contract-form [name=id]").val($(this).data("id"));
                $("#contract-form [name=start_date]").val($(this).find("td").eq(1).text());
                $("#contract-form [name=end_date]").val($(this).find("td").eq(2).text());
                $("#contract-form [name=contract_no]").val($(this).find("td").eq(3).text());
                $("#contract_modal").modal();
            });
            table.on('click', '.delete', function (e) {
                e.preventDefault();
                e.stopPropagation();

                if (confirm("Chắc chắn xóa ?") == false) {
                    return;
                }
                var id = $(this).closest('tr').data("id");
                $.ajax({
                    url: '/admin/contract/delete/'+ id,
                    type: 'post',
                    dataType: 'json'
                }).done(function (response) {
                    if (response.success) {
                        alert(response.message);
                        table.find("[data-id="+id+"]").fadeOut("slow");
                    }
                }).fail(function () {
                    alert("Có lỗi xảy ra");
                })
            });
        });

    </script>
@stop