@extends('admin.layouts.default')
@section('title')
    Lịch sử chia sẻ
@stop
@section('page_title')
    Lịch sử chia sẻ:: <span class="font-red"><b>{{"$social->first_name $social->last_name"}}</b></span>
@stop
@section('content')
    <form action="" method="get" class="form-horizontal col-xs-12" role="form">
        <div class="form-group no-gutter">
            <div class="col-xs-3 col-sm-2 col-md-1">
                <select class="form-control" name="month">
                    @for($i=1;$i<13;$i++)
                        <option value="{{$i}}" {{($i==$month)?"selected":""}}>Th {{$i}}</option>
                    @endfor
                </select>
            </div>
            <div class="col-xs-3 col-sm-2 col-md-1">
                <select class="form-control" name="year">
                    @for($i=2015;$i<2019;$i++)
                        <option value="{{$i}}" {{($i==$year)?"selected":""}}>{{$i}}</option>
                    @endfor
                </select>
            </div>
            <div class="col-xs-6">
                <button type="submit" class="btn red">Xem</button><a href="{!! url("admin/social/edit/$id") !!}" class="btn yellow">Hợp đồng</a>
            </div>
        </div>
    </form>
    @if(count($logs))
        <table class="table table-striped table-bordered table-advance table-hover">
            <thead>
            <tr>
                <th class="text-center font-red"><i class="fa fa-calendar"></i></th>
                <th class="text-center font-red"><i class="fa fa-share-alt"></i> <span class="hidden-xs">Chia sẻ</span></th>
                <th class="text-center font-red"><b>HĐ</b></th>
                <th class="font-red"><b>Thu nhập</b></th>
            </tr>
            </thead>
            <tbody>
            @foreach($logs as $log)
                <tr>
                    <td class="text-center" width="50"> {{$log->created_at->format("d/m")}}</td>
                    <td class="text-center" width="90"> {{$log->count or 0}}</td>
                    <td class="text-center" width="50"> {{$log->contract}}</td>
                    <td width="*"> <span class="label label-success"><b>{{number_format($log->contract*8333)}} đ</b></span></td>
                </tr>
            @endforeach
            <tfoot>
            <tr>
                <td colspan="3" class="text-right font-red"><b>Thu nhập tháng ({{$month}}): </b></td>
                <td class="font-red"><span class="label label-danger"><b>{{number_format($profit)}} đ</b></span></td>
            </tr>
            </tfoot>
            </tbody>
        </table>
    @endif
@stop