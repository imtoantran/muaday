@extends('admin.layouts.default') {{-- Web site Title --}}
@section('title') {{{ $title }}} :: @parent @stop {{-- Content --}}
@section('content')
    <div class="page-header">
        <h2>{{$title}}</h2>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="share-list">
                @foreach($data['posts'] as $post)
                    <li>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <div class="post-media"
                                 @if(isset($post['full_picture']))style="background-image:url('{{$post['full_picture']}}');"@endif>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                            <div class="post-info">
                                @if(isset($post['caption']))
                                    <div>{{$post['caption']}}</div>
                                @endif
                                @if(isset($post['message']))
                                    <h3>{{Str::words($post['message'],10)}}</h3>
                                @endif
                                @if(isset($post['name']))
                                    <div>{{$post['name']}}</div>
                                @endif

                                @if(isset($post['description']))
                                    <div>{{$post['description']}}</div>
                                @endif

                                {{--@if(isset($post['link']))<div>{{$post['link']}}</div>@endif--}}

                                {{--<div>@if(isset($post['icon']))<img src="{{$post['icon']}}">@endif</div>--}}
                                <form action="" method="post" class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-retweet"></i> Chia sẻ
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@stop
