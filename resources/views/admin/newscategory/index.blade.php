@extends('admin.layouts.default') {{-- Web site Title --}}
@section('title') Danh mục bài viết @parent @stop
@section('page_title') Danh mục bài viết @stop

@section('content')
	<!-- BEGIN PAGE CONTENT-->
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i>Danh mục bài viết
					</div>
					<div class="actions">
						<a href="{{URL::to('admin/news/category/create')}}" class="btn default yellow-stripe">
							<i class="fa fa-plus"></i>
								<span class="hidden-480">
								Thêm danh mục </span>
						</a>

					</div>
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<div class="table-actions-wrapper">
									<span>
									</span>
							<select class="table-group-action-input form-control input-inline input-small input-sm">
								<option value="">Chọn...</option>
							</select>
							<button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i>
								Lọc
							</button>
						</div>
						<table class="table table-striped table-bordered table-hover" id="datatable_ajax">
							<thead>
							<tr role="row" class="heading">
								<th>Id</th>
								<th>Tên</th>
								<th>Người đăng</th>
								<th>Ngày đăng</th>
								<th width="100"><i class="fa fa-gear"></i></th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- End: life time stats -->
		</div>
	</div>
	<!-- END PAGE CONTENT-->
	<div class="clearfix">
	</div>
@stop
@section('page_level_scrips')
	<script src="../../assets/global/scripts/datatable.js"></script>
	<script src="../../assets/admin/pages/scripts/table-ajax.js"></script>
@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#datatable_ajax").DataTable({
                serverSide:true,
                processing:true,
                oLanguage:{sUrl:"{{url("assets/global/plugins/datatables/plugins/i18n/Vietnamese.json")}}"},
                ajax:{
                    url:"{{URL::to('admin/news/category/data')}}",
                    method:"post",
                },
                "columnDefs": [
	            {
	                "targets": [0],
	                "visible": false,
	                "searchable": false
	            },],
			    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			    	$(nRow).attr("title","Click vào đây đê sửa");
			      	$(nRow).on("click",function(){
			      		location.href = '{{url("admin/news/category/edit")}}/' + aData[0];
			      	});
			      	return nRow;
			    }
            });
        });
    </script>
@stop