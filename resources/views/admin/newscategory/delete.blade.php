@extends('admin.layouts.default') {{-- Content --}} @section('content')

{{-- Delete Post Form --}}
<form id="deleteForm" class="form-horizontal" method="post"
	action="@if (isset($newscategory)){{ URL::to('admin/newscategory/' . $newscategory->id . '/delete') }}@endif"
	autocomplete="off">

	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> <input
		type="hidden" name="id" value="{{ $newscategory->id }}" />
	<!-- <input type="hidden" name="_method" value="DELETE" /> -->
	<!-- ./ csrf token -->

	<!-- Form Actions -->
	<div class="col-xs-12">			
	<div class="form-group">
		<div class="controls">
			<h4>Danh mục: {{$newscategory->title}}</h4>
			{{ Lang::get("admin/modal.delete_message") }}<br>
			<a class="btn btn-warning btn-sm close_popup" href="{{URL::to('admin/news/category')}}">
				<span class="glyphicon glyphicon-ban-circle"></span> {{Lang::get("admin/modal.cancel") }}
			</a>
			<button type="submit" class="btn btn-sm btn-danger">
				<span class="glyphicon glyphicon-trash"></span> {{
				Lang::get("admin/modal.delete") }}
			</button>
		</div>
	</div>
	</div>
	<!-- ./ form actions -->
</form>
@stop
