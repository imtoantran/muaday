@extends('admin.layouts.default') 
{{-- Content --}} 
@section('content')

{{-- Delete Post Form --}}
<form id="deleteForm" class="form-horizontal" method="post"
	action="@if (isset($news)){{ URL::to('admin/photo/' . $photo->id . '/delete') }}@endif"
	autocomplete="off">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> <input
		type="hidden" name="id" value="{{ $photo->id }}" />
	<!-- <input type="hidden" name="_method" value="DELETE" /> -->
	<!-- ./ csrf token -->

	<!-- Form Actions -->
	<div class="form-group">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="controls">
			<img src="/img/photo/{{$photo->filename}}" class="img-responsive" style="max-height:200px;">			
			<div class="label label-danger">Hình ảnh: {{$photo->name}} - {{$photo->filename}}</div>
			<h3>{{ Lang::get("admin/modal.delete_message") }}</h3>
			<element class="btn btn-warning btn-sm close_popup">
			<span class="glyphicon glyphicon-ban-circle"></span> {{
			Lang::get("admin/modal.cancel") }}</element>
			<button type="submit" class="btn btn-sm btn-danger">
				<span class="glyphicon glyphicon-trash"></span> {{
				Lang::get("admin/modal.delete") }}
			</button>
		</div>
	</div>
	</div>	
	<!-- ./ form actions -->
</form>

@stop
