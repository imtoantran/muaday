@extends('admin.layouts.default') {{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/photo.photo") }}} @parent @stop

{{-- Content --}} @section('content')
<div class="page-header">
	<h3>
		{{{ Lang::get("admin/photo.photo") }}}
		<div class="pull-right">
			<a href="{{{ URL::to('admin/photo/create') }}}"
				class="btn btn-sm  btn-primary"><span
				class="glyphicon glyphicon-plus-sign"></span> {{
				Lang::get("admin/modal.new") }}</a>
		</div>
	</h3>
</div>

<table id="datatable_ajax" class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Id</th>
			<th>{{{ Lang::get("admin/modal.title") }}}</th>
			<th>{{{ Lang::get("admin/photo.album") }}}</th>
			<th>{{{ Lang::get("admin/photo.album_cover") }}}</th>
			<th>{{{ Lang::get("admin/photo.slider") }}}</th>
			<th>{{{ Lang::get("admin/photo.picture") }}}</th>
			<th>{{{ Lang::get("admin/admin.created_at") }}}</th>
			<th>{{{ Lang::get("admin/admin.action") }}}</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
@stop {{-- Scripts --}} 
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#datatable_ajax").DataTable({
                serverSide:true,
                processing:true,
                oLanguage:{sUrl:"{{url("assets/global/plugins/datatables/plugins/i18n/Vietnamese.json")}}"},
                ajax:{
                    url:"{{URL::to('admin/photo/data/'.((isset($album))?$album->id:0))}}",
                    method:"post",
                },
                "columnDefs": [
	            {
	                "targets": [0],
	                "visible": false,
	                "searchable": false
	            },],
			    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			    	$(nRow).attr("title","Click vào đây đê sửa");
			      	$(nRow).on("click",function(){
			      		location.href = '{{url("admin/photo/edit")}}/' + aData[0];
			      	});
			      	return nRow;
			    }
            });
        });
    </script>
@stop
@section('scriptss')
<script type="text/javascript">
	var oTable;
	$(document).ready(function() {
		oTable = $('#table').dataTable({
			"sDom" : "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
			"sPaginationType" : "bootstrap",
			
			"bProcessing" : true,
			"bServerSide" : true,
			"sAjaxSource" : "{{ URL::to('admin/photo/data/'.((isset($album))?$album->id:0)) }}",
			"fnDrawCallback" : function(oSettings) {
				$(".iframe").colorbox({
					iframe : true,
					width : "80%",
					height : "80%",
					onClosed : function() {
						window.location.reload();
					}
				});
			}
		});
			var startPosition;
            var endPosition;
            $("#table tbody").sortable({
                cursor : "move",
                start : function(event, ui) {
                    startPosition = ui.item.prevAll().length + 1;
                },
                update : function(event, ui) {
                    endPosition = ui.item.prevAll().length + 1;
                    var navigationList = "";
                    $('#table #row').each(function(i) {
                        navigationList = navigationList + ',' + $(this).val();
                    });
                    $.getJSON("{{ URL::to('admin/photo/reorder') }}", {
                        list : navigationList
                    }, function(data) {
                    });
                }
            });
	}); 
</script>
@stop
