@extends('admin.layouts.default')
@section('content')
    <div class="row margin-top-20">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i> Hồ sơ cá nhân
                    </div>
                    <div class="tools">
                        <a title="" data-original-title="" href="" class="collapse">
                        </a>
                        <a title="" data-original-title="" href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a title="" data-original-title="" href="" class="reload">
                        </a>
                        <a title="" data-original-title="" href="" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" method="post">
                        <input name="_token" type="hidden" value="{{csrf_token()}}"/>
                        <div class="form-body">
                            <div class="form-group">
                                <label>Email</label>

                                <div class="input-icon">
                                    <i class="fa fa-envelope"></i>
                                    <input class="form-control" name="email" placeholder="Vui lòng cập nhật email" type="email" value="{{$user->email or ""}}">
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label>Mật khẩu cũ</label>--}}

                                {{--<div class="input-icon">--}}

                                    {{--<i class="fa fa-user"></i>--}}

                                    {{--<input class="form-control"  placeholder="Mật khẩu cũ"--}}
                                           {{--type="password" name="old_password">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label>Mật khẩu mới</label>

                                <div class="input-icon">

                                    <i class="fa fa-user"></i>

                                    <input class="form-control" placeholder="Mật khẩu mới"
                                           type="password" name="new_password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label >Xác nhận mật khẩu</label>

                                <div class="input-icon">

                                    <i class="fa fa-user"></i>

                                    <input class="form-control" placeholder="Xác nhận mật khẩu"
                                           type="password" name="ver_password">
                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Lưu</button>
                            <button type="button" class="btn default">Hủy</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>@stop
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $("#roles").select2()
        });
    </script>
@stop
