@extends('admin.layouts.default')
@section('content')


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-actions-wrapper">
									<span>
									</span>
                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                <option value="">Chọn...</option>
                                <option value="Cancel">Thành viên</option>
                                <option value="Close">Khách</option>
                            </select>
                            <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> Gửi
                            </button>
                        </div>
                        <table id="users-list" class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th class="font-red"><i class="fa fa-briefcase"></i> Tên</th>
                                <th class="font-red"><i class="fa fa-envelope"></i> Email</th>
                                <th class="font-red"><i class="fa fa-group"></i> Nhóm</th>
                                <th class="font-red"><i class="fa fa-calendar"></i> Tham gia</th>
                                <th class="font-red"><i class="fa fa-wrench"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="clearfix">
    </div>
@stop
@section('page_title')
    Quản lý người dùng
@stop
@section('scripts')
    <script>

        $(document).ready(function () {
            var table = $("#users-list").DataTable({
                serverSide:true,
                processing:true,
                oLanguage:{sUrl:"{{url("assets/global/plugins/datatables/plugins/i18n/Vietnamese.json")}}"},
                ajax:{
                    url:"{{URL::to('admin/users/data')}}",
                    method:"post"
                },
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    },],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).attr("title","Click vào đây đê sửa");
                    $(nRow).on("click",function(){

                        location.href = '{{url("admin/users/edit")}}/' + aData[0];
                    });
                    return nRow;
                }
            });
        });
    </script>
    @stop