@extends('admin.layouts.default')
{{-- Content --}}
@section('content')
    {{-- Edit Blog Form --}}
    <form class="form-horizontal"
          enctype="multipart/form-data"
          method="post"
          action="@if(isset($banner)){{ URL::to('admin/banners/home/'.$banner->id) }} @endif"
          autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
        <!-- ./ csrf token -->
        <div class="form-group {{{ $errors->has('image') ? 'error' : '' }}}">
            <div class="col-xs-6">
                <label class="control-label" for="picture" style="min-width: 200px; height: 200px;cursor:pointer;background:#eee;text-align:center">
                    <img id="uploadPreview" src="/img/banners/{{isset($banner->image)?$banner->image:''}}" style="min-width: 200px;height: 200px;" alt="Hình ảnh"/>
                </label>
                <input name="picture" type="file" class="uploader hidden" id="picture" value="Upload" onchange="PreviewImage();"/>
            </div>
            <div class="col-xs-6">
                <div class="alert alert-info">
                	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                	<i class="fa fa-info-circle"></i> <strong>Kích thước hình ảnh:</strong> 800x372
                </div>
            </div>
        </div>

        <div class="form-group {{{ $errors->has('title') ? 'has-error' : '' }}}">
            <div class="col-md-3">
                <label class="control-label" for="title"> Tiêu đề</label>
                <input class="form-control" type="text" name="title" id="title" value="{{{ Input::old('title', isset($banner) ? $banner->title : null) }}}"/>
            </div>
            <div class="col-md-3">
                <label class="control-label" for="icon">Icon: </label>
                <input class="form-control full-width " name="icon" value="{{{ Input::old('icon', isset($banner) ? $banner->icon : null) }}}">
            </div>
            <div class="col-md-3">
                <label class="control-label" for="link">Liên kết: </label>
                <input type="text" class="form-control full-width " name="link" value="{{{ Input::old('link', isset($banner) ? $banner->link : null) }}}">
            </div>
            <div class="col-md-3">
                <label class="control-label" for="order">Thứ tự: </label>
                <input type="number" class="form-control full-width " name="order" value="{{{ Input::old('order', isset($banner) ? $banner->order : null) }}}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <button type="reset" class="btn btn-sm btn-warning close_popup">
                    <span class="glyphicon glyphicon-ban-circle"></span> Hủy
                </button>
                <button type="submit" class="btn btn-sm btn-success">
                    <span class="glyphicon glyphicon-ok-circle"></span>
                        Lưu
                </button>
            </div>
        </div>
    </form>
@stop

@section('scripts')
<script type="text/javascript">
  function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("picture").files[0]);

    oFReader.onload = function(oFREvent) {
      document.getElementById("uploadPreview").src = oFREvent.target.result;
    };
  };
</script>
@stop