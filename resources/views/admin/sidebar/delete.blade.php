@extends('admin.layouts.default') {{-- Content --}}
@section('content')

<!-- Tabs -->
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">{{
			Lang::get("admin/modal.general") }}</a></li>
</ul>
<!-- ./ tabs -->
{{-- Delete Post Form --}}
<form id="deleteForm" class="form-horizontal" method="post"
	action="@if (isset($sidebar)){{ URL::to('admin/sidebars/delete/' . $sidebar->id) }}@endif"
	autocomplete="off">

	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> <input
		type="hidden" name="id" value="{{ $sidebar->id }}" />
	<!-- <input type="hidden" name="_method" value="DELETE" /> -->
	<!-- ./ csrf token -->
<div class="form-group">
    <div class="col-xs-12">
        <img src="/img/sidebars/{{$sidebar->image}}" style="max-height: 200px" class="img-responsive">
    </div>
</div>
	<!-- Form Actions -->
	<div class="form-group">
        <div class="col-xs-12">
            <div class="controls">
			{{ Lang::get("admin/modal.delete_message") }}<br>
			<element class="btn btn-warning btn-sm close_popup">
			<span class="glyphicon glyphicon-ban-circle"></span> {{Lang::get("admin/modal.cancel") }}
            </element>
			<button type="submit" class="btn btn-sm btn-danger">
				<span class="glyphicon glyphicon-trash"></span> {{Lang::get("admin/modal.delete") }}
			</button>
		</div>
		</div>
	</div>
	<!-- ./ form actions -->
</form>
@stop
