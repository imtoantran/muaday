@extends('admin.layouts.default') {{-- Web site Title --}}
@section('title') Banner :: @parent @stop
@section('page_title') Banner @stop

@section('content')
	<!-- BEGIN PAGE CONTENT-->
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i>Banner phải
					</div>
                    <div class="actions">
                        <a href="{{URL::to('admin/sidebars/create')}}" class="btn default yellow-stripe">
                            <i class="fa fa-plus"></i>
								<span class="hidden-480">
								Thêm banner </span>
                        </a>

                    </div>
				</div>
				<div class="portlet-body">
					<div class="table-container">
						<table class="table table-striped table-bordered table-hover" id="datatable_ajax">
							<thead>
							<tr role="row" class="heading">
                                <th width="300">Hình ảnh</th>
								{{--<th>Nội dung</th>--}}
								<th>Tiêu đề</th>
								<th>Thứ tự</th>
								<th>Kích hoạt</th>
								<th width="50"><i class="fa fa-gear"></i></th>
							</tr>
							</thead>
                            @if(isset($banners))
							<tbody>
                            @foreach($banners as $banner)
                                <tr>
                                    <td><img class="img-responsive" src="/img/sidebars/{!! $banner->image !!}" alt=""/></td>
                                    {{--<td>{!! $banner->content !!}</td>--}}
                                    <td>{!! $banner->title !!}</td>
                                    <td>{!! $banner->order !!}</td>
                                    <td>{!! $banner->active?"Có":"Không" !!}</td>
                                    <td>
                                        <a href="{{url("/admin/sidebars/edit/$banner->id")}}" class="btn btn-xs blue">Sửa</a>
                                        <a href="{{url("/admin/sidebars/delete/$banner->id")}}" class="btn btn-xs red">Xóa</a>
                                    </td>
                                </tr>
                                @endforeach
							</tbody>
                                @endif
						</table>
					</div>
				</div>
			</div>
			<!-- End: life time stats -->
		</div>
	</div>
	<!-- END PAGE CONTENT-->
	<div class="clearfix">
	</div>
@stop
@section('page_level_scrips')
@stop
@section('scriptss')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#datatable_ajax").DataTable({
                serverSide:true,
                processing:true,
                oLanguage:{sUrl:"{{url("assets/global/plugins/datatables/plugins/i18n/Vietnamese.json")}}"},
                ajax:{
                    url:"{{URL::to('admin/news/data')}}",
                    method:"post"
                },
                "columnDefs": [
	            {
	                "targets": [0],
	                "visible": false,
	                "searchable": false
	            },],
			    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			    	$(nRow).attr("title","Click vào đây đê sửa");
			      	$(nRow).on("click",function(){
			      		location.href = '{{url("admin/news/edit")}}/' + aData[0];
			      	});
			      	return nRow;
			    }
            });
        });
    </script>
@stop