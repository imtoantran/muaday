@extends('admin.layouts.default')
{{-- Content --}}
@section('content')
    {{-- Edit Blog Form --}}
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                Thêm/Cập nhật Banner phải
            </div>
            <div class="actions">
                <a href="{{URL::to('admin/sidebars/create')}}" class="btn default yellow-stripe">
                    <i class="fa fa-plus"></i>
								<span class="hidden-480">
								Thêm banner </span>
                </a>

            </div>
        </div>
        <div class="portlet-body">


    <form class="form-horizontal"
          enctype="multipart/form-data"
          method="post"
          action="@if(isset($banner)){{ URL::to('admin/sidebars/edit/'.$banner->id) }} @endif"
          autocomplete="off">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
        <!-- ./ csrf token -->
        <div class="form-group {{{ $errors->has('image') ? 'error' : '' }}}">
            <div class="col-xs-12 col-sm-6">
                <label class="control-label" for="picture" style="cursor:pointer;background:#eee;text-align:center">
                    <img id="uploadPreview" src="/img/sidebars/{{isset($banner->image)?$banner->image:''}}" style="width: 100%;" class="img-responsive" alt="Hình ảnh"/>
                </label>
                <input name="picture" type="file" class="uploader hidden" id="picture" value="Upload" onchange="PreviewImage();"/>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group {{{ $errors->has('title') ? 'has-error' : '' }}}">
                    <div class="col-xs-12">
                        <label class="control-label" for="title"> Tiêu đề</label>
                        <input class="form-control" type="text" name="title" id="title" value="{{{ Input::old('title', isset($banner) ? $banner->title : null) }}}"/>
                    </div >
                    <div class="col-xs-12">
                        <label class="control-label" for="link">Liên kết: </label>
                        <input type="text" class="form-control full-width " name="link" value="{{{ Input::old('link', isset($banner) ? $banner->link : null) }}}">
                    </div>
                    <div class="col-xs-12">
                        <label class="control-label" for="order">Thứ tự: </label>
                        <input type="number" class="form-control full-width " name="order" value="{{{ Input::old('order', isset($banner) ? $banner->order : $post) }}}">
                    </div>
                    <div class="col-xs-12">
                        <label class="control-label" for="active">Kích hoạt: </label>
                        <input type="checkbox" class="form-control full-width" name="active" value="1" {{{ Input::old('active', isset($banner) ? $banner->active : true)?"checked":"" }}}>
                    </div>
                </div>
            </div>
        </div>
        
        {{--<div class="form-group">--}}
            {{--<div class="col-md-12">--}}
                {{--<label class="control-label" for="content">Nội dung: </label>--}}
                {{--<textarea class="form-control full-width tinymce" name="content">{{{ Input::old('content', isset($banner) ? $banner->content : null) }}}</textarea>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="form-group">
            <div class="col-md-12">
                <button type="reset" class="btn btn-sm btn-warning close_popup">
                    <span class="glyphicon glyphicon-ban-circle"></span> Hủy
                </button>
                <button type="submit" class="btn btn-sm btn-success">
                    <span class="glyphicon glyphicon-ok-circle"></span>
                        Lưu
                </button>
            </div>
        </div>
    </form>
            </div>
@stop

@section('scripts')
<script type="text/javascript">
  function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("picture").files[0]);

    oFReader.onload = function(oFREvent) {
      document.getElementById("uploadPreview").src = oFREvent.target.result;
    };
  };
</script>
@stop