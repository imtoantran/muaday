@extends('admin.layouts.default') @section('content')
<form id="deleteForm" class="form-horizontal" method="post"
	action="@if (isset($photoalbum)){{ URL::to('admin/photo/album/delete/' . $photoalbum->id ) }}@endif"
	autocomplete="off">
	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> <input
		type="hidden" name="id" value="{{ $photoalbum->id }}" />
	<div class="form-group">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">		
		<div class="controls">
			<h3>Album: {{$photoalbum->name}}</h3>
			<div class="alert alert-danger">
				{{ Lang::get("admin/modal.delete_message") }}
			</div>
			<element class="btn btn-warning btn-sm close_popup">
			<span class="glyphicon glyphicon-ban-circle"></span> {{
			Lang::get("admin/modal.cancel") }}</element>
			<button type="submit" class="btn btn-sm btn-danger">
				<span class="glyphicon glyphicon-trash"></span> {{
				Lang::get("admin/modal.delete") }}
			</button>
		</div>
	</div>
	</div>
</form>
@stop
