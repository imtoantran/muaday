@extends('admin.layouts.default') 
{{-- Web site Title --}}
@section('title') 
{{{ Lang::get("admin/photoalbum.photoalbum") }}} ::
@parent 
@stop 
{{-- Content --}} 
@section('content')
<div class="page-header">
	<h3>
		{{{ Lang::get("admin/photoalbum.photoalbum") }}}
		<div class="pull-right">
			<a href="{{{ URL::to('admin/photo/album/create') }}}"
				class="btn btn-sm  btn-primary"><span
				class="glyphicon glyphicon-plus-sign"></span> {{
				Lang::get("admin/modal.new") }}</a>
		</div>
	</h3>
</div>

<table id="datatable_ajax" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Id</th>
			<th>{{{ Lang::get("admin/modal.title") }}}</th>			
			<th>{{{ Lang::get("admin/photoalbum.numbers_of_items") }}}</th>
			<th>{{{ Lang::get("admin/admin.created_at") }}}</th>
			<th>{{{ Lang::get("admin/admin.action") }}}</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
@stop 
{{-- Scripts --}} 
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#datatable_ajax").DataTable({
                serverSide:true,
                processing:true,
                oLanguage:{sUrl:"{{url("assets/global/plugins/datatables/plugins/i18n/Vietnamese.json")}}"},
                ajax:{
                    url:"{{URL::to('admin/photo/album/data')}}",
                    method:"post",
                },
                "columnDefs": [
	            {
	                "targets": [0],
	                "visible": false,
	                "searchable": false
	            },],
			    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			    	$(nRow).attr("title","Click vào đây đê sửa");
			      	$(nRow).on("click",function(){
			      		location.href = '{{url("admin/photo/album/edit")}}/' + aData[0];
			      	});
			      	return nRow;
			    }
            });
        });
    </script>
@stop