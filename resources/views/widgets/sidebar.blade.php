<div class="row">
    @foreach($sidebars as $sidebar)
        @if($sidebar->image)
        <div class="col-xs-12">
            <div class="sidebar-item">
                <a href="{!! $sidebar->link or '#' !!}">
                    <img src="/img/sidebars/{{$sidebar->image}}" class="img-responsive" alt="{!! $sidebar->title !!}">
                </a>
            </div>
        </div>
        @endif
    @endforeach
</div>