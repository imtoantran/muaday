<!-- BEGIN SALE PRODUCT & NEW ARRIVALS -->
<div class="row margin-bottom-40" id="san-pham">
    <!-- BEGIN SALE PRODUCT -->
    <div class="col-md-12 sale-product">
        <h2 style="text-transform: uppercase">Sản phẩm</h2>

        <div class="owl-carousel owl-carousel5">
            @foreach($products as $product)
                <div>
                    <div class="product-item">
                        <div class="pi-img-wrapper">
                            <img src="/img/products/{!! $product->image[0] !!}" class="img-responsive">

                            <div>
                                <a href="/img/products/{!! $product->image[0] !!}"
                                   class="btn btn-default fancybox-button">Zoom</a>
                                <a href="{!! $product->link() !!}"
                                   class="btn btn-default fancybox-fast-view">Xem</a>
                            </div>
                        </div>
                        <h3>
                            <a href="{!! $product->link() !!}">{!! $product->name !!}</a>
                            @if(0 < $product->sale_price && $product->sale_price< $product->price)
                                <del class="pull-right font-red">{!! number_format($product->price) !!} VNĐ</del>
                            @endif
                        </h3>

                        <div class="pi-price">
                            @if(0 < $product->sale_price && $product->sale_price< $product->price)
                                {!! number_format($product->sale_price) !!} VNĐ
                                <span class="badge badge-danger">- {{number_format(100*(1-$product->sale_price/$product->price))}}%</span>
                            @else
                                {!! number_format($product->price) !!} VNĐ
                            @endif
                        </div>
                        <a href="#" class="btn btn-default add2cart">Mua</a>
                        @if(0 < $product->sale_price && $product->sale_price< $product->price)
                            {{--<div class="sticker sticker-sale"></div>--}}
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <!-- END SALE PRODUCT -->
</div>
<!-- END SALE PRODUCT & NEW ARRIVALS -->