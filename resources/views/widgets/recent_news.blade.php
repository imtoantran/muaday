<h3>Tin mới</h3>
<div class="recent-news margin-bottom-10">
    @foreach ($articles as $i=>$article)
    <div class="margin-bottom-10">
        <div class="row">
        <div class="col-xs-2 col-sm-12 col-md-5">
            <img class="img-responsive" alt="" src="/img/articles/{!! $article->picture !!}">
        </div>
        <div class="col-xs-10 col-sm-12 col-md-7 recent-news-inner">
            <h3><a href="{!! $article->link() !!}">{!! $article->title !!}</a></h3>
            <p>{!! Str::limit(clean($article->introduction,'clear'),30) !!}</p>
        </div>
        </div>
    </div>
    @endforeach
</div>