           <!-- BEGIN SLIDER -->
    @if(count($slider))
        <div class="fullwidthbanner-container revolution-slider">
            <div class="fullwidthabnner">
                <ul id="revolutionul">
                    <!-- THE FIRST SLIDE -->
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                        data-thumb="/img/home/DHP VIETNAM (2).jpg">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="/img/home/03.jpg" class="ls-bg" alt="">
                        <div class="caption lfb"
                             data-x="0"
                             data-y="0"
                             data-speed="700"
                             data-start="1000"
                             data-easing="easeOutExpo">
                            {{--<img src="/img/home/DHP VIETNAM (2).jpg" alt="Image 1">--}}
                        </div>
                    </li>
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                        data-thumb="/img/home/DHP VIETNAM (2).jpg">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="/img/home/02.jpg" class="ls-bg" alt="">
                        <div class="caption lfb"
                             data-x="0"
                             data-y="0"
                             data-speed="700"
                             data-start="1000"
                             data-easing="easeOutExpo">
                            {{--<img src="/img/home/DHP VIETNAM (2).jpg" alt="Image 1">--}}
                        </div>
                    </li>
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                        data-thumb="/img/home/DHP VIETNAM (8).jpg">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="/img/home/01.jpg" alt="" class="ls-bg">
                        <div class="caption lfb"
                             data-x="0"
                             data-y="0"
                             data-speed="700"
                             data-start="1000"
                             data-easing="easeOutExpo">
                            {{--<img src="/img/home/DHP VIETNAM (8).jpg" alt="Image 1">--}}
                        </div>
                    </li>
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                        data-thumb="/img/home/DHP VIETNAM (8).jpg">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="/img/home/08.jpg" alt="" class="ls-bg">
                        <div class="caption lfb"
                             data-x="0"
                             data-y="0"
                             data-speed="700"
                             data-start="1000"
                             data-easing="easeOutExpo">
                            {{--<img src="/img/home/DHP VIETNAM (8).jpg" alt="Image 1">--}}
                        </div>
                    </li>
                </ul>
                <div class="tp-bannertimer tp-bottom"></div>
            </div>
        </div>
        @endif
                <!-- END SLIDER -->