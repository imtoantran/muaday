<h3>Danh mục</h3>
<ul class="nav sidebar-categories margin-bottom-40">

@foreach($categories as  $category)
    <li>
    <a href="/tin-tuc/{!! $category->slug !!}">{{ $category->title }}</a>
    </li>
@endforeach
</ul>
