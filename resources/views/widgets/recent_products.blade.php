<div class="box box_gray widget-container widget_recent_entries">
    <h3>Sản phẩm mới</h3>
    <ul>
    @foreach ($products as $i=>$element)
        <li class="{{($i%2)?"odd":"even"}}">
            <a href="{{$element->link()}}">
                <img src="/images/temp_thumb_1.jpg" alt="" class="thumbnail" border="0" height="70" width="70">
                <b>{{Str::words($element->name,6)}}</b>           
            <p style="word-break:break-all">{{ Str::words(clean($element->excerpt,'clear'),5) }}</p>
            <div class="date">{{$element->updated_at->format("d/m/Y")}}</div>         
            </a> 
            <div class="clear"></div>
        </li>
    @endforeach
    </ul>
</div>

