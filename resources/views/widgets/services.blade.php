    <div class="services-block content content-center" id="services">
        <div class="container">
            <h2><strong>Dịch vụ </strong> của chúng tôi</h2>
            @if(count($services))
                @foreach($services as $service)
                <div class="col-md-{{$column}} col-sm-{{$column}} col-xs-12 item">
                    <i class="{{$service->icon}}"></i>
                        <h3><a href="{{$service->link()}}">{{$service->title}}</a></h3>
                    <div class="service-content">
                        {!! Str::limit($service->description,400) !!}
                    </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
