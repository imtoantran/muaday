@if(count($banners))
<div class="row service-box margin-bottom-40">
    @foreach ($banners as $banner)
        <div class="col-md-4 col-sm-4">
            <div class="service-box-heading">

                <em><i class="{!! $banner->icon !!}"></i></em>
                <span>{!! $banner->title !!}</span>
            </div>
            <a href="{!! $banner->link or '#' !!}">
            <img class="img-responsive" src="/img/banners/{!! $banner->image !!}" alt=""/>
                </a>
        </div>
    @endforeach
</div>
@endif
