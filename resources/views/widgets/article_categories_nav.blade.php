@if(isset($newscategories))
    <ul class="dropdown-menu">
        @foreach($newscategories as $category)
            <li><a href="/tin-tuc/{{$category->slug}}">{{$category->tite}}</a></li>
        @endforeach
    </ul>
@endif