  {{--Created by toantran--}}
  {{--User: toantran--}}
  {{--Date: 6/2/2015--}}
  {{--Time: 11:28 PM--}}


@extends('site.layouts.app')
@section('content')
@foreach($products as $product)
    <?php $product->image = json_decode($product->image) ?>
    <img src="/img/products/{{ $product->image[0] }}" class="img-responsive" alt="{{$product->name}}">
    {{$product->name}}
    {{Str::words($product->excerpt,100)}}
    @endforeach
<div>
    <ul class="pagination pagination-small">
    	<li><a href="#">&laquo;</a></li>
    	<li><a href="#">1</a></li>
    	<li><a href="#">2</a></li>
    	<li><a href="#">3</a></li>
    	<li><a href="#">4</a></li>
    	<li><a href="#">5</a></li>
    	<li><a href="#">&raquo;</a></li>
    </ul>
</div>
@endsection