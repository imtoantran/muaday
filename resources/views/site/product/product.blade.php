<?php
/**
 * Created by PhpStorm.
 * User: toantran
 * Date: 6/2/2015
 * Time: 11:28 PM
 */
?>
@extends('site.layouts.product')
@section('meta')
    <meta property="og:title" content="{!! $product->excerpt !!}" />
    <meta property="og:type" content="product" />
    <meta property="og:url" content="{!! url("san-pham/$product->slug.html") !!}" />
    <meta property="og:description" content="{!! $product->excerpt or Purifier::clean($product->description,'clear') !!}" />
    @if(count($product->image))
    @foreach($product->image as $image)
        <meta property="og:image" content="{{url("img/products/".$image)}}" />
        @endforeach
    @endif
@stop
@section('scripts')
    <script>
        jQuery(document).ready(function(){
            Layout.initImageZoom();
        });
    </script>
@stop
@section('plugin_script')
    <script src='/assets/global/plugins/zoom/jquery.zoom.min.js' type="text/javascript"></script><!-- product zoom -->
    <script src="/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
@stop
@section('plugin-styles')
    <link href="/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
@stop
@section('theme-style')
    <link href="/assets/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css">
@stop
@section("page-title")
{{$product->name}}
@stop
@section('products')
    <!-- BEGIN CONTENT -->
    <div class="col-md-9 col-sm-12">
        <div class="product-page">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="product-main-image">
                        @if(count($product->image))
                        <img src="/img/products/{{$product->image[0]}}" alt="{{$product->name}}" class="img-responsive" data-BigImgsrc="/img/products/{!! $product->image[0] !!}">
                            @endif
                    </div>
                    <div class="product-other-images">
                        @if(count($product->image)>1)
                            @for($i=1;$i<count($product->image);$i++)
                                <a href="/img/products/{!! $product->image[$i] !!}" class="fancybox-button" rel="photos-lib"><img alt="Berry Lace Dress" src="/img/products/{!! $product->image[$i] !!}"></a>
                                @endfor
                            @endif
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <h1>
                        {{Str::words($product->name,10)}}
                    </h1>
                    <div class="price-availability-block clearfix">
                        <div class="price">
                            @if(0 < $product->sale_price && $product->sale_price< $product->price)
                            <p><strong>{{number_format($product->sale_price)}}<span>VNĐ</span></strong></p>
                            <p>
                                <em><span class="font-red">{{number_format($product->price)}} VNĐ</span> </em>
                                <i class="fa fa-sort-desc"></i>
                                <span class="badge badge-danger">{{100*number_format(1-$product->sale_price/$product->price,2)}}%</span>
                            </p>
                                @else
                                <strong>{{number_format($product->price)}}<span>VNĐ</span></strong>
                            @endif
                        </div>
                        <div class="availability">
                            Trạng thái: <strong>Có hàng</strong>
                        </div>
                    </div>
                    <div class="description">
                        {!! Str::words(Purifier::clean($product->description,'clear'),20) !!}
                    </div>
                    <hr/>
                    <div>
                        <h3>Hỗ trợ mua hàng</h3>
                        <p><i class="fa fa-phone-square"></i> Hotline: (08)62968444</p>
                        {{--<p><i class="fa fa-phone-square"></i> Di động: (08)62968444</p>--}}
                    </div>
                    <ul class="social-icons">
                        <li><a class="facebook" data-original-title="facebook" href="#"></a></li>
                        <li><a class="twitter" data-original-title="twitter" href="#"></a></li>
                        <li><a class="googleplus" data-original-title="googleplus" href="#"></a></li>
                    </ul>
                    <div class="product-page-cart text-right">
                        <button class="btn btn-primary" type="submit">Mua</button>
                    </div>
                </div>

                <div class="product-page-content">
                    <ul id="myTab" class="nav nav-tabs">
                        <li  class="active"><a href="#Description" data-toggle="tab">Mô tả</a></li>
                        <li><a href="#Information" data-toggle="tab">Thông tin</a></li>
                        <li><a href="#Reviews" data-toggle="tab">Đánh giá</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in active" id="Description">
                        {!! $product->description !!}
                        </div>
                        <div class="tab-pane fade" id="Information">
                            Đang cập nhật
                        </div>
                        <div class="tab-pane fade" id="Reviewss">
                            <!--<p>There are no reviews for this product.</p>-->
                            <div class="review-item clearfix">
                                <div class="review-item-submitted">
                                    <strong>Bob</strong>
                                    <em>30/12/2013 - 07:37</em>
                                    <div class="rateit" data-rateit-value="5" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                                </div>
                                <div class="review-item-content">
                                    <p>Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci. Phasellus sed erat leo. Donec luctus, justo eget ultricies tristique, enim mauris bibendum orci, a sodales lectus purus ut lorem.</p>
                                </div>
                            </div>
                            <div class="review-item clearfix">
                                <div class="review-item-submitted">
                                    <strong>Mary</strong>
                                    <em>13/12/2013 - 17:49</em>
                                    <div class="rateit" data-rateit-value="2.5" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                                </div>
                                <div class="review-item-content">
                                    <p>Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci. Phasellus sed erat leo. Donec luctus, justo eget ultricies tristique, enim mauris bibendum orci, a sodales lectus purus ut lorem.</p>
                                </div>
                            </div>

                            <!-- BEGIN FORM-->
                            <form action="#" class="reviews-form" role="form">
                                <h2>Write a review</h2>
                                <div class="form-group">
                                    <label for="name">Name <span class="require">*</span></label>
                                    <input type="text" class="form-control" id="name">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email">
                                </div>
                                <div class="form-group">
                                    <label for="review">Review <span class="require">*</span></label>
                                    <textarea class="form-control" rows="8" id="review"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="email">Rating</label>
                                    <input type="range" value="4" step="0.25" id="backing5">
                                    <div class="rateit" data-rateit-backingfld="#backing5" data-rateit-resetable="false"  data-rateit-ispreset="true" data-rateit-min="0" data-rateit-max="5">
                                    </div>
                                </div>
                                <div class="padding-top-20">
                                    <button type="submit" class="btn btn-primary">Send</button>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
                @if(0 < $product->sale_price && $product->sale_price< $product->price)
                {{--<div class="sticker sticker-sale"></div>--}}
                @endif
            </div>
        </div>
    </div>
    <!-- END CONTENT -->
    @stop
