@extends('site.layouts.app') {{-- Web site Title --}}
@section('title') {{{ $news->title }}} :: @parent @stop

@section('meta_author')
<meta name="author" content="{{{ $news->author->username }}}" />
@stop {{-- Content --}}
@section('content')
@section('content')
	<div class="main">
		<ul class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li><a href="#">Blog</a></li>
			<li class="active">Blog Page</li>
		</ul>
		<!-- BEGIN SIDEBAR & CONTENT -->
		<div class="row margin-bottom-40">
			<!-- BEGIN CONTENT -->
			<div class="col-md-12 col-sm-12">
				<h1>{{$news->title}}</h1>
				<div class="content-pages">
					<div class="row">
						<!-- BEGIN LEFT SIDEBAR -->
						<div class="col-md-9 col-sm-9 blog-posts">

							<div class="blog-item-img">
								<!-- BEGIN CAROUSEL -->
								<div class="front-carousel">
									<div id="myCarousel" class="carousel slide">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<div class="item active">
												<img src="/img/articles/{{$news->picture}}" alt="">
											</div>
										</div>
										<!-- Carousel nav -->
									</div>
								</div>
								<!-- END CAROUSEL -->
							</div>
							<h2><a href="#">{{$news->title}}</a></h2>
							{!! $news->content !!}
							<ul class="blog-info">
								<li><i class="fa fa-user"></i> Đăng bởi {{$news->author->name}}</li>
								<li><i class="fa fa-calendar"></i> {{ $news->created_at->format("d-m-Y") }}</li>
								<li><i class="fa fa-comments"></i></li>
								<li><i class="fa fa-tags"></i> Tin tức, Sự kiện</li>
							</ul>
						</div>
						<!-- END LEFT SIDEBAR -->

						<!-- BEGIN RIGHT SIDEBAR -->
						<div class="col-md-3 col-sm-3 blog-sidebar">
							<!-- CATEGORIES START -->
							<h2 class="no-top-space">Danh mục</h2>
							<ul class="nav sidebar-categories margin-bottom-40">
								@if(count($categories))
									@foreach($categories as $category)
										<li><a href="{{url("tin-tuc/$category->slug")}}">{{$category->title}}</a></li>
									@endforeach
								@endif
							</ul>
							<!-- CATEGORIES END -->

							<!-- BEGIN RECENT NEWS -->
							<h2>Tin gần đây</h2>
							<div class="recent-news margin-bottom-10">
								<div class="row margin-bottom-10">
									<div class="col-md-3">
										<img class="img-responsive" alt="" src="../../assets/frontend/pages/img/people/img2-large.jpg">
									</div>
									<div class="col-md-9 recent-news-inner">
										<h3><a href="#">Letiusto gnissimos</a></h3>
										<p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
									</div>
								</div>
								<div class="row margin-bottom-10">
									<div class="col-md-3">
										<img class="img-responsive" alt="" src="../../assets/frontend/pages/img/people/img1-large.jpg">
									</div>
									<div class="col-md-9 recent-news-inner">
										<h3><a href="#">Deiusto anissimos</a></h3>
										<p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
									</div>
								</div>
								<div class="row margin-bottom-10">
									<div class="col-md-3">
										<img class="img-responsive" alt="" src="../../assets/frontend/pages/img/people/img3-large.jpg">
									</div>
									<div class="col-md-9 recent-news-inner">
										<h3><a href="#">Tesiusto baissimos</a></h3>
										<p>Decusamus tiusto odiodig nis simos ducimus qui sint</p>
									</div>
								</div>
							</div>
							<!-- END RECENT NEWS -->

							<!-- BEGIN BLOG TAGS -->
							<div class="blog-tags margin-bottom-20">
								<h2>Tags</h2>
								<ul>
									<li><a href="#"><i class="fa fa-tags"></i>MS</a></li>
									<li><a href="#"><i class="fa fa-tags"></i>Google</a></li>
									<li><a href="#"><i class="fa fa-tags"></i>Twitter</a></li>
								</ul>
							</div>
							<!-- END BLOG TAGS -->
						</div>
						<!-- END RIGHT SIDEBAR -->
					</div>
				</div>
			</div>
			<!-- END CONTENT -->
		</div>
		<!-- END SIDEBAR & CONTENT -->
	</div>
@stop
@stop
