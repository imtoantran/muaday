@extends('site.layouts.default')
@section('content')
    <div class="col-md-9 col-xs-12  page-content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>{!! $service->title !!}</h2>
                <i>{!! $service->description !!}</i>
                <div class="service-content">{!! $service->content !!}</div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @widget('sidebar',["cssClassForWrapper"=>"sidebar-right"])
    </div>
@stop