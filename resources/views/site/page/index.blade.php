@extends('site.layouts.page')
@section('page')
    <div class="blog-posts">
        @foreach($articles as $article)
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <img src="/img/articles/{{$article->picture}}" class="img-responsive" alt="Image">
                </div>
                <div class="col-sm-6 col-md-6">
                    <h2>{{$article->title}}</h2>
                    <ul class="blog-info">
                        <li><i class="fa fa-calendar"></i> {!! $article->created_at->format("h:i:s d/m/Y") !!}</li>
                        <li><i class="fa fa-comments"></i> <span class="fb-comments-count"
                                                                 data-href="{!! $article->link() !!}" data-numposts="5">Đang tải...</span>
                        </li>
                        {{--<li><i class="fa fa-tags"></i> </li>--}}
                    </ul>
                    <p>{!! Str::words(clean($article->content,'clear'),20) !!}</p>
                    <a href="{!! $article->link() !!}" class="more">Chi tiết <i class="icon-angle-right"></i></a>
                </div>
            </div>
            <hr class="blog-post-sep">
        @endforeach
        <div class="text-center">{!! $articles->render() !!}</div>
    </div>
@stop
@section('title')
    {!! $title or "Trang tin tức" !!}
@stop
