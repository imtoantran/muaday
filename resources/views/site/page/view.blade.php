@extends('site.layouts.page')
@section('page')
    <div class="text">
        @if(!empty($page->picture))
            <div class="thumbnail">
                <img src="/img/articles/{!! $page->picture !!}" class="img-responsive" alt="Image">
            </div>
        @endif
        {!! $page->content !!}
    </div>
@stop
@section('title')
    {!! $page->title !!}
@stop
