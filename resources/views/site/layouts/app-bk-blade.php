<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DHP Viet Nam</title>
    @yield('meta')
    <!-- Fonts START -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=all" rel="stylesheet" type="text/css">
    <!-- Fonts END -->
    <!-- Global styles START -->
    <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/global/plugins/supersized/core/css/supersized.core.css" rel="stylesheet">
    <!-- Global styles END -->    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/assets/frontend/pages/css/style-shop.css">
    <link rel="stylesheet" href="/assets/global/plugins/rateit/src/rateit.css">
    <link rel="stylesheet" href="/assets/global/css/components.css">
    @yield('css_plugins')
    @yield('css')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9] 
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    [endif]-->
</head>
<body class="dhp">
<div class="wrapper container" style="background:rgba(222,222,222,.3)">    
<div class="row">    
    <div class="top-bar hidden-xs">
        <div class="logo">
            <a href="/"><img src="/img/logo/logo.png" class="img-responsive" alt="DHP Việt Nam"></a>
        </div>
        <div class="contact-top"><i class="fa fa-phone font-red"></i> Hot line: 08 629 68 444</div>
        <div class="contact-top"><i class="fa fa-envelope-square font-red"></i> Email: dhpvn888@gmail.com</div>
        <div class="contact-top"><i class="fa fa-skype font-red"></i> Skype: kinh doanh</div>
        <div class="user-panel">
            @if(Session::has("social"))
            <img src="https://graph.facebook.com/v2.2/{{Session::get("social")->id}}/picture?type=normal"
            class="profile-picture img-circle" alt="">
            <div class="user-panel-info">
                <div class="user-guide">
                    <div class="welcome"><i class="fa fa-smile-o"></i> Hi, {{Session::get("social")->name}}!</div>
                </div>
                <div class="auth">
                    <a href="/dang-xuat"><i class="fa fa-sign-out"></i> Đăng xuất</a>
                </div>
            </div>
            @else
            <div class="user-panel-info">
                <div class="auth">
                    <a href="/dang-nhap"><i class="fa fa-sign-in"></i> Đăng nhập</a>
                </div>
            </div>
            @endif
        </div>
    <div class="header">
        <nav class="navbar navbar-dhp">
            <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Menu</span>          
                    <i class="fa fa-bars"></i>
                </button>
                <div class="navbar-brand visible-xs">
                    @if(Session::has("social"))
                    <div class="dropdown">
                      <a class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                        {{Session::get("social")->name}}
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/social/history">Lịch sử chia sẻ</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="/dang-xuat">Đăng xuất</a></li>
                    </ul>
                </div>
                @else
                    <a href="/dang-nhap"> Đăng nhập</a>
                @endif
                </div>           
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="{{Request::is('gioi-thieu*')?"active":""}}">
                        <a href="{{Request::is('gioi-thieu*')?"javascript:void(0);":URL::to('gioi-thieu.html')}}">Giới thiệu</a>
                    </li>
                    <li class="{{Request::is('san-pham*')?"active":""}}">
                        <a href="{{Request::is('san-pham')?"javascript:void(0);":url('san-pham')}}">Sản phẩm</a>
                    </li>
                    <li class="{{Request::is('tuyen-dung*')?"active":""}}">
                        <a href="{{Request::is('tuyen-dung*')?"javascript:void(0);":url('tuyen-dung.html')}}">Tuyển dụng</a>
                    </li>
                    <li class="{{Request::is('thu-vien-anh')?"active":""}}">
                        <a href="{{Request::is('thu-vien-anh')?"javascript:void(0);":url('thu-vien-anh')}}">Thư viện ảnh</a>
                    </li>
                    <li class="{{Request::is('tin-tuc')?"active":""}}">
                        <a href="{{Request::is('tin-tuc')?"javascript:void(0);":url('tin-tuc')}}">Tin tức</a>
                    </li>
                    <li class="{{Request::is('lien-he*')?"active":""}}">
                        <a href="{{Request::is('lien-he*')?"javascript:void(0);":url('lien-he.html')}}">Liên hệ</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    </div>
    <div class="container-fluids" style="background:rgba(255,255,255,.9)">
        <div class="row" id="container">
            <div class="hidden hidden-xs hidden-sm">
                @include('partials.sidebar.left')
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8">
                @yield('slider')
                @if ($__env->yieldContent('content'))
                    @yield('content')
                @endif
            </div>
            <div class="col-xs-4 hidden-xs hidden-sm">
                @include('partials.sidebar.left')
            </div>
        </div>
    </div>
    <footer id="footer">
        <div style="border-top: 5px solid #6CB2E4">
            <ul class="flex-container footer-column">
                <li>
                    <div>
                        <h3>Thông tin</h3>
                        <ul class="footer-menu">
                            <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                            <li><a><i class="fa fa-info-circle"></i> Giới thiệu</a></li>
                            <li><a href="#"><i class="fa fa-book"></i> Chính sách</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div>
                        <h3>Liên kết website</h3>
                        <ul class="footer-menu">
                            <li><a href="#"><i class="fa fa-link"></i> 5giay.vn</a></li>
                            <li><a href="#"><i class="fa fa-link"></i> hoasen.com</a></li>
                            <li><a href="#"><i class="fa fa-link"></i> doigio.com</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div>
                        <h3>Kết nối DHP Việt Nam</h3>
                        <ul class="footer-menu">
                            <li><a href="#"><i class="fa fa-facebook-square"></i> Mạng xã hội Facebook</a></li>
                            <li><a href="#"><i class="fa fa-google-plus-square"></i> Mạng xã hội Google+</a></li>
                            <li><a href="#"><i class="fa fa-youtube-square"></i>Kênh Youtube</a></li>
                            <li><a href="#"><i class="fa fa-vimeo-square"></i> Trên Facebook</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div>
                        <h3>Hỗ trợ</h3>
                        <ul class="footer-menu">
                            <li><a href="#"><i class="fa fa-skype"></i> Hỗ trợ qua Skype</a></li>
                            <li><a href="#"><i class="fa fa-yahoo"></i> Hỗ trợ qua Yahoo</a></li>
                            <li><a href="#"><i class="fa fa-phone-square"></i>Hỗ trợ qua hotline</a></li>
                        </ul>
                    </div>
                </li>
                <li style="flex-grow: 2;display: flex;">
                    <div style="font-size: 18px;">
                        <h3>Liên hệ</h3>

                        <div><i class="fa fa-phone-square"></i> 08 629 68 444</div>
                        <div><i class="fa fa-home"></i> 3,Bạch Đằng, Q.Tân Bình</div>
                    </div>
                    <div style="padding: 5px;margin-top: 15px">
                        <p><i class="fa fa-user"></i> Đang online: 100</p>

                        <p><i class="fa fa-users"></i> Tổng lượt truy cập: 100</p>
                    </div>
                </li>
            </ul>

        </div>
    </footer>
    <div class="clearfix"></div>
    <nav class="navbar-inverse navbar-fixed-bottom" role="navigation">
        <div style="float: left"><a>DHP</a></div>
        <div style="overflow: hidden" class="marquee">
            <a>Thông báo, cập nhật tin tức</a>
        </div>
    </nav>
</div>
</div>
<!-- Scripts -->
<script src="/assets/global/plugins/jquery.min.js"></script>
<script src="/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/js/modernizr-transitions.js"></script>
<script src="/js/jquery.scrollto.js"></script>
<script src="/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="/assets/global/plugins/supersized/core/js/supersized.core.3.2.1.min.js" type="text/javascript"></script>
<!-- pop up -->
<script src="/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js"
type="text/javascript"></script>
<!-- slider for products -->
<script src='/assets/global/plugins/zoom/jquery.zoom.min.js' type="text/javascript"></script>
<!-- product zoom -->
<script src="/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<!-- Quantity -->
<script type="text/javascript" src="js/jquery.easing.min.js"></script>
<script src="/assets/global/plugins/jssor/js/jssor.slider.mini.js"></script>
<script src="/assets/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script src="/js/jmarquee.js"></script>
@yield('js_plugins')
@yield('js')
<script>
    $(document).ready(function ($) {
        Layout.initFixHeaderWithPreHeader();
        Layout.initNavScrolling();
        $.ajaxSetup({ cache: true });
        $.getScript('//connect.facebook.net/vi_VN/sdk.js', function(){
        	FB.init({
                appId: '{{Config::get("services.facebook.client_id")}}',
                xfbml: true,
                status:true,
                version: 'v2.3' 
            });
            FB.getLoginStatus(function(response) {            	
            	if (response.status === 'connected') {
            		var uid = response.authResponse.userID;
                    $(".dropdown-user img").attr("src","https://graph.facebook.com/v2.2/"+uid+"/picture?type=normal");
                    FB.api("/"+uid,function (response) {
                        if (response && !response.error) {
                            $(".dropdown-user .username").text(response.name);
                        }
                    });
                }else{
                    $.ajax({
                        url: '/social/logout',
                        type: 'get',
                        dataType: 'json',
                    });

                }
            });
        });        
                $.supersized({
                
                    // Functionality
                    start_slide             :   0,          // Start slide (0 is random)
                    new_window              :   1,          // Image links open in new window/tab
                    image_protect           :   1,          // Disables image dragging and right click with Javascript
                                                               
                    // Size & Position                         
                    min_width               :   0,          // Min width allowed (in pixels)
                    min_height              :   0,          // Min height allowed (in pixels)
                    vertical_center         :   1,          // Vertically center background
                    horizontal_center       :   1,          // Horizontally center background
                    fit_always              :   0,          // Image will never exceed browser width or height (Ignores min. dimensions)
                    fit_portrait            :   1,          // Portrait images will not exceed browser height
                    fit_landscape           :   0,          // Landscape images will not exceed browser width
                                                               
                    // Components
                    slides                  :   [           // Slideshow Images
                                                        {image : 'http://buildinternet.s3.amazonaws.com/projects/supersized/3.2/slides/kazvan-2.jpg', title : 'Image Credit: Maria Kazvan'},  
                                                ]
                    
                });
});
$('.marquee').marquee('pointer').mouseover(function () {
    $(this).trigger('stop');
}).mouseout(function () {
    $(this).trigger('start');
}).mousemove(function (event) {
    if ($(this).data('drag') == true) {
        this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
    }
}).mousedown(function (event) {
    $(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
}).mouseup(function () {
    $(this).data('drag', false);
});
</script>
</body>
</html>
