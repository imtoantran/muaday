@extends('site.layouts.default')
@section('content')
    <div class="col-md-9 col-xs-12  page-content">
        <div class="row">
            <div class="col-sm-8 col-md-8">
                <h1>@yield('title')</h1>
                @yield('page')
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="rows">
                    @widget('ArticleCategories',["cssClassForWrapper"=>"blog-sidebar"])
                    @widget('RecentNews',["cssClassForWrapper"=>"blog-sidebar"])
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @widget('sidebar',["cssClassForWrapper"=>"sidebar-right"])
    </div>
@stop