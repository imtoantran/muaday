<!DOCTYPE html>
<html>
@include('partials.site.head')
<body class="corporate">
@include('partials.site.topbar')
@include('partials.site.topmenu')
@yield('slider')
<div class="container-fluid" style="overflow: hidden">
    <div class="row">
        @yield("top-a")
        @yield("top-b")
        <!-- middle content -->
        <div class="main">
            <div class="container">
                @yield('inner-top')
                <div class="row">
                @yield('content')
                </div>
                @yield('inner-bottom')
            </div>
        </div>
        <!--/ middle content -->
        @yield("bottom-a")
        @yield("bottom-b")
        @include('partials.site.footer')
    </div>
</div>
@include('partials.site.foot')
</body>
</html>