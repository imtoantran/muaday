@extends('site.layouts.default')
@section("slider")
@if(count($products))
<div id="slides">
  <div class="slides_container">
    @foreach ($products as $product)
    <div>
      <a href='{!! url("/san-pham/$product->slug.html") !!}' class="product-image"><img src="/img/products/{!! $product->image[0] or '' !!}" alt="{!! $product->name !!}" /></a>
      <div class="caption"><p>{!! $product->name !!}</p></div>
      <button class="btn btn-xs btn-primary btn-share">
        <i class="fa fa-facebook-square"></i> <span>Chia sẻ</span>
      </button>
      <div class="slide-content">                        
        <div class="product-name">{!! $product->name !!}</div>  
        <div class="product-excerpt">{!! \Str::limit($product->excerpt,50) !!}</div>
        <div class="product-description">
          {!! \Str::limit($product->description,650) !!}
        </div>   
        <a href='{!! url("/san-pham/$product->slug.html") !!}'>           
          <div class="slide-contact">
            <div class="table">
             <div class="table-row">
              <div class="table-cell">
                <p class="price">
                  <strong style="color:red;">{!! number_format(($product->sale_price>0)?min($product->price,$product->sale_price):$product->price) !!}</strong> vnđ</p>
                  <h3 style="text-align:center;text-transform:uppercase;color:#eee;">Liên hệ tư vấn</h3>
                  <div style="color:#222;font-weight:bold;">
                    <p>(08)62968444</p>
                    <p>0938158086</p>                                       
                  </div>
                  <div style="text-align:center;color:#ddd;font-weight:bold;">Ms. YÊN</div>
                </div>                                      
              </div>                                  
            </div>
          </div>
        </a>
      </div>
    </div>
    @endforeach
  </div>
  <a href="#" class="prev">Previous</a>
  <a href="#" class="next">Next</a>
</div>
@endif
@stop
<!-- baners top -->
@section("banner-top")
<div class="baners_top">
  <div class="baner-item">
    <div class="baner-img"><a href="reservations.html"><img src="img/banners/baner_top_1.jpg" alt="" width="230" height="73" border="0" /></a></div>
    <h2>Sự kiện</h2>
  </div>

  <div class="baner-item">
    <div class="baner-img"><a href="rooms.html"><img src="/img/banners/gynos8h.png" alt="" width="230" height="73" border="0" /></a></div>
    <h2>Bán chạy</h2>
  </div>

  <div class="baner-item">
    <div class="baner-img"><a href="news.html"><img src="img/banners/baner_top_3.jpg" alt="" width="230" height="73" border="0" /></a></div>
    <h2>Khuyến mãi</h2>
  </div>
</div>    
@stop
<!--/ baners top -->
@section('bottom-a')
<div class="grid_12">
  <div class="news-item">
    <h2><a href="news-details.html">Giới thiệu DHP</a></h2>
    <div  class="news-text">
      <img src="images/temp_img_2.jpg" width="140" height="93" alt="" class="alignleft" />
      <p>Enjoy a striking destination inspired by the extraordinary landscape, melding the offerings of thrilling in outdoor recreation and renowned spa treatments, with the hot &amp; unsurpassed beauty of the City of Seven Hills. </p>
    </div>
  </div>
</div>
<div class="grid_12">
  <div class="news-item">
    <h2><a href="news-details.html">BED &amp; BREAKFAST</a></h2>
    <div  class="news-text">
      <img src="images/temp_img_1.jpg" width="140" height="93" alt="" class="alignleft" />
      <p>Indulge in our delicious cuisine infused with organic ingredients and prepared by expert chefs. We are providing a wide array of fine dining establishments offering stunning views from every turn in a relaxing and cosy atmosphere.</p>
    </div>
  </div>
</div>
@stop
@section("scripts")
<script type="text/javascript">
  $(function(){
   $('#slides').slides({
    width: 960,
    height: 379,
    preload: true,
    preloadImage: 'images/loading.gif',
    play: 5000,
    pause: 2500,
    effect: 'fade, fade',
    hoverPause: true,
    animationStart: function(){
     $('.slide-contact').animate({
      left:0,
    },300);    
     $('#slides .product-image').animate({
      "top":100,
    },400);    
   },
   animationComplete: function(current){
     $('.slide-contact').animate({
      left:-200
    },200);           
     $('#slides  .product-image').animate({
      "top":0
    },400);
   }
 });

 });
</script>
<script>
    jQuery(document).ready(function ($) {
        var options = { $AutoPlay: true };
        var slider = new $JssorSlider$('slider', options);
        //responsive code begin
        //you can remove responsive code if you don't want the slider scales
        //while window resizes
        function ScaleSlider() {
            var parentWidth = $('#slider').parent().width();
            if (parentWidth) {
                slider.$ScaleWidth(parentWidth);
            }
            else
                window.setTimeout(ScaleSlider, 30);
        }
        // Scale slider after document ready
        ScaleSlider();
                                        
        //Scale slider while window load/resize/orientationchange.
        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        //responsive code end
    });
</script>
@stop
@section("page_scripts")
<script type="text/javascript" src="/js/slides.jquery.js"></script>
@stop