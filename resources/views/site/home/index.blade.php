@extends('site.layouts.default')
@section('plugin-styles')
    <link href="/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
    <link href="/assets/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css" rel="stylesheet">
    <link href="/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
@stop
@section('theme-style')
    <!-- Theme styles START -->
    {{--<link href="/assets/global/css/components.css" rel="stylesheet">--}}
    {{--<link href="/assets/frontend/layout/css/style.css" rel="stylesheet">--}}
    <link href="/assets/frontend/pages/css/style-revolution-slider.css"
          rel="stylesheet"><!-- metronic revo slider styles -->
    {{--<link href="/assets/frontend/layout/css/style-responsive.css" rel="stylesheet">--}}
    {{--<link href="/assets/frontend/layout/css/themes/red.css" rel="stylesheet" id="style-color">--}}
    {{--<link href="/assets/frontend/layout/css/custom.css" rel="stylesheet">--}}
    <!-- Theme styles END -->
    <link href="/assets/frontend/pages/css/style-shop.css" rel="stylesheet" type="text/css">
@stop
@section("slider")
    @widget('slider',['cssClassForWrapper'=>'page-slider'])
@stop
@section('inner-top')
    @widget('InfoBlock')
@stop
@section('content')
    @widget('products',['cssClassForWrapper'=>'product-wrapper'])
@stop
@section("scripts")
    <script type="text/javascript">
        jQuery(document).ready(function () {
//            Layout.init();
            RevosliderInit.initRevoSlider();
//            Layout.initTwitter();
            //Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
            //Layout.initNavScrolling();
//            $('.grid').masonry({
//                // options
//                itemSelector: '.grid-item',
//                columnWidth: 200
//            });
            Layout.initOWL();
        });
    </script>
@stop
@section("page_scripts")
    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
    <!-- BEGIN RevolutionSlider -->

    <script src="/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"
            type="text/javascript"></script>
    <script src="/assets/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"
            type="text/javascript"></script>
    <script src="/assets/frontend/pages/scripts/revo-slider-init.js" type="text/javascript"></script>
    <!-- END RevolutionSlider -->
    <script src="/assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js"
            type="text/javascript"></script><!-- slider for products -->
@stop
@section('bottom-a')

@stop
@section('inner-bottom')
    @widget('services',["cssClassForWrapper"=>"row"])
@stop