@extends('site.layouts.app')
@section('slider')
    <div style="display: block; overflow: hidden; margin: 0 auto;  width: 100%;">
        <!-- Jssor Slider Begin -->
        <!-- You can move inline styles to css file or css block. -->
        <div id="slider2_container" style="position: relative;  float: left; top: 0px; left: 0px; width: 600px;
            height: 300px; overflow: hidden;">
            <!-- Slides Container -->
            <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 600px; height: 300px;
                overflow: hidden;">
                <div><img u="image" src="/img/home/slide1.jpg"/></div>
                <div><img u="image" src="/img/home/slide4.jpg"/></div>
                <div><img u="image" src="/img/home/slide3.jpg"/></div>
            </div>
            <!-- Bullet Navigator Skin Begin -->
            <style>
                /* jssor slider bullet navigator skin 21 css */
                /*
                .jssorb21 div           (normal)
                .jssorb21 div:hover     (normal mouseover)
                .jssorb21 .av           (active)
                .jssorb21 .av:hover     (active mouseover)
                .jssorb21 .dn           (mousedown)
                */
                .jssorb21 div, .jssorb21 div:hover, .jssorb21 .av
                {
                    background: url(../img/b21.png) no-repeat;
                    overflow:hidden;
                    cursor: pointer;
                }
                .jssorb21 div { background-position: -5px -5px; }
                .jssorb21 div:hover, .jssorb21 .av:hover { background-position: -35px -5px; }
                .jssorb21 .av { background-position: -65px -5px; }
                .jssorb21 .dn, .jssorb21 .dn:hover { background-position: -95px -5px; }
            </style>
            <!-- bullet navigator container -->
            <div u="navigator" class="jssorb21" style="position: absolute; bottom: 26px; left: 6px;">
                <!-- bullet navigator item prototype -->
                <div u="prototype" style="POSITION: absolute; WIDTH: 19px; HEIGHT: 19px; text-align:center; line-height:19px; color:White; font-size:12px;"></div>
            </div>
            <!-- Bullet Navigator Skin End -->

            <!-- Arrow Navigator Skin Begin -->
            <style>
                /* jssor slider arrow navigator skin 21 css */
                /*
                .jssora21l              (normal)
                .jssora21r              (normal)
                .jssora21l:hover        (normal mouseover)
                .jssora21r:hover        (normal mouseover)
                .jssora21ldn            (mousedown)
                .jssora21rdn            (mousedown)
                */
                .jssora21l, .jssora21r, .jssora21ldn, .jssora21rdn
                {
                    position: absolute;
                    cursor: pointer;
                    display: block;
                    background: url(../img/a21.png) center center no-repeat;
                    overflow: hidden;
                }
                .jssora21l { background-position: -3px -33px; }
                .jssora21r { background-position: -63px -33px; }
                .jssora21l:hover { background-position: -123px -33px; }
                .jssora21r:hover { background-position: -183px -33px; }
                .jssora21ldn { background-position: -243px -33px; }
                .jssora21rdn { background-position: -303px -33px; }
            </style>
            <!-- Arrow Left -->
        <span u="arrowleft" class="jssora21l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
            <!-- Arrow Right -->
        <span u="arrowright" class="jssora21r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
            <!-- Arrow Navigator Skin End -->
        </div>
    </div>
    <!-- Jssor Slider End -->
@endsection
@section('accordion_homepage')
    <div id="occasions">
        <div id="landing-sba">
            <div class="box-acc box3">
                <div class="bg">
                    <!--<img class="focus-bw" src="assets/images/wedding-bw.jpg" />-->
                    <img class="focus" src="/img/home/01.jpg">
                </div>
                <div class="frame-middle">
                    <div class="frame-middle-inner">
                        <div class="text">
                            <h2 class="title"><a href="wedding.php">Tiệc cưới</a></h2>

                            <div class="short-text">ALL YOU NEED IS LOVE. WE’LL DO THE REST</div>
                            <p class="gettouch"><a href="wedding-offer.php">ƯU ĐÃI</a></p>

                            <p class="box-gift"><a href="wedding-offer.php"><i class="i-box"></i></a></p>

                            <p class="small-text">Cập nhật thông tin ưu đãi mới nhất <br>dành cho bạn</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-acc box1">
                <div class="bg">        <!--<img class="focus-bw" src="assets/images/meeting-bw.jpg" />-->
                    <img class="focus" src="/img/home/02.jpg">
                </div>
                <div class="frame-middle">
                    <div class="frame-middle-inner">
                        <div class="text">
                            <h2 class="title"><a href="meetings.php">Hội nghị</a></h2>

                            <div class="short-text">WHERE BUSINESS AND PLEASURE COLLIDE</div>
                            <p class="gettouch"><a href="meeting-offer.php">ƯU ĐÃI</a></p>

                            <p class="box-gift"><a href="meeting-offer.php"><i class="i-box"></i></a></p>

                            <p class="small-text">Cập nhật thông tin ưu đãi mới nhất <br>dành cho bạn</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-acc box2">
                <div class="bg">        <!--<img class="focus-bw" src="assets/images/event-bw.jpg" />-->
                    <img class="focus" src="/img/home/03.jpg">
                </div>
                <div class="frame-middle">
                    <div class="frame-middle-inner">
                        <div class="text">
                            <h2 class="title"><a href="events.php">Sự kiện </a></h2>

                            <div class="short-text">OUR FIRST AIM IS TO FULFIL YOUR IDEAS</div>
                            <p class="gettouch"><a href="events-offers.php">ƯU ĐÃI</a></p>

                            <p class="box-gift"><a href="events-offers.php"><i class="i-box"></i></a></p>

                            <p class="small-text">Cập nhật thông tin ưu đãi mới nhất <br>dành cho bạn</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-acc box4">
                <div class="bg">        <!--<img class="focus-bw" src="assets/images/ouside-bw.jpg" />-->
                    <img class="focus" src="/img/home/04.jpg">
                </div>
                <div class="frame-middle">
                    <div class="frame-middle-inner">
                        <div class="text">
                            <h2 class="title"><a href="outside-catering.php">Tiệc ngoài</a></h2>

                            <div class="short-text">NOT ONLY SERVE YOU AT WHITE PALACE, WE'RE GLAD TO SERVE YOU
                                EVERYWHERE!
                            </div>
                            <p class="gettouch"><a href="outside-catering-offer.php">ƯU ĐÃI</a></p>

                            <p class="box-gift"><a href="outside-catering-offer.php"><i class="i-box"></i></a>
                            </p>

                            <p class="small-text">Cập nhật thông tin ưu đãi mới nhất <br>dành cho bạn</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-acc box5">
                <div class="bg">        <!--<img class="focus-bw" src="assets/images/ouside-bw.jpg" />-->
                    <img class="focus" src="/img/home/04.jpg">
                </div>
                <div class="frame-middle">
                    <div class="frame-middle-inner">
                        <div class="text">
                            <h2 class="title"><a href="outside-catering.php">Tiệc ngoài</a></h2>

                            <div class="short-text">NOT ONLY SERVE YOU AT WHITE PALACE, WE'RE GLAD TO SERVE YOU
                                EVERYWHERE!
                            </div>
                            <p class="gettouch"><a href="outside-catering-offer.php">ƯU ĐÃI</a></p>

                            <p class="box-gift"><a href="outside-catering-offer.php"><i class="i-box"></i></a>
                            </p>

                            <p class="small-text">Cập nhật thông tin ưu đãi mới nhất <br>dành cho bạn</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        jQuery(document).ready(function ($) {
            //Reference http://www.jssor.com/development/tip-make-responsive-slider.html

            var options = {
//                $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actual size, 5 contain for large image, actual size for small image, default value is 0
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuint,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: 800,                               //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $CaptionSliderOptions: {                            //[Optional] Options which specifies how to animate caption
                    $Class: $JssorCaptionSlider$,                   //[Required] Class to create instance to animate caption
//                    $CaptionTransitions: _CaptionTransitions,       //[Required] An array of caption transitions to play caption, see caption transition section at jssor slideshow transition builder
                    $PlayInMode: 1,                                 //[Optional] 0 None (no play), 1 Chain (goes after main slide), 3 Chain Flatten (goes after main slide and flatten all caption animations), default value is 1
                    $PlayOutMode: 3                                 //[Optional] 0 None (no play), 1 Chain (goes before main slide), 3 Chain Flatten (goes before main slide and flatten all caption animations), default value is 1
                },

                $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 8,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 8,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                },

                $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };
            var jssor_slider2 = new $JssorSlider$("slider2_container", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {

                //reserve blank width for margin+padding: margin+padding-left (10) + margin+padding-right (10)
                var paddingWidth = 0;

                //minimum width should reserve for text
                var minReserveWidth = 225;

                var parentElement = jssor_slider2.$Elmt.parentNode;

                //evaluate parent container width
                var parentWidth = parentElement.clientWidth;
                console.log(parentWidth);
                var r = jssor_slider2.$Elmt.clientWidth / jssor_slider2.$Elmt.clientHeight;
                parentElement.Height = 333;
                if (parentWidth) {

                    //exclude blank width
                    var availableWidth = parentWidth - paddingWidth;

                    //calculate slider width as 70% of available width
                    var sliderWidth = availableWidth * 0.7;

                    //slider width is maximum 600
                    sliderWidth = Math.min(sliderWidth, 600);

                    //slider width is minimum 200
                    sliderWidth = Math.max(sliderWidth, 200);
                    var clearFix = "none";

                    //evaluate free width for text, if the width is less than minReserveWidth then fill parent container
                    if (availableWidth - sliderWidth < minReserveWidth) {

                        //set slider width to available width
                        sliderWidth = availableWidth;

                        //slider width is minimum 200
                        sliderWidth = Math.max(sliderWidth, 200);

                        clearFix = "both";
                    }

                    //clear fix for safari 3.1, chrome 3
                    $('#clearFixDiv').css('clear', clearFix);
                    jssor_slider2.$ScaleWidth(parentWidth - paddingWidth);
                }
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>
@endsection