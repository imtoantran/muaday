<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                <form class="sidebar-search " action="extra_search.html" method="POST">
                    <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                    </a>

                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
                    </div>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
            {{-- Admin dashboard begin --}}
            <li class="{{{ Request::is('admin/dashboard*')?"start active":"" }}}">
                <a href="{{URL::to('admin/dashboard')}}">
                    <i class="icon-home"></i>
                    <span class="title">Bảng tin</span>
                    <span class="selected"></span>
                </a>
            </li>
            {{-- Admin dashboard end --}}
            {{-- Admin Facebook members begin --}}
            <li class = "{{{ Request::is('admin/social*')?"start active":"" }}}">
                <a href="{{URL::to("admin/social")}}">
                    <i class="icon-social-facebook"></i>
                    <span class="title">Facebook</span>
                    @if(Request::is('admin/social*'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            {{-- Admin Facebook members end --}}
            {{-- Admin products begin --}}
            <li class = "{{{ Request::is('admin/products*')?"start active open":"" }}}">
                <a href="javascript:;">
                    <i class="icon-basket"></i>
                    <span class="title">Sản phẩm</span>

                    @if(Request::is('admin/products*'))
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li {{Request::is('admin/products')?"class=active":""}}>
                        <a href="{{URL::to('admin/products')}}">
                            <i class="icon-home"></i> Tất cả
                        </a>
                    </li>
                    <li {{Request::is('admin/products/category')?"class=active":""}}>
                        <a href="{{URL::to('admin/products/category')}}">
                            <i class="icon-home"></i> Danh mục
                        </a>
                    </li>
                </ul>
            </li>
            {{-- Admin products end --}}
            {{-- Admin news begin --}}
            <li class = "{{{ Request::is('admin/news*')?"start active open":"" }}}">
                <a href="javascript:;">
                    <i class="icon-info"></i>
                    <span class="title">Tin tức</span>

                    @if(Request::is('admin/news*'))
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li {{Request::is('admin/news')?"class=active":""}}>
                        <a href="{{URL::to('admin/news')}}">
                            <i class="icon-list"></i> Tất cả
                        </a>
                    </li>
                    <li {{Request::is('admin/news/category')?"class=active":""}}>
                        <a href="{{URL::to('admin/news/category')}}">
                            <i class="icon-credit-card"></i> Danh mục
                        </a>
                    </li>
                </ul>
            </li>
            {{-- Admin news end --}}
            {{-- Admin hình ảnh begin --}}
            <li class = "{{{ Request::is('admin/photo*')?"start active open":"" }}}">
                <a href="javascript:;">
                    <i class="icon-info"></i>
                    <span class="title">Hình ảnh</span>

                    @if(Request::is('admin/photo*'))
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li {{Request::is('admin/photo')?"class=active":""}}>
                        <a href="{{URL::to('admin/photo')}}">
                            <i class="icon-list"></i> Tất cả
                        </a>
                    </li>
                    <li {{Request::is('admin/photo/album')?"class=active":""}}>
                        <a href="{{URL::to('admin/photo/album')}}">
                            <i class="icon-credit-card"></i> Album
                        </a>
                    </li>
                </ul>
            </li>
            {{-- Admin hình ảnh end --}}
            {{-- Admin video begin --}}
            <li class = "{{{ Request::is('admin/slider*')?"start active open":"" }}}">
                <a href="javascript:;">
                    <i class="icon-info"></i>
                    <span class="title">Video</span>

                    @if(Request::is('admin/news*'))
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li {{Request::is('admin/news')?"class=active":""}}>
                        <a href="{{URL::to('admin/news')}}">
                            <i class="icon-list"></i> Tất cả
                        </a>
                    </li>
                    <li {{Request::is('admin/news/category')?"class=active":""}}>
                        <a href="{{URL::to('admin/news/category')}}">
                            <i class="icon-credit-card"></i> Danh mục
                        </a>
                    </li>
                </ul>
            </li>
            {{-- Admin video end --}}
            {{-- Admin banner begin --}}
            <li class = "{{{ Request::is('admin/banners*')?"start active open":"" }}}">
                <a href="{{URL::to('admin/banners/home')}}">
                    <i class="icon-home"></i>
                    <span class="title">Banner trang chủ</span>

                    @if(Request::is('admin/banners*'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            {{-- Admin banner end --}}
            <li {{Request::is('admin/sidebars*')?"class=active":""}}>
                <a href="{{URL::to('admin/sidebars/')}}">
                    <i class="icon-credit-card"></i> Banner Phải
                </a>
            </li>
            {{-- Admin services begin --}}
            <li class = "{{{ Request::is('admin/services*')?"start active":"" }}}">
                <a href="/admin/services">
                    <i class="icon-info"></i>
                    <span class="title">Quản lý dịch vụ</span>
                    @if(Request::is('admin/services*'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            {{-- Admin services end --}}
            {{-- Admin user start --}}
            <li class = "{{{ Request::is('admin/users*')?"start active":"" }}}">
                <a href="/admin/users">
                    <i class="icon-info"></i>
                    <span class="title">Người dùng</span>
                    @if(Request::is('admin/users*'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            {{-- Admin user end --}}
            {{-- Admin page begin --}}
            <li class = "{{{ Request::is('admin/page*')?"start active":"" }}}">
                <a href="{{url("admin/page")}}">
                    <i class="icon-info"></i>
                    <span class="title">Trang</span>

                    @if(Request::is('admin/page'))
                        <span class="selected"></span>
                    @endif
                </a>
            </li>
            {{-- Admin page end --}}
            {{-- Admin settings begin --}}
            <li class = "{{{ Request::is('admin/slider*')?"start active open":"" }}}">
                <a href="javascript:;">
                    <i class="icon-settings"></i>
                    <span class="title">Cài đặt</span>

                    @if(Request::is('admin/news*'))
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    @else
                        <span class="arrow"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li {{Request::is('admin/news')?"class=active":""}}>
                        <a href="{{URL::to('admin/news')}}">
                            <i class="icon-list"></i> Trang chủ
                        </a>
                    </li>
                    <li {{Request::is('admin/news/category')?"class=active":""}}>
                        <a href="{{URL::to('admin/news/category')}}">
                            <i class="icon-credit-card"></i> Giới thiệu
                        </a>
                    </li>
                    <li {{Request::is('admin/news/category')?"class=active":""}}>
                        <a href="{{URL::to('admin/news/category')}}">
                            <i class="icon-credit-card"></i> Liên hệ
                        </a>
                    </li>
                    <li {{Request::is('admin/settings/password')?"class=active":""}}>
                        <a href="{{URL::to('admin/settings/password')}}">
                            <i class="icon-credit-card"></i> Đổi mật khẩu
                        </a>
                    </li>
                </ul>
            </li>
            {{-- Admin settings end --}}
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->