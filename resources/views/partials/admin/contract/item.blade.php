<tr data-id="{{$contract->id}}">
    <td colspan="" rowspan="" headers="" width="5">{{isset($key)?$key+1:"-"}}</td>
    <td colspan="" rowspan="" headers="">{{\Carbon\Carbon::parse($contract->start_date)->format("d-m-Y")}}</td>
    <td colspan="" rowspan="" headers="">{{\Carbon\Carbon::parse($contract->end_date)->format("d-m-Y")}}</td>
    <td colspan="" rowspan="" headers="">{{$contract->contract_no}}</td>
    <td colspan="" rowspan="" headers="" width="10">
        <button type="button" class="btn btn-xs btn-danger delete"></i> Xóa</button>
    </td>
</tr>