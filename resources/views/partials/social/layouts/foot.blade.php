<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/assets/global/plugins/respond.min.js"></script>
<script src="/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
{{-- <script src="/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script> --}}
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
{{-- <script src="/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> --}}
<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>
{{-- <script src="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>
 --}}<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
{{-- <script type="text/javascript" src="/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/assets/global/plugins/jquery-validation/js/localization/messages_vi.min.js"></script>
<script src="/assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="/assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
 --}}
 {{-- <script type="text/javascript" src="/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> --}}
{{-- <script src="/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script> --}}
{{-- <script src="/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script> --}}
@yield('page_level_script_plugins')
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{{-- <script src="/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="/assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script> --}}
@yield('page_level_scrips')
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {
            $.ajaxSetup({ cache: true });
            $.getScript('//connect.facebook.net/vi_VN/sdk.js', function(){
                FB.init({
                    appId: '{{Config::get("services.facebook.client_id")}}',
                    xfbml: true,
                    status:true,
                    version: 'v2.3' 
                });
                FB.getLoginStatus(function(response) {
                  if (response.status === 'connected') {
                    // the user is logged in and has authenticated your
                    // app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed
                    // request, and the time the access token 
                    // and signed request each expire
                    var uid = response.authResponse.userID;
                    if(uid != {{\Session::get('social')->id}})
                        $.ajax({
                            url: '/social/logout',
                            type: 'get',
                            dataType: 'json'
                        }).done(function(){alert("Bạn cần đăng nhập lại.");location.href="{{url("dang-nhap")}}"});
                        FB.api("/"+uid,function (response) {
                              if (response && !response.error) {
                                /* handle the result */
                                $(".dropdown-user .username").text(response.name);
                              }
                            }
                        );
                        $(".dropdown-user img").attr("src","https://graph.facebook.com/v2.2/"+uid+"/picture?type=normal");
                        var accessToken = response.authResponse.accessToken;
                    } else {
                            // the user isn't logged in to Facebook.
                            $.ajax({
                                    url: '/social/logout',
                                    type: 'get',
                                    dataType: 'json'
                            })
                            .done(function() {
                                 alert("Bạn cần đăng nhập lại.");location.href="{{url("dang-nhap")}}"
                            })                            
                        }
                 });
                 @yield('share_scripts')
            });        
    });
</script>
@yield('scripts')
<!-- END JAVASCRIPTS -->