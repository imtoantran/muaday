<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@yield('title')</title>
    <meta name="author" content="DHP Vietnam" />    
    <meta name="keywords" content="gynos8h" />
    @yield('meta')
    <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
    @yield('plugin-styles')
    <link rel="stylesheet" type="text/css" href="/assets/global/css/components.css" />
    <link rel="stylesheet" type="text/css" href="/assets/frontend/layout/css/style.css" />
    @yield('theme-style')
    <link rel="stylesheet" type="text/css" href="/assets/frontend/layout/css/style-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/assets/frontend/layout/css/themes/blue.css" />
    <link rel="stylesheet" type="text/css" href="/assets/global/css/plugins.css" />
    <link rel="stylesheet" type="text/css" href="/styles.css" />
    {{--<link rel="stylesheet" type="text/css" href="/css/slides.css" />--}}
    <link rel="stylesheet" type="text/css" href="/custom.css" />
</head>