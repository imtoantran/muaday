<div class="grid_6">

    <div class="box box_black">
        <div class="inner">
        <h3 class="bordered"><span>Make a</span> Quang cao</h3>
            <p><span class="text-green"><a href="reservations.html">Send us a message</a></span> with your desired room type, arrival date and number of nights you'd like to reserve, and we'll contact you in no time.</p>
        </div>
        <a href="reservations.html"><img src="images/calendar_230.jpg" alt="" border="0" height="73" width="230"></a>           </div>

        <div class="box box_gray widget-container widget_recent_entries">
            <h3><span>Latest</span> News &amp; Promos</h3>
            <ul>
                <li class="even"><a href="news-details.html"><img src="images/temp_thumb_1.jpg" alt="" class="thumbnail" border="0" height="50" width="50"></a> <a href="news-details.html">NEW jumbo breakfast in bed included for all guests! </a> <div class="date">Feb 23, 2011</div> </li>
                <li><a href="news-details.html"><img src="images/temp_thumb_2.jpg" alt="" class="thumbnail" border="0" height="50" width="50"></a> <a href="news-details.html">Awesome new pool on the west side entrance!</a> <div class="date">Feb 11, 2011</div> </li>
                <li class="even"><a href="news-details.html"><img src="images/temp_thumb_3.jpg" alt="" class="thumbnail" border="0" height="50" width="50"></a> <a href="news-details.html">Further reductions for cash checkout transactions</a> <div class="date">Feb 23, 2011</div> </li>
                <li><a href="news-details.html"><img src="images/temp_thumb_4.jpg" alt="" class="thumbnail" border="0" height="50" width="50"></a> <a href="news-details.html">Further reductions for cash checkout transactions</a> <div class="date">Dec 18, 2010</div> </li>
            </ul>
        </div>
    </div>       
