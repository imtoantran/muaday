<!-- BEGIN TOP BAR -->
<div class="pre-header">
    <div class="container">
        <div class="row">
            <!-- BEGIN TOP BAR LEFT PART -->
            <div class="col-md-6 col-sm-6 additional-shop-info">
                <ul class="list-unstyled list-inline">
                    {{--<li><i class="fa fa-phone"></i><span></span></li>--}}
                    <li><i class="fa fa-envelope-o"></i><span>info@muaday.com</span></li>
                </ul>
            </div>
            <!-- END TOP BAR LEFT PART -->
            <!-- BEGIN TOP BAR MENU -->
            <div class="col-md-6 col-sm-6 additional-nav user-panel">
                <ul class="list-unstyled list-inline pull-right">
                    <li class="controls hidden">
                        <span>Đăng nhập </span>
                        <a class="btn btn-xs fb-logins blue" href="/social/login/facebook">Facebook <i class="fa fa-facebook-square"></i></a>
                        <button class="btn btn-xs red">Google <i class="fa fa-google-plus-square"></i></button>
                    </li>
                    <li class="welcome hidden">
                        <div class="dropdown">
                          <a class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                            <span class="text"></span>
                            <span class="caret"></span>
                          </a>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                            <li role="presentation" class="dropdown-header"></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/social/history">Lịch sử chia sẻ</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/social">Tin chia sẻ</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" class="fb-logout">Đăng xuất</a></li>
                              <li></li>
                          </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- END TOP BAR MENU -->
        </div>
    </div>
</div>
<!-- END TOP BAR -->
