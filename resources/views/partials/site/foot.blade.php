<script type="text/javascript" src="/js/jquery.min.js"></script>
<script src="/assets/global/plugins/jquery.cokie.min.js"></script>
<script type="text/javascript" src="/js/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script src="/assets/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
@yield("plugin_script")

<script src="/assets/global/plugins/jquery.easing.js"></script>
<script src="/assets/global/plugins/jquery.parallax.js"></script>
<script src="/assets/global/plugins/jquery.scrollTo.min.js"></script>
<script src="/assets/frontend/onepage/scripts/jquery.nav.js"></script>
<script src="/assets/frontend/layout/scripts/layout.js" type="text/javascript"></script>
<script src="/assets/frontend/onepage/scripts/layout.js" type="text/javascript"></script>

@yield("page_scripts")
<script>
    $(document).ready(function () {
        var fbUser = {};
        Onepage.init();
        Layout.init();
        Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
//        Layout.initNavScrolling();
        $.ajaxSetup({cache: true});
        $.getScript('//connect.facebook.net/vi_VN/sdk.js', function () {
            FB.init({
                appId: '{!! Config::get("services.facebook.client_id") !!}',
                version: 'v2.3',
                xfbml: true,
                status: true
            });
            var fblogincallback = function (response) {
                if (response.status === 'connected') {
                    console.log(response);
                    if($.cookie("fbAccessToken")==null)
                    $.cookie("fbAccessToken",response.authResponse.accessToken);
                    if($.cookie("fbUserID")==null)
                    $.cookie("fbUserID",response.authResponse.userID);
                    if($.cookie("fbSignedRequest")==null)
                    $.cookie("fbSignedRequest",response.authResponse.signedRequest);
                    FB.api('/me', {fields: 'picture,name'}, function (response) {
                        $(".user-panel .text").html("<i>Xin chào </i><b class='font-blue'>" + response.name + "</b>");
//                        $(".user-panel .welcome img").attr({'src': response.picture.data.url});
                        $(".user-panel .welcome").removeClass('hidden');
                        $(".user-panel .controls").addClass('hidden');
                    });
                }
                else {
                    console.log("not login");
                    $(".user-panel .controls").removeClass('hidden');
                    $(".user-panel .welcome").addClass('hidden');
                    $.cookie("fbAccessToken",null,{path:'/'});
                    $.cookie("fbUserID",null,{path:'/'});
                    $.cookie("fbSignedRequest",null,{path:'/'});
                    $.removeCookie("fbAccessToken",{path:'/'});
                    $.removeCookie("fbUserID",{path:'/'});
                    $.removeCookie("fbSignedRequest",{path:'/'});
                }
            };
            $(".user-panel").on('click', '.fb-logout', function (event) {
                event.preventDefault();
                FB.logout();
            });
            $(".user-panel").on('click', '.fb-login', function (event) {
                event.preventDefault();
                FB.login(function(){},{scope: 'publish_actions,email,user_likes,user_friends', return_scopes: true});
            });
            $('#loginbutton,#feedbutton').removeAttr('disabled');
            $(".btn-share").on("click", function () {
                FB.ui({
                    method: 'feed',
                    link: 'https://developers.facebook.com/docs/',
                    caption: 'An example caption'
                }, function (response) {
                    if (response.post_id) {
                        alert("Chúc mừng bạn đã chia sẻ thành công trên DHP");
                    }
                });
            })
            FB.getLoginStatus(fblogincallback);
            FB.Event.subscribe('auth.login', function (response) {
                // do something with response
                console.log('auth.login')
                fblogincallback(response);
                $.ajax({

                });
            });
            FB.Event.subscribe('auth.logout', function (response) {
                // do something with response
                fblogincallback(response);
                console.log('auth.logout');
            });
            FB.Event.subscribe('auth.authResponseChange', function (response) {
                // do something with response
                console.log('authResponseChange');
            });
            FB.Event.subscribe('auth.statusChange', function (response) {
                // do something with response
                console.log('statusChange');
            });
            @yield('share_scripts')
        });

toastr.options = {
  "closeButton": true,
  "debug": false,
  "positionClass": "toast-top-right",
  "onclick": null,
  "showDuration": "1000",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
//toastr.success('Have fun storming the castle!', 'Miracle Max Says')

    });
</script>
@yield("scripts")
