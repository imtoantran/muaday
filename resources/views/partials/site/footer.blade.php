<!-- BEGIN PRE-FOOTER -->
<div class="pre-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 pre-footer-col">
                <h2>Thông tin</h2>
                <ul class="list-unstyled">
                    <li><i class="fa fa-angle-right"></i> <a href="#">Thông tin giao hàng</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#">Liên hệ</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#">Tuyển dụng</a></li>
                    <li><i class="fa fa-angle-right"></i> <a href="#">Phương thức thanh toán</a></li>
                </ul>
            </div>            <!-- BEGIN BOTTOM ABOUT BLOCK -->
            <!-- END BOTTOM ABOUT BLOCK -->

            <!-- BEGIN BOTTOM CONTACTS -->
            <div class="col-md-4 col-sm-6 pre-footer-col">
                <h2>Liên hệ</h2>
                <address class="margin-bottom-40">
                    Email: <a href="mailto:info@muaday.com">info@muaday.com</a><br>
                    Skype: <a href="skype:muaday">muaday</a>
                </address>

            </div>
            <!-- END BOTTOM CONTACTS -->

            <!-- BEGIN TWITTER BLOCK -->
            <div data-twttr-id="twttr-sandbox-0" class="col-md-4 col-sm-6 pre-footer-col">
                <div>
                    <h2>Đăng ký nhận bản tin</h2>
                    <form action="#">
                        <div class="input-group">
                            <input placeholder="Nhập email" class="form-control" type="text">
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Đăng ký</button>
                  </span>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END TWITTER BLOCK -->
        </div>
    </div>
</div>
<!-- END PRE-FOOTER -->

<!-- BEGIN FOOTER -->
<div class="footer padding-top-15">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-6 col-sm-6 padding-top-10">
                2015 © MUADAY
            </div>
            <!-- END COPYRIGHT -->
            <!-- BEGIN PAYMENTS -->
            <div class="col-md-6 col-sm-6">
                <ul class="social-footer list-unstyled list-inline pull-right">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
            <!-- END PAYMENTS -->
        </div>
    </div>
</div>
<!-- END FOOTER -->