<div class="header">
    <div class="container">
        <a class="site-logo" href="/"><img src="/img/home/logo.png" alt="" height="40" border="0"/></a>
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>
        <div class="header-navigation pull-right font-transform-inherit">
            <ul class="header-navigation">
                <li {{{ Request::is("gioi-thieu.html")?"class=active":"" }}}>
                    <a href="/gioi-thieu.html">
                        Giới thiệu
                    </a>
                </li>
                <li>
                    <a href="/#san-pham">
                        Sản phẩm
                    </a>
                </li>
                <li>
                    <a href="/#services">
                        Dịch vụ
                    </a>
                </li>
                <li class="dropdown {{Request::is('tin-tuc*')?"active":""}}">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">
                        Tin tức
                    </a>
                    @if(Cache::has('newscategories'))
                        <ul class="dropdown-menu">
                            @foreach(Cache::get('newscategories') as $category)
                                <li class="{{Request::is("/tin-tuc/$category->slug")?"current":""}}" ><a href="/tin-tuc/{{$category->slug}}">{!! $category->title !!}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </li>
                <li {{Request::is("lien-he.html")?"class=active":""}}>
                    <a href="/lien-he.html">
                        Liên hệ
                    </a>
                </li>
                <!-- BEGIN TOP SEARCH -->
                <li class="menu-search">
                    <span class="sep"></span>
                    <i class="fa fa-search search-btn show-search-icon"></i>

                    <div class="search-box">
                        <form action="#">
                            <div class="input-group">
                                <input placeholder="Search" class="form-control" type="text">
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit">Search</button>
                    </span>
                            </div>
                        </form>
                    </div>
                </li>
                <!-- END TOP SEARCH -->
            </ul>
        </div>
    </div>
</div>
