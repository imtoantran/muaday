@section("slider")
@if(count($products))
<div id="slider" style="position: relative; top: 0px; left: 0px; width: {{$sliderwidth or 600}}px; height: {{$300}}px;">
    <!-- Slides Container -->
    <div u="slides" style="cursor: move; position: absolute; overflow: hidden; left: 0px; top: 0px; width: 600px; height: 300px;">
    @foreach ($products as $product)
    <div>
        <img u="image" src="/img/products/{!! $product->image[0] or '' !!}" alt="{!! $product->name !!}" />
    </div>
    @endforeach
    </div>
</div>
@endif
@stop@section("slider-bk")
@if(count($products))
<div id="slides">
  <div class="slides_container">
    @foreach ($products as $product)
    <div>
      <a href='{!! url("/san-pham/$product->slug.html") !!}' class="product-image">
        <img src="/img/products/{!! $product->image[0] or '' !!}" alt="{!! $product->name !!}" />
      </a>
      <div class="caption"><p>{!! $product->name !!}</p></div>
    </div>
    @endforeach
  </div>
  <a href="#" class="prev">Previous</a>
  <a href="#" class="next">Next</a>
</div>
@endif
@stop