<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>DHP Viet Nam</title>
    <!-- Fonts START -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=all" rel="stylesheet"
          type="text/css">
    <!-- Fonts END -->
    <!-- Global styles START -->
    <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Global styles END -->    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/css/style.css">
    @yield('css_plugins')
    @yield('css')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="dhp">
<div class="top-bar">
    <a href="/"><img src="/images/logo.jpg" class="img-responsive" alt="DHP Việt Nam"></a>

    <div class="banner-top">
        <img src="/images/banner_top.png" class="img-responsive" alt="Image">
    </div>
    <div class="user-panel">
        <img src="https://graph.facebook.com/v2.2/458641130979464/picture?type=normal"
             class="profile-picture img-circle" alt="">

        <div class="user-panel-info">
            <div class="auth">
                <div></div>
                <a href="">Đăng nhập</a>

                <div></div>
            </div>
            <div class="user-guide">
                <div class="welcome"><i class="fa fa-angelist"></i> Hi, Toan Tran!</div>
                <div class="link">
                    <a href="#"><i class="fa fa-angelist"></i> Lịch sử chia sẻ</a>
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-dhp">
    <ul class="nav navbar-nav">
        <li>
            <a href="#" class="active">Giới thiệu</a>
        </li>
        <li>
            <a href="#">Sản phẩm</a>
        </li>
        <li>
            <a href="#">Tuyển dụng</a>
        </li>
        <li>
            <a href="#">Thư viện ảnh</a>
        </li>
        <li>
            <a href="#">Tin tức sự kiện</a>
        </li>
        <li>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Liên hệ <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
            </ul>
        </li>
    </ul>
</nav>
<div class="col-md-2 col-lg-2">
    @include('partials.sidebar.left')
</div>
<div class="col-md-8">
    @yield('slider')
    @yield('content')
</div>
<div class="col-md-2">
    @include('partials.sidebar.left')
</div>
<div class="clearfix">

</div>
<footer id="footer">
    <div style="border-top: 5px solid #6CB2E4">
        <ul class="flex-container footer-column">
            <li>
                <div>
                    <h3>Thông tin</h3>
                    <ul class="footer-menu">
                        <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                        <li><a><i class="fa fa-info-circle"></i> Giới thiệu</a></li>
                        <li><a href="#"><i class="fa fa-book"></i> Chính sách</a></li>
                        <li><a href="#"><i class="fa fa-volume-up"></i> Điều khoản</a></li>
                    </ul>
                </div>
            </li>
            <li>
                <div>
                    <h3>Liên kết website</h3>
                    <ul class="footer-menu">
                        <li><a href="#"><i class="fa fa-link"></i> 5giay.vn</a></li>
                        <li><a href="#"><i class="fa fa-link"></i> hoasen.com</a></li>
                        <li><a href="#"><i class="fa fa-link"></i> doigio.com</a></li>
                    </ul>
                </div>
            </li>
            <li>
                <div>
                    <h3>Kết nối DHP Việt Nam</h3>
                    <ul class="footer-menu">
                        <li><a href="#"><i class="fa fa-facebook-square"></i> Mạng xã hội Facebook</a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square"></i> Mạng xã hội Google+</a></li>
                        <li><a href="#"><i class="fa fa-youtube-square"></i>Kênh Youtube</a></li>
                        <li><a href="#"><i class="fa fa-vimeo-square"></i> Trên Facebook</a></li>
                    </ul>
                </div>
            </li>
            <li>
                <div>
                    <h3>Hỗ trợ</h3>
                    <ul class="footer-menu">
                        <li><a href="#"><i class="fa fa-skype"></i> Hỗ trợ qua Skype</a></li>
                        <li><a href="#"><i class="fa fa-yahoo"></i> Hỗ trợ qua Yahoo</a></li>
                        <li><a href="#"><i class="fa fa-phone-square"></i>Hỗ trợ qua hotline</a></li>
                    </ul>
                </div>
            </li>
            <li style="flex-grow: 2;display: flex;">
                <div style="font-size: 18px;">
                    <h3>Liên hệ</h3>
                    <div><i class="fa fa-phone-square"></i> 08 629 68 444</div>
                    <div><i class="fa fa-home"></i> 3,Bạch Đằng, Q.Tân Bình </div>
                </div>
                <div style="padding: 5px;margin-top: 15px">
                    <p><i class="fa fa-user"></i> Đang online: 100</p>
                    <p><i class="fa fa-users"></i> Tổng lượt truy cập: 100</p>
                </div>
            </li>
        </ul>

    </div>
</footer>
<div class="clearfix">

</div>
<nav class="navbar-inverse navbar-fixed-bottom" role="navigation">
    <div style="float: left"><a>DHP</a></div>
    <div style="overflow: hidden" class="marquee">
        <a>Thông báo, cập nhật tin tức</a>
    </div>
</nav>
<!-- Scripts -->
<script src="/assets/global/plugins/jquery.min.js"></script>
<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
{{--<script src="/js/jquery.hoverIntent.minified.js"></script>--}}
<script src="/js/modernizr-transitions.js"></script>
<script src="/js/jquery.scrollto.js"></script>
{{--<script src="/js/accordion-homepage.js"></script>--}}
<script type="text/javascript" src="js/jquery.easing.min.js"></script>
<script src="/assets/global/plugins/jssor/js/jssor.slider.mini.js"></script>
<script src="/js/jmarquee.js"></script>
@yield('js_plugins')
@yield('js')
<script>
    $('.marquee').marquee('pointer').mouseover(function () {
        $(this).trigger('stop');
    }).mouseout(function () {
        $(this).trigger('start');
    }).mousemove(function (event) {
        if ($(this).data('drag') == true) {
            this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
        }
    }).mousedown(function (event) {
        $(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
    }).mouseup(function () {
        $(this).data('drag', false);
    });
</script>
</body>
</html>
