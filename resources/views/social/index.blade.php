@extends('site.layouts.default')
@section('page_title')
Tin chia sẻ
@stop
@section('content')
@if(count($products))
<div class="share-wrapper row">
    @foreach($products as $key => $product)

        <div class="col-sm-4 col-md-4 col-lg-4">
            <div class="share-item">
                @if(count($product->image))
                <div class="text-center">
                    <img src="/img/products/{{ $product->image[0] }}" alt="{{$product->name}}">
                </div>
                @endif
                <div class="share-item-info">
                    <a href="/san-pham/{{$product->slug}}.html">{{$product->name}}</a>

                    <div class="pi-price">{{number_format($product->price)}}đ</div>
                </div>
                <a class="share font-red" data-product_id="{{$product->id}}" data-caption="{{$product->name}}"
                   data-link='{{URL::to("san-pham/$product->slug.html")}}'><i class="fa fa-money"></i> Chia sẻ</a>
            </div>
        </div>

    @endforeach
    {!! $products->render() !!}
</div>
@endif
@stop
@section('share_scripts')
$(".share-wrapper").on("click",".share",function(){
var _ = this;
var data = {"method":"feed"};
data.link = $(_).data("link");
data.caption = $(_).data("caption");
data.product_id = $(_).data("product_id");
FB.ui(data, function(response){
response._token = "{{Session::token()}}";
$.ajax({
url:"/social/share",
type:"post",
dataType:"json",
data:response,
success:function (response) {
alert(response.message);
}
})
});
});
@stop