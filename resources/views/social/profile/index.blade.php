@extends('social.layouts.default')
@section('page_title')
    Hồ sơ cá nhân
    @stop
@section('content')
    <div class="row margin-top-20">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img src="https://graph.facebook.com/v2.2/{{$social->id}}/picture?type=large"
                             class="img-circle img-responsive" alt="">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            {{$social->name}}
                        </div>
                        <div class="profile-usertitle-job">
                            Thành viên
                        </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                </div>
                <!-- END PORTLET MAIN -->
                <!-- PORTLET MAIN -->
                <div class="portlet light">
                    <!-- STAT -->
                    <div class="row list-separated profile-stat">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="uppercase profile-stat-title">
                                {{$total_contract}}
                            </div>
                            <div class="uppercase profile-stat-text">
                                Hợp đồng
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="uppercase profile-stat-title">
                                {{$total_share}}
                            </div>
                            <div class="uppercase profile-stat-text">
                                Lượt chia sẻ
                            </div>
                        </div>
                    </div>
                    <!-- END STAT -->
                </div>
                <!-- END PORTLET MAIN -->
            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PORTLET -->
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption caption-md">
                                    <i class="icon-bar-chart theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Thu nhập</span>
                                    <span class="caption-helper hide">weekly stats...</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row number-stats margin-bottom-30">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="stat-left">
                                            <div class="stat-chart">
                                                <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                <div id="sparkline_bar"><canvas height="45" width="90" style="display: inline-block; width: 90px; height: 45px; vertical-align: top;"></canvas></div>
                                            </div>
                                            <div class="stat-number">
                                                <div class="title">
                                                    <span class="label label-info">Tổng thu nhập</span>
                                                </div>
                                                <div class="number">
                                                    {{number_format($total_profit)}}đ
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="stat-right">
                                            <div class="stat-chart">
                                                <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                <div id="sparkline_bar2"><canvas height="45" width="90" style="display: inline-block; width: 90px; height: 45px; vertical-align: top;"></canvas></div>
                                            </div>
                                            <div class="stat-number">
                                                <div class="title">
                                                    <span class="label label-danger">Thu nhập tháng này</span>
                                                </div>
                                                <div class="number">
                                                    {{number_format($current_profit)}}đ
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PORTLET -->
                    </div>
                </div>

            </div>
            <!-- END PROFILE CONTENT -->
        </div>
    </div>
@stop
@section('page_level_styles')
    <link href="/assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
@stop
