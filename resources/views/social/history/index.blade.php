@extends('site.layouts.default')
@section('title')
    Lịch sử chia sẻ
    @stop
@section('page_title')
    Lịch sử chia sẻ
    @stop
@section('content')
    <form action="" method="get" class="form-horizontal" role="form">
        <div class="col-xs-12">
        <div class="form-group no-gutter">
            <div class="col-xs-2">
                <div class="control-label">Xem theo tháng:</div>
            </div>
            <div class="col-xs-2">
                <select class="form-control" name="month">
                    @for($i=1;$i<13;$i++)
                        <option value="{{$i}}" {{($i==$month)?"selected":""}}>{{$i}}</option>
                    @endfor
                </select>
            </div>
            <div class="col-xs-2">
                <select class="form-control" name="year">
                    @for($i=2015;$i<2019;$i++)
                        <option value="{{$i}}" {{($i==$year)?"selected":""}}>{{$i}}</option>
                    @endfor
                </select>
            </div>
            <div class="col-xs-2">
                <button type="submit" class="btn btn-danger">Xem</button>
            </div>
        </div>
        </div>
    </form>
    @if(count($logs))
        <table class="table table-condensed table-hover">
            <thead>
            <tr>
                <th>Ngày</th>
                <th>Số lượt chia sẻ</th>
                <th>Hợp đồng</th>
                <th>Thu nhập</th>
            </tr>
            </thead>
            <tbody>
            @foreach($logs as $log)
                <tr>
                    <td> {{$log->date}}</td>
                    <td> {{$log->count}}</td>
                    <td> {{$log->contract}}</td>
                    <td> <span class="label label-info">{{number_format($log->contract*8333)}}đ</span></td>
                </tr>
            @endforeach
            <tfoot>
            <tr>
                <td colspan="3" class="text-right">Thu nhập tháng ({{$month}}): </td>
                <td class="font-red"><span class="label label-danger">{{number_format($profit)}}đ</span></td>
            </tr>
            </tfoot>
            </tbody>
        </table>
    @endif
@stop