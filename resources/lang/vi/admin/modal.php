<?php

return [
    'create' => 'Tạo',
    'edit' => 'Sửa',
    'reset' => 'Khôi phục',
    'cancel' => 'Hủy',
    'general' => 'Chung',
    'title' => 'Tiêu đề',
    'new' => 'Mới',
    'delete' => 'Xóa',
    'items' => 'Items',
    'delete_message' => 'Bạn muốn xóa?',
    'delete' => 'Xóa',
    'view' => 'Xem',

];