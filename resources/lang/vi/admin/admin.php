<?php

return [
	'settings' => 'Cài đặt',
	'homepage' => 'Trang chủ',
	'home' => 'Trang chủ',
	'welcome' => 'Welcome',
    'action' => 'Thao tác',
    'back' => 'Trở về',
    'created_at' => 'Tạo lúc',
    'language' => 'Ngôn ngữ',
    'yes'       => 'Có',
    'no'        =>  'Không',
    'view_detail' => 'Xem thêm',
    'news_categories' => 'Danh mục tin tức',
    'news_items' => 'Tin tức',
    'photo_albums' => 'Album hình ảnh',
    'photo_items' => 'Hình ảnh',
    'video_albums' => 'Video albums',
    'video_items' => 'Video',
    'users' => 'Người dùng',
	];