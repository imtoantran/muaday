<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key' => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model' => 'App\User',
        'key' => '',
        'secret' => '',
    ],
    'facebook' => [
        'client_id' => '897956023595118',
        'client_secret' => '8de624c1061ce3045638a54058553854',
        'redirect' => 'http://muaday.com/social/facebook',
    ],
    'google' => [
        'client_id' => '220113778888-9ihqeajftuuirhmajd7nnjdp6h28mf6q.apps.googleusercontent.com',
        'client_secret' => 'QKJfvMdP3QBWdTtAM83Hb3Lc',
        'redirect' => 'http://gynos8h.vn/auth/social',
    ],
];
