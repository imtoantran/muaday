<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class ArticleCategoriesNav extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view("widgets.article_categories_nav");
    }
}