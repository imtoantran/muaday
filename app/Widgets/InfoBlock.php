<?php

namespace App\Widgets;

use App\Social;
use Arrilot\Widgets\AbstractWidget;

class InfoBlock extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $members = \DB::table('socials')->count();
        $products = \DB::table('products')->count();
        return view("widgets.info_block",compact("members","products"));
    }
}