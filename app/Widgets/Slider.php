<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Product;
class Slider extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    protected $class;
    public function run()
    {
        //
//        $this->cssClassForWrapper = ;
    	$slider = Product::all();
        return view("widgets.slider",compact("slider"));
    }
}