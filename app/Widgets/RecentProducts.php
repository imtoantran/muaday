<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Product;

class RecentProducts extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
    	$products = Product::take(2)->orderBy('created_at','asc')->get();
        return view("widgets.recent_products",compact("products"));
    }
}