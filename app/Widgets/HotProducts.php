<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Product;
class HotProducts extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
    	$products= Product::where("featured",1)->take(2)->get();
        return view("widgets.hot_products",compact("products"));
    }
}