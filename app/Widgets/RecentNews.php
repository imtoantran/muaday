<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Article;
// use Illuminate\Support\Str;
class RecentNews extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
    	$articles = Article::take(3)->orderBy('updated_at','asc')->get();
        return view("widgets.recent_news",compact("articles"));
    }
}