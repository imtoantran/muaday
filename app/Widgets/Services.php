<?php

namespace App\Widgets;

use App\Service;
use Arrilot\Widgets\AbstractWidget;

class Services extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $services = Service::orderBy('order','asc')->get();
        $column = ceil(12/count($services));
        return view("widgets.services",compact('services','column'));
    }
}