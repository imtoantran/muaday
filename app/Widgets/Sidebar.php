<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class Sidebar extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $sidebars = \App\Sidebar::where('active',1)->orderBy("order","asc")->get();
        return view("widgets.sidebar",compact("sidebars"));
    }
}