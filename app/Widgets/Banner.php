<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class Banner extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
    	$banners = \App\Banner::orderBy('order','asc')->get();
        return view("widgets.banner",compact("banners"));
    }
}