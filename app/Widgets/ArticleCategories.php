<?php

namespace App\Widgets;

use App\ArticleCategory;
use Arrilot\Widgets\AbstractWidget;

class ArticleCategories extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $categories = ArticleCategory::select(['id','slug','title'])->where('slug','not like','gioi-thieu')->get();
        return view("widgets.article_categories",compact('categories'));
    }
}