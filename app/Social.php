<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model {

	//
    public function contract()
    {
        return $this->hasMany("App\Contract","social_id","social_id");
    }
}
