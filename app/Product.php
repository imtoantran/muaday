<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends \Eloquent {
    protected $casts = ["image"=>"array"];

    public function setImage($value)
    {
        if($value==null)  $this->attributes['image'] = [];
    }
	//
	public function link()
	{
		return "/san-pham/$this->slug.html";
	}

}
