<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Zizaco\Entrust\HasRole;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword,EntrustUserTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * Save roles inputted from multiselect
	 * @param $inputRoles
	 */

	/**
	 * Get user by username
	 * @param $username
	 * @return mixed
	 */
	public function getUserByUsername( $username )
	{
		return $this->where('username', '=', $username)->first();
	}


	/**
	 * Find the user and check whether they are confirmed
	 *
	 * @param array $identity an array with identities to check (eg. ['username' => 'test'])
	 * @return boolean
	 */
	public function isConfirmed($identity) {
		$user = Confide::getUserByEmailOrUsername($identity);
		return ($user && $user->confirmed);
	}

	/**
	 * Get the date the user was created.
	 *
	 * @return string
	 */
	public function joined()
	{
		return String::date(Carbon::createFromFormat('Y-n-j G:i:s', $this->created_at));
	}

	/**
	 * Save roles inputted from multiselect
	 * @param $inputRoles
	 */
	public function saveRoles($inputRoles)
	{
		if(! empty($inputRoles)) {
			$this->roles()->sync($inputRoles);
		} else {
			$this->roles()->detach();
		}
	}

	/**
	 * Returns user's current role ids only.
	 * @return array|bool
	 */
	public function currentRoleIds()
	{
		$roles = $this->roles;
		$roleIds = false;
		if( !empty( $roles ) ) {
			$roleIds = array();
			foreach( $roles as $role )
			{
				$roleIds[] = $role->id;
			}
		}
		return $roleIds;
	}

	/**
	 * Redirect after auth.
	 * If ifValid is set to true it will redirect a logged in user.
	 * @param $redirect
	 * @param bool $ifValid
	 * @return mixed
	 */
	public static function checkAuthAndRedirect($redirect, $ifValid=false)
	{
		// Get the user information
		$user = Auth::user();
		$redirectTo = false;

		if(empty($user->id) && ! $ifValid) // Not logged in redirect, set session.
		{
			Session::put('loginRedirect', $redirect);
			$redirectTo = Redirect::to('user/login')
				->with( 'notice', Lang::get('user/user.login_first') );
		}
		elseif(!empty($user->id) && $ifValid) // Valid user, we want to redirect.
		{
			$redirectTo = Redirect::to($redirect);
		}

		return array($user, $redirectTo);
	}

	public function currentUser()
	{
		return Confide::user();
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}
}
