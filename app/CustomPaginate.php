<?php namespace App;
use App\Illuminate\Pagination;
class CustomPaginate extends Illuminate\Pagination\Presenter {

    public function getActivePageWrapper($text)
    {
        return '<a class="page_current" href="">'.$text.'</a>';
    }

    public function getDisabledTextWrapper($text)
    {
        return '<a href="" class="unavailable">'.$text.'</a>';
    }

    public function getPageLinkWrapper($url, $page, $rel = null)
    {
        return '<a href="'.$url.'">'.$page.'</a>';
    }

}