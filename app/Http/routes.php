<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('user', 'App\User');
Route::model('comment', 'Comment');
Route::model('post', 'Post');
Route::model('role', 'Role');
/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('comment', '[0-9]+');
Route::pattern('post', '[0-9]+');
Route::pattern('user', '[0-9]+');
Route::pattern('role', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');

// Route::get('', 'WelcomeController@index');
Route::get('gioi-thieu-{slug}.html', 'NewsController@view');
Route::get('{slug}.html', 'PagesController@view');
Route::get('thu-vien-anh', 'PagesController@view');
Route::get('admin',function(){return redirect("auth/login");});
Route::get('dang-nhap',"SocialController@login");
Route::get('dang-xuat',"SocialController@logout");
Route::get('', 'HomeController@index');

Route::get('auth/social', 'Auth\AuthController@social');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	'share'=>'ShareController',
]);

Route::controller('test','TestController');

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin'], function() {
	Route::pattern('id', '[0-9]+');
	Route::pattern('id2', '[0-9]+');

    # Admin services
    Route::controller('services', 'ServicesController');
	# Admin Dashboard
	Route::get('dashboard', 'DashboardController@index');
    # Banners
    Route::controller('banners', 'BannerController');
    # Sidebar
    Route::controller('sidebars', 'SidebarController');
	# Page
	Route::controller("page","PagesController");
	# Social
	Route::controller('social','SocialController');
	# Contract
	Route::controller('contract','ContractController');
	# Product
	Route::any('products/upload','ProductController@upload');
	Route::controller('products/category','ProductCategoryController');
	Route::controller('products','ProductController');
	# Language
	Route::get('language', 'LanguageController@index');
	Route::get('language/create', 'LanguageController@getCreate');
	Route::post('language/create', 'LanguageController@postCreate');
	Route::get('language/{id}/edit', 'LanguageController@getEdit');
	Route::post('language/{id}/edit', 'LanguageController@postEdit');
	Route::get('language/{id}/delete', 'LanguageController@getDelete');
	Route::post('language/{id}/delete', 'LanguageController@postDelete');
	Route::get('language/data', 'LanguageController@data');
	Route::get('language/reorder', 'LanguageController@getReorder');

	# News category
	Route::get('news/category/data', 'ArticleCategoriesController@data');
	Route::controller('news/category', 'ArticleCategoriesController');
	Route::get('newscategory/create', 'ArticleCategoriesController@getCreate');
	Route::post('newscategory/create', 'ArticleCategoriesController@postCreate');
	Route::get('newscategory/{id}/edit', 'ArticleCategoriesController@getEdit');
	Route::post('newscategory/{id}/edit', 'ArticleCategoriesController@postEdit');
	Route::get('newscategory/{id}/delete', 'ArticleCategoriesController@getDelete');
	Route::post('newscategory/{id}/delete', 'ArticleCategoriesController@postDelete');	

	Route::get('newscategory/reorder', 'ArticleCategoriesController@getReorder');
	Route::controller('news/category','ArticleCategoriesController');

	# News
	Route::get('news', 'ArticlesController@index');
	Route::get('news/create', 'ArticlesController@getCreate');
	Route::post('news/create', 'ArticlesController@postCreate');
	Route::get('news/{id}/edit', 'ArticlesController@getEdit');
	Route::post('news/{id}/edit', 'ArticlesController@postEdit');
	Route::get('news/{id}/delete', 'ArticlesController@getDelete');
	Route::post('news/{id}/delete', 'ArticlesController@postDelete');
	Route::get('news/data', 'ArticlesController@data');
	Route::get('news/reorder', 'ArticlesController@getReorder');
	Route::controller('news','ArticlesController');
	# Photo Album
	Route::get('photo/album', 'PhotoAlbumController@index');
	Route::get('photo/album/create', 'PhotoAlbumController@getCreate');
	Route::post('photo/album/create', 'PhotoAlbumController@postCreate');
	Route::get('photo/album/{id}/edit', 'PhotoAlbumController@getEdit');
	Route::post('photo/album/{id}/edit', 'PhotoAlbumController@postEdit');
	Route::get('photo/album/{id}/delete', 'PhotoAlbumController@getDelete');
	Route::post('photo/album/{id}/delete', 'PhotoAlbumController@postDelete');
	Route::get('photo/album/data', 'PhotoAlbumController@data');
	Route::get('photo/album/reorder', 'PhotoAlbumController@getReorder');
	Route::controller('photo/album', 'PhotoAlbumController');

	# Photo
	// Route::get('photo', 'PhotoController@index');
	Route::get('photo/create', 'PhotoController@getCreate');
	Route::post('photo/create', 'PhotoController@postCreate');
	Route::get('photo/{id}/edit', 'PhotoController@getEdit');
	Route::post('photo/{id}/edit', 'PhotoController@postEdit');
	Route::get('photo/{id}/delete', 'PhotoController@getDelete');
	Route::post('photo/{id}/delete', 'PhotoController@postDelete');
	Route::get('photo/{id}/itemsforalbum', 'PhotoController@itemsForAlbum');
	Route::get('photo/{id}/{id2}/slider', 'PhotoController@getSlider');
	Route::get('photo/{id}/{id2}/albumcover', 'PhotoController@getAlbumCover');
	Route::get('photo/data/{id}', 'PhotoController@data');
	Route::get('photo/reorder', 'PhotoController@getReorder');
	Route::controller('photo', 'PhotoController');

	# Video
	Route::get('videoalbum', 'VideoAlbumController@index');
	Route::get('videoalbum/create', 'VideoAlbumController@getCreate');
	Route::post('videoalbum/create', 'VideoAlbumController@postCreate');
	Route::get('videoalbum/{id}/edit', 'VideoAlbumController@getEdit');
	Route::post('videoalbum/{id}/edit', 'VideoAlbumController@postEdit');
	Route::get('videoalbum/{id}/delete', 'VideoAlbumController@getDelete');
	Route::post('videoalbum/{id}/delete', 'VideoAlbumController@postDelete');
	Route::get('videoalbum/data', 'VideoAlbumController@data');
	Route::get('video/reorder', 'VideoAlbumController@getReorder');

	# Video
	Route::get('video', 'VideoController@index');
	Route::get('video/create', 'VideoController@getCreate');
	Route::post('video/create', 'VideoController@postCreate');
	Route::get('video/{id}/edit', 'VideoController@getEdit');
	Route::post('video/{id}/edit', 'VideoController@postEdit');
	Route::get('video/{id}/delete', 'VideoController@getDelete');
	Route::post('video/{id}/delete', 'VideoController@postDelete');
	Route::get('video/{id}/itemsforalbum', 'VideoController@itemsForAlbum');
	Route::get('video/{id}/{id2}/albumcover', 'VideoController@getAlbumCover');
	Route::get('video/data/{id}', 'VideoController@data');
	Route::get('video/reorder', 'VideoController@getReorder');

	# Users
	Route::get('users/', 'UserController@index');
	Route::get('users/create', 'UserController@getCreate');
	Route::post('users/create', 'UserController@postCreate');
	Route::get('users/edit/{user}', 'UserController@getEdit');
	Route::post('users/edit/{user}', 'UserController@postEdit');
	Route::get('users/{id}/delete', 'UserController@getDelete');
	Route::post('users/{id}/delete', 'UserController@postDelete');
	Route::get('users/data', 'UserController@data');
	Route::controller('users', 'UserController');
    Route::get("settings/password","UserController@getPassword");
    Route::post("settings/password","UserController@postPassword");

});

Route::group(['prefix'=>'san-pham'],function(){
	Route::get('{slug}.html',['as'=>'product','uses'=>'ProductController@view']);
	Route::get('{slug}','ProductController@category');
	Route::get('','ProductController@index');
});
Route::group(['prefix'=>'tin-tuc'],function(){
	Route::get('/{slug}.html','NewsController@view');
	Route::get('/{slug}','NewsController@category');
	Route::controller('','NewsController');
});
Route::group(["prefix"=>"social"],function(){
	Route::controller("/","SocialController");
});
Route::get("services/{slug}.html","ServicesController@show");