<?php namespace App\Http\Middleware;

use Closure;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;

class SocialAuthenticate {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if(!isset($_COOKIE['fbUserID'])){
            return redirect('/');
        }
		return $next($request);
	}

}
