<?php namespace App\Http\Controllers;

use Facebook\FacebookRequest;
use Facebook\FacebookSession;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class ShareController extends Controller
{
    function __construct(){
        if(null == Session::get('facebook')){
            redirect('/auth/social/facebook');
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        return view('share.index');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getHistory(){
        return view('share.history');
    }

    public function postSave(){
        $user = session('facebook');
        $post_id = Request::input('post_id');
        $session = new FacebookSession($user->token);
        $request = new FacebookRequest($session, 'GET', "/$post_id");
        $response = $request->execute();
        $graphObject = $response->getGraphObject();
        return Response::json($graphObject->asArray());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
