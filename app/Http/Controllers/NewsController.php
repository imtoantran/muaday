<?php namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\NewsCategory;
use Illuminate\Http\Request;

class NewsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	public function getIndex(){
		$articles = Article::orderBy("updated_at","desc")->paginate(5);
		return view('site.page.index',compact('articles'));
	}

	public function view($slug)
	{
		// Get all the blog posts
		$page = Article::whereSlug($slug)->first();
		$categories = ArticleCategory::select(['id','slug','title'])->get();
		return view('site.page.view', compact('page','categories'));
	}

    public function category($slug)
    {
        $category = ArticleCategory::whereSlug($slug)->first();
        $title = $category->title;
        $this->slug = $slug;
        $articles = Article::whereHas('category', function($q)
        {
            $q->where('slug', 'like', $this->slug)->where('slug', 'not like', 'gioi-thieu');
        })->paginate(5);
        return view('site.page.index', compact('articles','title'));
    }
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
