<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Social;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller {
	protected $redirectPath = '/admin/dashboard';
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar $registrar
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function social()
	{
		if(Session::has('provider')) {
			$provider = Session::get('provider');
		}
		if($user = Socialite::with($provider)->user()){
			Session::put('social',$user);
			if(!Social::where("social_id",$user->id)->count()){
				$social = new Social();
				$social->first_name = $user->user['first_name'];
				$social->last_name = $user->user['last_name'];
				$social->social_id = $user->id;
				$social->email = $user->email;
				$social->save();
			}
			return redirect('social');
		}
		return redirect('/');
	}

	/**
	 * @param $provider
	 * @return mixed
     */
	public function getSocial($provider){
		Session::put('provider',$provider);
		if($provider=='facebook'){
			return 	Socialite::with($provider)->scopes(['email','user_friends','read_stream','publish_actions'])->redirect();
		}
		return 	Socialite::with($provider)->redirect();
	}
}
