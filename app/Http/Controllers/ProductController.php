<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller {
	function __construct() {
		\Widget::group('sidebar')->position(1)->addWidget("Banner");
		\Widget::group('sidebar')->position(2)->addWidget("RecentProducts");
		\Widget::group('sidebar')->position(3)->addWidget("HotProducts");
		\Widget::group('sidebar')->position(4)->addWidget("Contact");		
	}
	public function view($slug){
		$product = Product::whereSlug($slug)->first();
		\Breadcrumbs::register('product', function($breadcrumbs) {
			$breadcrumbs->push('Home', route('product'));
		});
		return view('site.product.product',compact('product'));
	}

	public function category($slug)
	{
		echo "$slug category";
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::paginate(9);
		return view('site.product.index',compact('products'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
