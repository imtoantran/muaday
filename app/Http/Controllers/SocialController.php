<?php namespace App\Http\Controllers;

use App\Contract;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\ShareLog;
use App\Social;
use Carbon\Carbon;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Laravel\Socialite\Facades\Socialite;


class SocialController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $products = Product::paginate(9);
        return view("social.index",compact('products'));
    }

    public function getDashboard()
    {
        return view('social.dashboard.index');
    }

    public function getProfile()
    {
        $social = \Session::get('social');
        $current = Carbon::now();
        $total_profit = $this->profit($social);
        $current_profit = $this->profit($social,$current->format("m"),$current->format("Y"));
        $total_contract = $this->total_contract($social->id);
        $total_share = $this->total_share($social->id);
        return view('social.profile.index',compact('social','total_profit','current_profit','total_contract','total_share'));
    }

    /**
     * @param Requests\SocialRequest $request
     * @return mixed
     * @throws \Facebook\FacebookSDKException
     */
    public function postShare(Requests\SocialRequest $request)
    {
        if(!isset($_COOKIE['fbUserID'])){
            return Response::json(["success"=>false,"message"=>"Vui lòng đăng nhập."]);
        }
        $fbUserID = $_COOKIE['fbUserID'];
        $fbAccessToken = $_COOKIE['fbAccessToken'];
        $fbUserID = $_COOKIE['fbUserID'];
        $session = new FacebookSession($fbAccessToken);
        if($session){
            try{
                $post = (new FacebookRequest($session,"GET","/$request->post_id"))->execute()->getGraphObject()->asArray();
                if($post['from']->id == $fbUserID&&$post['application']->id==\Config::get("services.facebook.client_id")){
                    //Todo save share history
                    $log = new ShareLog();
                    $log->link = $post['link'];
                    $log->social_link = $post['actions'][0]->link;
                    $log->social_id = $post['from']->id;
                    $log->product_id = $request->product_id;
                    $log->post_id = $request->post_id;
                    if($log->save())
                        return Response::json(["success"=>true,"message"=>"Chia sẻ thành công"]);
                }
            }catch (FacebookRequestException $e){
                return Response::json(["success"=>false,"message"=>"Chia sẻ không thành công, có thể bạn đã đăng nhập bằng tài khoản facebook khác","error"=>$e->getMessage()]);
            }
        }
        return Response::json([$request->post_id]);
    }

    public function getLogout()
    {
        \Session::forget('social');
        \Session::forget('provider');
        return \Response::json(["success"=>true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function login()
    {
        return view("social.layouts.login");
    }

    public function logout()
    {
        \Session::forget('social');
        \Session::forget('provider');
        return redirect("/");
    }

    public function register(Requests\SocialRequest $request)
    {
        $social_id = $request->id;
        if(!Social::where("social_id",$social_id)->count()){
            $social = new Social();
//            $social->first_name = $user->user['first_name'];
//            $social->last_name = $user->user['last_name'];
            $social->social_id = $social_id;
//            $social->email = $user->email;
            $social->save();
        }
    }

    public function getHistory(Requests\SocialRequest $request)
    {
        if(!isset($_COOKIE['fbUserID']))
            return redirect("/");
        $current = Carbon::now();
        $fbUserID = $_COOKIE['fbUserID'];
        $month = $current->format("m");
        $year = $current->format("Y");
        if($request->month) {$month = $request->month;}
        if($request->year) $year = $request->year;
        $logs = ShareLog::select(\DB::raw("date_format(share_logs.created_at,'%d-%m-%Y') as date,count(share_logs.link) as count,(select sum(contracts.contract_no) as contract from contracts where social_id = $fbUserID and share_logs.created_at between contracts.start_date and contracts.end_date) as contract"))
            ->whereRaw(\DB::raw("share_logs.social_id = $fbUserID and month(share_logs.created_at) = $month and year(share_logs.created_at)=$year"))
            ->groupBy(\DB::raw("date_format(share_logs.created_at,'%d-%m-%Y')"))
            ->get();
        $profit = 0;
        foreach($logs as $log) $profit+=$log->contract*8333;
        return view("social.history.index",compact("month","year","logs","profit","current","fbUserID"));
    }
    private function profit($social,$month='',$year=''){
        $daily_salary = 8333;
        $logs = \DB::table(\DB::raw("(SELECT  share_logs.created_at,(select sum(contracts.contract_no) from contracts where contracts.social_id = $social->id and share_logs.created_at  between start_date and end_date ) as daily_salary from share_logs where share_logs.social_id = $social->id group by date(share_logs.created_at)) as a"))
            ->select(\DB::raw("sum(daily_salary)*$daily_salary as profit"));
        if($month!='') $logs=$logs->whereRaw(\DB::raw("month(a.created_at)=$month"));
        if($year!='') $logs=$logs->whereRaw(\DB::raw("year(a.created_at)=$year"));
        $logs = $logs->get();
        return $logs[0]->profit;
    }

    /**
     * @param $id
     */
    private function total_contract($id){
        $current = Carbon::now()->format("Y-m-d h:i:s");
        /** @var TYPE_NAME $social */
        return Contract::whereRaw(\DB::raw("social_id=$id and '$current' between start_date and end_date"))
            ->sum('contract_no');
    }
    private function total_share($id){
        return ShareLog::where("social_id",$id)->count();
    }
    public function getFacebook()
    {
        if($user = Socialite::with('facebook')->user()){
            if(!Social::where("social_id",$user->id)->count()){
                $social = new Social();
                $social->first_name = $user->user['first_name'];
                $social->last_name = $user->user['last_name'];
                $social->name = $user->user['name'];
                $social->social_id = $user->id;
                $social->email = $user->email;
                $social->save();
            }
            return redirect('social');
        }
        return redirect('/');
    }

    /**
     * @param $provider
     * @return mixed
     */
    public function getLogin($provider){
//        $provider='facebook';
        if($provider=='facebook'){
            return 	Socialite::with($provider)->scopes(['email','user_friends','read_stream','publish_actions'])->redirect();
        }
        return 	Socialite::with($provider)->redirect();
    }
}
