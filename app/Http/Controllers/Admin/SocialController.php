<?php namespace App\Http\Controllers\Admin;

use App\Contract;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ShareLog;
use App\Social;
use Carbon\Carbon;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class SocialController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //

    }

    public function getIndex()
    {
        return view('admin.social.index');
    }

    public function getData(Requests\SocialRequest $request)
    {
        $socials = Social::select(['socials.id', \DB::raw("CONCAT(first_name,' ',last_name) as full_name"),'socials.email', 'socials.created_at', 'socials.social_id', \DB::raw("sum(contracts.contract_no) as total_contract")])
            ->leftJoin('contracts','contracts.social_id','=','socials.social_id');
        if($request->name) $socials = $socials->where(\DB::raw("CONCAT(first_name,' ',last_name)"),"like","%$request->name%");
        if($request->email) $socials =  $socials->where("email","like","%$request->email%");
        $socials = $socials->groupBy("socials.id");
        if($request->contracted) {
            switch($request->contracted){
                case "uncontracted":
//                    $socials = $socials->having("total_contract","<",1);
                    break;
                case "contracted":
                    //$socials = $socials->having("total_contract",">",0);
                    break;
                default:
                    break;
            }
        }
        $socials = $socials->orderBy('socials.created_at', 'DESC');
        $datatables = Datatables::of($socials)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/social/edit/\'. $id ) }}}" class="btn red btn-xs" >{{ trans("admin/modal.edit") }}</a> <a href="{{{ URL::to(\'admin/social/history/\'. $id ) }}}" class="btn green btn-xs" >Xem</a>')
            ->remove_column('id','social_id')
            ->edit_column("created_at",function($data){return Carbon::parse($data->created_at)->format('d/m/Y');});       
        return $datatables->make();
    }
    public function postData(Requests\SocialRequest $request)
    {
        $socials = Social::select(['socials.id', \DB::raw("CONCAT(first_name,' ',last_name) as full_name"),'socials.email', 'socials.created_at', 'socials.social_id', \DB::raw("sum(contracts.contract_no) as total_contract")])
            ->leftJoin('contracts','contracts.social_id','=','socials.social_id');
        if($request->name) $socials = $socials->where(\DB::raw("CONCAT(first_name,' ',last_name)"),"like","%$request->name%");
        if($request->email) $socials =  $socials->where("email","like","%$request->email%");
        $socials = $socials->groupBy("socials.id");
        if($request->contracted) {
            switch($request->contracted){
                case "uncontracted":
//                    $socials = $socials->having("total_contract","<",1);
                    break;
                case "contracted":
                    //$socials = $socials->having("total_contract",">",0);
                    break;
                default:
                    break;
            }
        }
        $socials = $socials->orderBy('socials.created_at', 'DESC');
        $datatables = Datatables::of($socials)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/social/edit/\'. $id ) }}}" class="btn red btn-xs" >{{ trans("admin/modal.edit") }}</a> <a href="{{{ URL::to(\'admin/social/history/\'. $id ) }}}" class="btn green btn-xs" >Xem</a>')
            ->remove_column('id','social_id')
            ->edit_column("created_at",function($data){return Carbon::parse($data->created_at)->format('d/m/Y');});       
        return $datatables->make();
    }

    public function getContract()
    {
        $socials = Social::select(['id', 'first_name', 'created_at', 'social_id'])
            ->orderBy('created_at', 'ASC');

        return Datatables::of($socials)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/social/\'  . \'/edit\'. $id ) }}}" class="btn btn-success btn-xs" ><span class="glyphicon glyphicon-pencil"></span>  {{ trans("admin/modal.edit") }}</a>')
            ->remove_column('id')
            ->make();
    }

    public function getEdit($id)
    {
        $social = Social::find($id);
        $contracts = $social->contract()->get();
        return view('admin.social.edit', compact('social','contracts','id'));
    }

    public function getHistory(Requests\SocialRequest $request,$id)
    {
        $current = Carbon::now();
        $social = Social::find($id);
        $month = $current->format("m");
        $year = $current->format("Y");
        if($request->month) {$month = $request->month;}
        if($request->year) $year = $request->year;
        $logs = ShareLog::select(\DB::raw("share_logs.created_at,count(share_logs.link) as count,(select sum(contracts.contract_no) as contract from contracts where social_id = $social->social_id and share_logs.created_at between contracts.start_date and contracts.end_date) as contract"))
            ->whereRaw(\DB::raw("share_logs.social_id = $social->social_id and month(share_logs.created_at) = $month and year(share_logs.created_at)=$year"))
            ->groupBy(\DB::raw("date_format(share_logs.created_at,'%d-%m-%Y')"))
            ->get();
        $profit = 0;
        foreach($logs as $log) $profit+=$log->contract*8333;
        return view("admin.social.history",compact("month","year","logs","profit","current","social","id"));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
