<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Article;
use App\ArticleCategory;
use App\Language;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\Admin\NewsRequest;
use App\Http\Requests\Admin\DeleteRequest;
use App\Http\Requests\Admin\ReorderRequest;
use Illuminate\Support\Facades\Auth;
use Datatables;
use Illuminate\Support\Facades\Redirect;

class ArticlesController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
        return view('admin.news.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $languages = Language::all();
        $language = "";
		$newscategories = ArticleCategory::all();
		$newscategory = "";
        // Show the page
        return view('admin.news.create_edit', compact('languages', 'language','newscategories','newscategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(NewsRequest $request)
    {
        $news = new Article();
        $news -> user_id = Auth::id();
        $news -> title = $request->title;
        $news -> category_id = $request->newscategory_id;
        $news -> introduction = $request->introduction;
        $news -> content = $request->content;
        $news -> source = $request->source;
        $news ->slug = \Str::slug($request->title);


        $picture = "";
        if(Input::hasFile('picture'))
        {
            $file = Input::file('picture');
            $filename = $file->getClientOriginalName();
            $extension = $file -> getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
        }
        $news -> picture = $picture;
        $news -> save();

        if(Input::hasFile('picture'))
        {
            $destinationPath = public_path() . '/img/articles/';
            Input::file('picture')->move($destinationPath, $picture);
        }
        if(isset($news->id)) return Redirect::to(\URL::to("admin/news/$news->id/edit"));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id)
    {
        $news = Article::find($id);
        $languages = Language::all();
        $language = $news->language_id;
		$newscategories = ArticleCategory::all();
		$newscategory = $news->category_id;

        return view('admin.news.create_edit',compact('news','languages','language','newscategories','newscategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postEdit(NewsRequest $request, $id)
    {
        $news = Article::find($id);
        $news -> user_id = Auth::id();
        $news -> title = $request->title;
        $news -> category_id = $request->newscategory_id;
        $news -> introduction = $request->introduction;
        $news -> content = $request->content;
        $news -> source = $request->source;
        $news -> slug = \Str::slug($request->title);

        if(Input::hasFile('picture'))
        {
            $file = Input::file('picture');
            $filename = $file->getClientOriginalName();
            $extension = $file -> getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
            $news -> picture = $picture;
            $destinationPath = public_path() . '/img/articles/';
            Input::file('picture')->move($destinationPath, $picture);
        }
        if($news -> save()) {
            return Redirect::to(\URL::to("admin/news/$news->id/edit"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */

    public function getDelete($id)
    {
        $news = Article::find($id);
        // Show the page
        return view('admin.news.delete', compact('news'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function postDelete(DeleteRequest $request,$id)
    {
        $news = Article::find($id);
        $news->delete();
        return Redirect::to("admin/news");
    }


    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function postData()
    {
        $news = Article::leftJoin('article_categories as ac', 'ac.id', '=', 'articles.category_id')
            ->leftJoin('users as u','u.id','=','articles.user_id')
            ->select(array('articles.id','articles.title','ac.title as category', 'u.name', 'articles.created_at as created_at'))
            ->where('articles.title','like','%'.Input::get('search')['value'].'%')
            ->orWhere('ac.title','like','%'.Input::get('search')['value'].'%')
            ->orWhere('u.name','like','%'.Input::get('search')['value'].'%')
            ->orWhere('articles.created_at','like','%'.Input::get('search')['value'].'%')
            ->orderBy('articles.position', 'ASC');

        return Datatables::of($news)
            ->add_column('actions','<a href="{{{ URL::to(\'admin/news/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>')
            ->make();
    }

    /**
     * Reorder items
     *
     * @param items list
     * @return items from @param
     */
    public function getReorder(ReorderRequest $request) {
        $list = $request->list;
        $items = explode(",", $list);
        $order = 1;
        foreach ($items as $value) {
            if ($value != '') {
                Article::where('id', '=', $value) -> update(array('position' => $order));
                $order++;
            }
        }
        return $list;
    }
}
