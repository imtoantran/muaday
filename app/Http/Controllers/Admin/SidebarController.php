<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Sidebar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SidebarController extends Controller {
    public function getIndex(){

        $banners = Sidebar::orderBy('order','asc')->get();
        return view('admin.sidebar.index',compact("banners"));

    }

    public function getEdit($id = null){
        if($id!=null){
            $banner = Sidebar::find($id);
            if($banner){
                return view('admin.sidebar.edit',compact('banner'));
            }
        }
    }
    public function postEdit($id,Requests\Admin\SidebarRequest $request){
        if(isset($id)){
            $banner = Sidebar::find($id);
            if($banner){
                $banner->content = $request->content;
                $banner->title = $request->title;
                $banner->order = $request->order;
                $banner->link = $request->link;
                $banner->active = $request->active;
                $picture = $banner->image;
                if(\Input::hasFile('picture'))
                {
                    $file = \Input::file('picture');
                    $filename = $file->getClientOriginalName();
                    $extension = $file -> getClientOriginalExtension();
                    $picture = sha1($filename . time()) . '.' . $extension;
                }
                $banner -> image = $picture;
                if(\Input::hasFile('picture'))
                {
                    $destinationPath = public_path() . '/img/sidebars/';
                    \Input::file('picture')->move($destinationPath, $picture);
                }
                if($banner -> save()){
                    return \Redirect::to(\URL::to("admin/sidebars/edit/$banner->id"))->with("success","Cập nhật banner thành công");
                }
            }
        }
    }

    public function getDelete($id = null)
    {
        if($id != null){
            $sidebar = Sidebar::find($id);
            if($sidebar){
                return view('admin.sidebar.delete',compact('sidebar'));
            }
        }
    }

    public function postDelete($id = null)
    {
        if($id!=null){
            $sidebar = Sidebar::find($id);
            $sidebar->delete();
            return \Redirect::to('admin/sidebars')->with("success","Xóa banner thành công");
        }
        return Redirect::to("admin/sidebars")->with("error","Không tồn tại");
    }
    public function getCreate()
    {
        // Show the page
        $post = Sidebar::max("order") + 1;
        return view('admin.sidebar.edit',compact('post'));
    }
    public function postCreate(Requests\Admin\SidebarRequest $request)
    {
        $sidebar = new Sidebar();
        $sidebar -> title = $request->title;
        $sidebar -> content = $request->content;
        $sidebar -> order = $request->order;
        $sidebar -> link = $request->link;
        $sidebar -> active = $request->active;
        $picture = "";
        if(\Input::hasFile('picture'))
        {
            $file = \Input::file('picture');
            $filename = $file->getClientOriginalName();
            $extension = $file -> getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
            $destinationPath = public_path() . '/img/sidebars/';
            \Input::file('picture')->move($destinationPath, $picture);
        }
        $sidebar -> image = $picture;
        if($sidebar -> save())
            return Redirect::to(\URL::to("admin/sidebars/edit/$sidebar->id"))->with("success","Thêm banner thành công");
    }

    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
