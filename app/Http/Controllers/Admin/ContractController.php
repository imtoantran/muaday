<?php namespace App\Http\Controllers\Admin;

use App\Contract;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ContractController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	public function postDelete($id)
	{
		if(Contract::find($id)->delete())
			return Response::json(["success"=>true,"message"=>"Xóa thành công"]);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Requests\Admin\ContractRequest $request
	 * @return Response
	 * @internal param int $id
	 */
	public function postEdit(Requests\Admin\ContractRequest $request)
	{
		if(\Auth::guest()){return Response::json(["needauth"=>true]);}
		if(!$request->id){
			$contract = new Contract();
			$contract->social_id = $request->social_id;
			$contract->provider = "facebook";
			$message = "Thêm hợp đồng thành công";
			$action = "add";
		}else{
			$contract = Contract::find($request->id);
			$message = "Cập nhật hợp đồng thành công";
			$action= "update";
		}
		$contract->start_date = Carbon::parse($request->start_date)->format("Y-m-d");
		$contract->end_date = Carbon::parse($request->end_date)->format("Y-m-d");
		$contract->contract_no = $request->contract_no;
		if($contract->save()) {
			return Response::json(["success" => true, "message" => $message, "contract" => \View::make("partials.admin.contract.item",compact('contract'))->render(), "action" => $action]);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
