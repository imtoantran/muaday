<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\Request;
use App\Permission;
use App\Role;
use App\User;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UserEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Datatables;
use Illuminate\Support\Facades\Auth;


class UserController extends AdminController
{
    /**
     * User Model
     * @var User
     */
    protected $user;
    /**
     * Role Model
     * @var Role
     */
    protected $role;
    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;

    public function __construct(User $user, Role $role, Permission $permission)
    {
        parent::__construct();
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
    }

    public function postPassword(Request $request)
    {
        $user = Auth::user();
        //if ($request->old_password != '') {
        /* check old password */
        //if(bcrypt($request->old_password) == $user->password ){
        /* verify new password */
        $user->email = $request->email;
        if ($request->new_password != '') {
            if($request->new_password == $request->ver_password )
                $user->password = bcrypt($request->new_password);
            else {
                return redirect('admin/settings/password')->with("error", "Mật khẩu mới không trùng khớp");
            }
        }
        if (!$user->save()) {
            return redirect('admin/settings/password')->with("error", "Cập nhật không thành công");
        }
        return redirect('admin/settings/password')->with("success", "Cập nhật thành công");
    }

    public function getPassword()
    {
        $user = \Auth::user();
        return view('admin.users.password', compact('user'));
    }

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
        $users = User::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        // All roles
        $roles = $this->role->all();
        // Get all the available permissions
        $permissions = $this->permission->all();
        // Selected groups
        $selectedRoles = \Input::old('roles', array());
        // Selected permissions
        $selectedPermissions = \Input::old('permissions', array());
        // Title
        $title = \Lang::get('admin/users/title.create_a_new_user');
        // Mode
        $mode = 'create';
        // Show the page
        return view('admin/users/create_edit', compact('roles', 'permissions', 'selectedRoles', 'selectedPermissions', 'title', 'mode'));

        return view('admin.users.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(UserRequest $request)
    {

        $user = new User ();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->confirmation_code = str_random(32);
        $user->confirmed = $request->confirmed;
        $user->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($user)
    {

//        $user = User::find($id);
//        $roles = Role::all();

//        return view('admin.users.create_edit', compact('user','roles'));
        if ($user->id) {
            $roles = $this->role->all();
            $permissions = $this->permission->all();
            // Title
            $title = \Lang::get('admin/users/title.user_update');
            // mode
            $mode = 'edit';
            return view('admin/users/create_edit', compact('user', 'roles', 'permissions', 'title', 'mode'));
        } else {
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(UserEditRequest $request, $user)
    {

        $user->name = $request->name;
        $user->confirmed = $request->confirmed;

        $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;

        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $user->password = bcrypt($password);
            }
        }
        if ($user->save()) {
            return redirect("/admin/users/edit/$user->id")->with("success", "Cập nhật người dùng thành công");
        } else {
            return redirect("/admin/users/edit/$user->id")->with("success", "Cập nhật người dùng không thành công");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */

    public function getDelete($id)
    {
        $user = User::find($id);
        // Show the page
        return view('admin.users.delete', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request, $id)
    {
        $user = User::find($id);
        $user->delete();
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
        $users = User::select(array('users.id', 'users.name', 'users.email', 'users.confirmed', 'users.created_at'));

        return Datatables::of($users)
            ->edit_column('confirmed', '@if ($confirmed=="1") <span class="glyphicon glyphicon-ok"></span> @else <span class=\'glyphicon glyphicon-remove\'></span> @endif')
            ->add_column('actions', '@if ($id!="1")<a href="{{{ URL::to(\'admin/users/edit/\' . $id ) }}}" class="btn btn-success btn-sm
" ><span class="glyphicon glyphicon-pencil"></span>  {{ trans("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'admin/users/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>
                @endif')
            ->remove_column('id')
            ->make();
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function postData()
    {
        $users = User::select(array('users.id', 'users.name', 'users.email', 'users.confirmed', 'users.created_at'));

        return Datatables::of($users)
            ->edit_column('confirmed', '@if ($confirmed=="1") <span class="glyphicon glyphicon-ok"></span> @else <span class=\'glyphicon glyphicon-remove\'></span> @endif')
            ->add_column('actions', '@if ($id!="1")<a href="{{{ URL::to(\'admin/users/edit/\' . $id) }}}" class="btn btn-success btn-sm " ><span class="glyphicon glyphicon-pencil"></span>  {{ trans("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'admin/users/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger "><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>
                @endif')
            ->remove_column('id')
            ->make();
    }
}
