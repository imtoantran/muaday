<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Service;
use Illuminate\Http\Request;

class ServicesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $services = Service::all();
        return view('admin.service.index', compact("services"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        return view('admin.service.edit');
    }

    public function postCreate(Requests\Admin\ServiceRequest $request)
    {
        $service = new Service();
        $service->name = $request->name;
        $service->title = $request->title;
        $service->slug = \Str::slug($request->title);
        $service->icon = $request->icon;
        $service->order = $request->order;
        $service->description = $request->description;
        $service->content = $request->content;
        if($service->save()){
            return redirect("/admin/services/edit/$service->id")->with("success","Thêm dịch vụ thành công");
        }
        return redirect("/admin/services/create")->with("error","Thêm dịch vụ không thành công");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function getEdit($id)
    {
        $service = Service::find($id);
        return view('admin.service.edit', compact('service'));
    }

    public function postEdit($id,Requests\Admin\ServiceRequest $request)
    {
        $service = Service::find($id);
        $service->name = $request->name;
        $service->title = $request->title;
        $service->slug = \Str::slug($request->title);
        $service->icon = $request->icon;
        $service->order = $request->order;
        $service->deactive = $request->deactive;
        $service->description = $request->description;
        $service->content = $request->content;
        if($service->save()){
            return redirect("/admin/services/edit/$service->id")->with("success","Cập nhật dịch vụ thành công");
        }
        return redirect("/admin/services/edit/$service->id")->with("error","Cập nhật dịch vụ không thành công");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function getDelete($id)
    {
        //
    }

    public function postDelete($id)
    {

    }

}
