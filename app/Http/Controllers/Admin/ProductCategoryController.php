<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ProductCategory;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class ProductCategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        return view('admin.productcategory.index');
    }

    public function getEdit($id){
        $category = ProductCategory::find($id);
        return view('admin.productcategory.create_edit',compact("category"));
    }
    public function postEdit(Requests\Admin\ProductCategoryRequest $request, $id)
    {
        $category = ProductCategory::find($id);
        $category -> user_id = \Auth::id();
        $category -> name = $request->name;
        $category -> description = $request->description;
        if($category -> save()){
            return \Redirect::to("admin/products/category/edit/$category->id");
        };

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        return view('admin.productcategory.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\Admin\ProductCategoryRequest $request
     * @return Response
     */
    public function postCreate(Requests\Admin\ProductCategoryRequest $request)
    {
        $productcategory = new ProductCategory();
        $productcategory->user_id = \Auth::id();
        $productcategory->name = $request->name;
        $productcategory->description = $request->description;
        if ($productcategory->save()) {
            return \URL::to("admin/product/category/edit/$productcategory->id");
        }
    }

    public function postData(Requests\Admin\ProductCategoryRequest $request)
    {
        $products = ProductCategory::select(array('id', 'name', 'description', 'created_at'))
        ->where('name','like',"%".$request->search['value']."%")
        ->orWhere('description','like',"%".$request->search['value']."%")
        ->orWhere('created_at','like',"%".$request->search['value']."%")
            ->orderBy('created_at', 'ASC');

        return Datatables::of($products)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/products/category/edit/\'.$id ) }}}" class="btn btn-success btn-xs iframe" ><span class="glyphicon glyphicon-pencil"></span>  Sửa</a>
                    <a href="{{{ URL::to(\'admin/products/category/delete/\' . $id) }}}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
                    <input type="hidden" name="row" value="{{$id}}" id="row">')
            ->remove_column('id')
            ->editColumn("created_at", function ($data) {
                return $data->created_at->toDateTimeString();
            })
            ->editColumn("description", function ($data) {
                return \Str::words($data->description, 20);
            })
            ->make();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */

    public function getDelete($id)
    {
        $news = ProductCategory::find($id);
        // Show the page
        return view('admin.productcategory.delete', compact('news'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function postDelete(Requests\Admin\DeleteRequest $request,$id)
    {
        $news = ProductCategory::find($id);
        $news->delete();
        return \Redirect::to("admin/products/category");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
