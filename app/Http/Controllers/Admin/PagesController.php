<?php namespace App\Http\Controllers\Admin;

use App\Events\MenuUpdated;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Page;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class PagesController extends Controller {
	public function getIndex()
	{
		return view('admin.page.index');
	}

	public function postData()
	{
		Carbon::setLocale('vi');
		$pages = Page::select(["pages.id","pages.name as title","excerpt","users.name","pages.created_at"])
		->join('users','pages.user_id','=','users.id');
		return Datatables::of($pages)
			-> add_column('actions', '<a href="{{{ URL::to(\'admin/page/edit/\' . $id) }}}" class="btn btn-success btn-sm " ><span class="glyphicon glyphicon-pencil"></span>  {{ trans("admin/modal.edit") }}</a>')
			// -> remove_column('id')
			->editColumn('created_at',function($data){return $data->created_at->diffForHumans();})
			->make();

	}

	public function getEdit($id)
	{
		$page = Page::find($id);
		return view('admin.page.create_edit',compact('page'));
	}

	public function postEdit(Requests\Admin\PageRequest $request,$id)
	{
		$page = Page::find($id);
		$page->title = $request->title;
		$page->excerpt = $request->excerpt;
		$page->content = $request->content;
		// $page->slug = \Str::slug($request->title);
		$picture = "";
		if(\Input::hasFile('picture'))
		{
			$file = \Input::file('picture');
			$filename = $file->getClientOriginalName();
			$extension = $file -> getClientOriginalExtension();
			$picture = sha1($filename . time()) . '.' . $extension;
		}
		$page -> picture = $picture;
		$page -> save();
        event( new MenuUpdated($page));
		if(\Input::hasFile('picture'))
		{
			$destinationPath = public_path() . '/img/page/';
			\Input::file('picture')->move($destinationPath, $picture);
		}
		return redirect(\URL::to("admin/page/edit/$page->id"));
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
