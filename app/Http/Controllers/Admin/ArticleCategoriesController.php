<?php namespace App\Http\Controllers\Admin;

use App\ArticleCategory;
use App\Language;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\NewsCategoryRequest;
use App\Http\Requests\Admin\DeleteRequest;
use App\Http\Requests\Admin\ReorderRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Datatables;

class ArticleCategoriesController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        // Show the page
        return view('admin.newscategory.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $languages = Language::all();
        $language = "";
        // Show the page
        return view('admin.newscategory.create_edit', compact('languages','language'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(NewsCategoryRequest $request)
    {
        $newscategory = new ArticleCategory();
        $newscategory -> user_id = Auth::id();
        $newscategory -> language_id = 1;
        $newscategory -> title = $request->title;
        $newscategory -> slug = \Str::slug($request->title);
        if($newscategory -> save()) {
            $this->__updateMenuCache();
            return \Redirect::to(url("admin/news/category/edit/$newscategory->id"))->with("success", "Thêm thành công");
        }
        else
            return \Redirect::to(url("admin/news/category/edit/$newscategory->id"))->with("error","Cập nhật thất bại");
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id)
    {
        $newscategory = ArticleCategory::find($id);
        return view('admin.newscategory.create_edit',compact('newscategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postEdit(NewsCategoryRequest $request, $id)
    {
        $newscategory = ArticleCategory::find($id);
        $newscategory -> user_id_edited = Auth::id();
        $newscategory -> title = $request->title;
        $newscategory -> slug = \Str::slug($request->title);
        if($newscategory -> save()) {
            $this->__updateMenuCache();
            return \Redirect::to(url("admin/news/category/edit/$newscategory->id"))->with("success", "Cập nhật thành công");
        }
        else
            return \Redirect::to(url("admin/news/category/edit/$newscategory->id"))->with("error","Cập nhật thất bại");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */

    public function getDelete($id)
    {
        $newscategory = ArticleCategory::find($id);
        // Show the page
        return view('admin.newscategory.delete', compact('newscategory'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function postDelete(DeleteRequest $request,$id)
    {
        $newscategory = ArticleCategory::find($id);
        if($newscategory->delete()) {
            $this->__updateMenuCache();
            return \Redirect::to(url("admin/news/category"))->with("success", "Xóa danh mục thành công");
        }
        else
            return \Redirect::to(url("admin/newscategory/$newscategory->id/delete"))->with("error","Xóa danh mục không thành công");
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function postData()
    {
        $article_categories = ArticleCategory::select(['article_categories.id','article_categories.title','users.name',  'article_categories.created_at'])
        ->leftJoin('users','users.id','=','article_categories.user_id')
        ->orderBy('article_categories.created_at', 'ASC');

        return Datatables::of($article_categories)
           ->add_column('actions', '<a href="{{{ URL::to(\'admin/newscategory/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>')
            // ->remove_column('id')
            ->editColumn('created_at',function($data){return $data->created_at->format("d/m/Y");})
            ->make();
    }

    /**
     * Reorder items
     *
     * @param items list
     * @return items from @param
     */
    public function getReorder(ReorderRequest $request) {
        $list = $request->list;
        $items = explode(",", $list);
        $order = 1;
        foreach ($items as $value) {
            if ($value != '') {
                ArticleCategory::where('id', '=', $value) -> update(array('position' => $order));
                $order++;
            }
        }
        return $list;
    }

    private function __updateMenuCache()
    {
        $categories = ArticleCategory::where('slug','like','%gioi-thieu%')->get();
        \Cache::forget('newscategories');
        \Cache::forever('newscategories',$categories);
    }
}
