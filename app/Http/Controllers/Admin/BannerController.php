<?php namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Sidebar;
use Illuminate\Http\Request;

class BannerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        //
        return view('admin.banner.index');
    }

    public function getHome($id = null)
    {
        if($id!=null){
            $banner = Banner::find($id);
            if($banner){
                return view('admin.banner.edit',compact('banner'));
            }
        }
        $banners = Banner::orderBy('order','asc')->get();
        return view('admin.banner.home',compact("banners"));
    }

    public function postHome($id,Requests\Admin\BannerRequest $request){
        if(isset($id)){
            $banner = Banner::find($id);
            if($banner){
                $banner->icon = $request->icon;
                $banner->title = $request->title;
                $banner->order = $request->order;
                $banner->link = $request->link;
                $picture = $banner->image;
                if(\Input::hasFile('picture'))
                {
                    $file = \Input::file('picture');
                    $filename = $file->getClientOriginalName();
                    $extension = $file -> getClientOriginalExtension();
                    $picture = sha1($filename . time()) . '.' . $extension;
                }
                $banner -> image = $picture;
                if(\Input::hasFile('picture'))
                {
                    $destinationPath = public_path() . '/img/banners/';
                    \Input::file('picture')->move($destinationPath, $picture);
                }
                if($banner -> save()){
                    return \Redirect::to(\URL::to("admin/banners/home/$banner->id"))->with("success","Cập nhật banner thành công");
                }
            }
        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
