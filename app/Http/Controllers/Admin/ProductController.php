<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\ProductCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use yajra\Datatables\Datatables;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    public function getIndex()
    {
        return view('admin.products.index');
    }

    public function getCategory()
    {
        return view('admin.productcategory.index');
    }

    public function postData(Requests\Admin\ProductRequest $request)
    {
        $products = Product::leftJoin('product_categories', 'product_categories.id', '=', 'products.product_category_id')
            ->select(['products.sku','products.id', 'products.name', 'product_categories.name as category',\DB::raw('format(products.price,0) as price'), 'products.created_at','image'])
            ->where('products.sku','like',"%".$request->search['value']."%")
            ->orWhere('products.name','like',"%".$request->search['value']."%")
            ->orWhere('product_categories.name','like',"%".$request->search['value']."%")
            ->orWhere('price','like',"%".$request->search['value']."%")
            ->orWhere('products.created_at','like',"%".$request->search['value']."%")
            ->orderBy('products.created_at', 'ASC');

        return Datatables::of($products)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/products/edit/\' . $id) }}}" class="btn btn-success btn-xs" ><span class="glyphicon glyphicon-pencil"></span>  {{ trans("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'admin/products/delete/\' . $id ) }}}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>
                    <input type="hidden" name="row" value="{{$id}}" id="row">')
            ->remove_column('id')
            ->editColumn('created_at',function($data){return Carbon::parse($data->created_at)->format("d-m-Y");})
            ->editColumn('image',function($data){return "<img src='/img/products/thumbnail/".$data->image[0]."'/>";})
            ->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function postCreate(Requests\Admin\ProductRequest $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->product_category_id = $request->category;
        $product->price = $request->price;
        $product->excerpt = $request->excerpt;
        $product->description = $request->description;
        $product->image = $request->image;
        $product->slug = \Str::slug($request->name);
        $product->sale_price = $request->sale_price;
        if($product->save()){
            return Redirect::to(\URL::to("admin/products/edit/$product->id"));
        }
    }
    public function getCreate()
    {
        $categories  = ProductCategory::all();
        return view('admin.products.create_edit',compact('categories'));
    }

    public function getEdit($id)
    {
        $product = Product::find($id);
        $categories  = ProductCategory::all();
        return view('admin.products.create_edit',compact('product','categories'));
    }

    public function upload()
    {
        $options =[
            'script_url' => \URL::to('admin/products/upload'),
            "upload_url"=>\URL::to("img/products")."/",
            "upload_dir"=>public_path()."/img/products/",
            "width"=>800,
            "height"=>600
        ];
        //return public_path();
        $upload = new \App\Libraries\UploadHandler($options,true,null);
    }
    public function postEdit(Requests\Admin\ProductRequest $request,$id)
    {
        $product = Product::find($id);
        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->product_category_id = $request->category;
        $product->price = $request->price;
        $product->excerpt = $request->excerpt;
        $product->description = $request->description;
        $product->image = $request->image;
        $product->sale_price = $request->sale_price;
        $product->image = $request->image;
        if($product->save()){
            return Redirect::to(\URL::to("admin/products/edit/$product->id"));
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
