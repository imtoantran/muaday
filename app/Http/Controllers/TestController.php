<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;

class TestController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		//if(Session::has('provider')) {
			//$provider = Session::get('provider');
		//}
		//$user = Socialite::with($provider)->user();
		//dd(Session::all());
//		$user = App\User::find(1);
//		$user->password = bcrypt("admindhp");
//		$user->save();
//		$admin = App\Role::find(2);
//		$user->attachRole($admin);
		// $img =  \Image::make(public_path('img/travel/01.jpg'));
		// $img->fit(600, 360)->save('test.jpg');
	// add `use Facebook\FacebookJavaScriptLoginHelper;`
	FacebookSession::setDefaultApplication(\Config::get("services.facebook.client_id"),\Config::get("services.facebook.client_secret"));
	$helper = new FacebookJavaScriptLoginHelper();
	try {
	    $session = $helper->getSession();
	    // dd($session);
	} catch(FacebookRequestException $ex) {
	    // When Facebook returns an error
	     dd($ex);
	} catch(\Exception $ex) {
	    // When validation fails or other local issues
	    dd($ex);
	}
		dd($session);
		dd(Session::all());
	}


	public function getCleansession()
	{
		Session::flush();
	}
	public function getRole(){
		$owner = new App\Role();
		$owner->name         = 'owner';
		$owner->display_name = 'Project Owner'; // optional
		$owner->description  = 'User is the owner of a given project'; // optional
		$owner->save();

		$admin = new App\Role();
		$admin->name         = 'admin';
		$admin->display_name = 'Administrator'; // optional
		$admin->description  = 'User is allowed to manage and edit other users'; // optional
		$admin->save();
	}

	public function getPermission(){
		$createPost = new App\Permission();
		$createPost->name         = 'create-post';
		$createPost->display_name = 'Create Posts'; // optional
// Allow a user to...
		$createPost->description  = 'create new blog posts'; // optional
		$createPost->save();

		$editUser = new App\Permission();
		$editUser->name         = 'edit-user';
		$editUser->display_name = 'Edit Users'; // optional
// Allow a user to...
		$editUser->description  = 'edit existing users'; // optional
		$editUser->save();
//
//		$admin->attachPermission($createPost);
//// equivalent to $admin->perms()->sync(array($createPost->id));
//
//		$owner->attachPermissions(array($createPost, $editUser));
//// equivalent to $owner->perms()->sync(array($createPost->id, $editUser->id));
	}

	public function getLogin(){
//		Auth::attempt(['email'=>'imtoantran@gmail.com']);
//		Auth::loginUsingId(1);
		var_dump(Auth::user());
	}

	public function getTest(){
		Carbon::setLocale('vi');
		$dt = Carbon::now();
		//return $dt->diffForHumans();
//		\Auth::attempt(['email'=>'imtoantran@gmail.com']);
		\Auth::basic('imtoantran@gmail.com');
		return dd(\Auth::user());
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
