<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller {
	function __construct() {
		\Widget::group('sidebar')->position(1)->addWidget("Banner");
		// \Widget::group('sidebar')->position(2)->addWidget("RecentNews");
		// \Widget::group('sidebar')->position(3)->addWidget("Contact");
	}

	public function welcome()
	{
		return view('pages.welcome');
	}

	public function about()
	{
		return view('pages.about');
	}

	public function contact()
	{
		return view('pages.contact');
	}

	public function view($slug)
	{
		$page = Page::whereSlug($slug)->first();
		return view('site.page.view',compact('page'));
	}
}
