<?php namespace App\Handlers\Events;

use App\Events\MenuUpdated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class UpdateMenuCache {

    /**
     * Create the event handler.
     *
     */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  MenuUpdated  $event
	 * @return void
	 */
	public function handle(MenuUpdated $event)
	{
		//
	}

}
