$(window).load(function() {
	//alert(screen.height);
//	alert($(window).height());
	if($("#fix-nav-right").length)
	{ 
		if(screen.height > 800) {
			$("#fix-nav-right").css("top", "140px");
			$("#container > #fix-nav-right").css("top", "140px");
			$("#btn-location-close.close-overlay").css("top", "222px");
		}
		else if (screen.height <= 800)
		{
			$("#fix-nav-right").css("top", "120px");
			$("#container > #fix-nav-right").css("top", "120px");

			$("#btn-location-close.close-overlay").css("top", "202px");
		}
		 
	}
});
$(document).ready(function(){

	$('.crollto').click(function(e){
		$('html,body').scrollTo(this.hash, this.hash);
		e.preventDefault();
	});
	
	$("#thumb-button").click( function(event){
		
		event.preventDefault();
		//event.stopPropagation();
		if ($(this).hasClass("isDown") ) {
			$("#thumb-tray").animate({bottom:"27px"}, 200);          
			$(this).removeClass("isDown");
		} else {
			$("#thumb-tray").animate({bottom:"-92px"}, 200);   
			$(this).addClass("isDown");
		}
		return false;
	});
	
	
	$("#right-sidebar, #left-gallery").mouseup(function (e) {
    	var target = $(e.target);
		if(!target.is('#thumb-button') && !target.is('#thumb-tray')) {
		   
			$("#thumb-tray").animate({bottom:"-92px"}, 200);  
			$("#thumb-button").addClass("isDown");
			
		}
    });
	
	
	
	//Re-style select dropdown
	$('select.style-sel').each(function(){
		var title = $(this).attr('title');
		if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
		$(this)
			.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
			.after('<span class="select">' + title + '</span>')
			.change(function(){
				val = $('option:selected',this).text();
				$(this).next().text(val);
				})
	});
	
	
	//$("select.style-sel").chosen().next(".chzn-container").hover(
//		function(){
//			$("select.style-seln").trigger("liszt:open");
//		},
//		function(){
//			$(this).trigger("click");
//		}
//	);
	
    //Hover Icon on Sidebar
	//$('#sidebar').hover(function(){
//		$(this).addClass('is-active');
//        $(this).children('.sub-nav-bar').animate({left: "36px"}, 200)
//    },function(){
//		$(this).removeClass('is-active');
//        $(this).children('.sub-nav-bar').animate({left: "-180px"}, 200)
//    });
	
	
	$("#sidebar").hoverIntent(function () {
		$(this).addClass('is-active');
    	$(this).children('.sub-nav-bar').css("width", "150px");
		//$(this).children('.sub-nav-bar').animate({width: "150px"}, 200)
	}, function() {
		$(this).removeClass('is-active');
        $(this).children('.sub-nav-bar').animate({width: "0px"}, 250)
			
	});
	
	var config = {    
		 sensitivity: 3,    
		 interval: 5000,     
		 timeout: 5000,    
	};
	
	$(".menu-item-parent").hoverIntent(function () {
			$(this).addClass('js-is-active');
			$('.menu-item-parent').stop(true, true);
			$(this).children('.site-navigation-sub-menu').slideDown('fast');
	}, function() {
			$(this).removeClass('js-is-active');
			$(this).children('.site-navigation-sub-menu').slideUp('fast');
			
	}, config);
	
	//Hover Submenu Sidebar
	//$('.menu-item-parent').hover(function(){
//		$(this).addClass('js-is-active');
//        $(this).children('.site-navigation-sub-menu').stop(true,true).slideDown(200, 'easeInOutSine');
//    },function(){
//		$(this).removeClass('js-is-active');
//        $(this).children('.site-navigation-sub-menu').stop(true,true).slideUp(200, 'easeInOutSine');
//    });
	
	 
	
	
	
	
	//Dropdown menu on Footer
	$('.menu-footer > li').hoverIntent(function(){
		//$('select.style-sel').hide();
		$(this).addClass('is-active');
       	$(this).children('.sub-menu').animate({bottom: "0px"}, 300)
		//$(this).children('.sub-menu').show('slide', { direction: 'up' }, 230);
		
    },function(){
		//$('select.style-sel').show();
		$(this).removeClass('is-active');
        $(this).children('.sub-menu').animate({bottom: "-200px"}, 500);
		//$(this).children('.sub-menu').hide('slide', { direction: 'up' }, 230);
    }, config);
	
	//Active state Calendar
	$(".fc-calendar .fc-row > div.has-offer").click(function() {
		$('.fc-calendar .fc-row > div.has-offer').removeClass('is-offer-active');
		$(this).addClass('is-offer-active');
	});
	
	//$('.scroll-pane').jScrollPane();
	
	$('body').ready(function(){
		$('input:text').each( function () {
			$(this).val($(this).attr('defaultVal')); 
		});
		$('input:text').focus(function(){
			if ( $(this).val() == $(this).attr('defaultVal') ){
			  $(this).val('');
			}
		});
		$('input:text').blur(function(){
			if ( $(this).val() == '' ){
			  $(this).val($(this).attr('defaultVal'));
			}
		});
	});
	
	$('textarea').each(function() {
		$(this).data('default', this.value);
	}).focusin(function() {
		if ( this.value == $(this).data('default') ) {
			this.value = '';    
		}
	}).focusout(function() {
		if ( this.value == '' ) {
			this.value = $(this).data('default');    
		}
	});
	
	//Active state Calendar
	$("#info-nav").click(function() {
		//$(this).parent().addClass('is-active');
		//$("#wrap-pop").animate({right:"0px"}, 200); 
		$("#wrap-pop").show('slide', { direction: 'right' }, 230);
		//$("#wrap-pop .pop-body").show();
		//$('.flyout').hide('slide', { direction: 'right' }, 200);
		//$(".toggle-flyout").addClass("isDown");
	});
	
	
	
	$("#close-pop").click(function() {
		//$("#wrap-pop").animate({right:"-590px"}, 200); 
		$("#wrap-pop").hide('slide', { direction: 'right' }, 200);
		//$("#wrap-pop .pop-body").hide();
		//$("#fix-nav-right").removeClass('is-active');
		
	});
	
	 
	$(".nav-meeting #info-nav").click(function() {
		//$(this).parent().addClass('is-active');
		//$("#over-hall").show('slide', { direction: 'right' }, 300);
		$("#wrap-pop").show('slide', { direction: 'right' }, 300);
		//$("#wrap-pop").css("width", "650px");
		//$(".nav-halls #wrap-pop .pop-body .border").show();
		//$("#wrap-pop").css("right", "0px");
		
		
	});
	
	$(".nav-halls #info-nav").click(function() {
		//$(this).parent().addClass('is-active');
		$("#over-hall").show('slide', { direction: 'right' }, 300);
		$("#wrap-pop").show('slide', { direction: 'right' }, 100);
		//$("#wrap-pop").css("width", "650px");
		//$(".nav-halls #wrap-pop .pop-body .border").show();
		//$("#wrap-pop").css("right", "0px");
		$("#hide-btn").addClass("isShow");
		
	});
	$(".nav-halls #close-pop").click(function() {
		$("#over-hall").hide('slide', { direction: 'right' }, 200);
		//$(".nav-halls #wrap-pop .pop-body .border").hide();
		//$("#wrap-pop").css("width", "0px"); 
		$("#hide-btn").removeClass("isShow");
	});
	
	$("#btn-location").click(function() {
		$(".meeting-overlay").addClass("isShow");
		$(".meeting-overlay").show('slide', { direction: 'right' }, 300);
		//$("#wrap-pop").css("width", "0px");
		$("#wrap-pop").hide('slide', { direction: 'right' }, 300);
		//$("#wrap-pop .pop-body").hide();	
	});
	$("#btn-location-close").click(function() {
		$(".meeting-overlay").removeClass("isShow");
		$(".meeting-overlay").hide('slide', { direction: 'right' }, 200);
	});
	$("#info-page").click(function() {
		$("#over-info").addClass("isShow");
		$("#over-info").show('slide', { direction: 'right' }, 300);
		 	
	});
	$("#i-close").click(function() {
		$("#over-info").removeClass("isShow");
		$("#over-info").hide('slide', { direction: 'right' }, 200);
	});
	
	
	$(".local-numb").hover(function(){
		$(".local-numb").removeClass("active");
		
	});
	
	//Location blokc1
	$('.fl01').hover(function(){
		$(".fl-01").html($(this).attr('title'));
		
    },function(){
		$(".fl-01").html("");
    });
	//Location blokc2
	$('.fl02').hover(function(){
		$(".fl-02").html($(this).attr('title'));
		
    },function(){
		$(".fl-02").html("");
    });
	//Location blokc3
	$('.fl03').hover(function(){
		$(".fl-03").html($(this).attr('title'));
		
    },function(){
		$(".fl-03").html("");
    });
	//Location blokc4
	$('.fl04').hover(function(){
		$(".fl-04").html($(this).attr('title'));
		
    },function(){
		$(".fl-04").html("");
    });
	
	//Location blokc5
	$('.fl05').hover(function(){
		$(".fl-05").html($(this).attr('title'));
		
    },function(){
		$(".fl-05").html("");
    });
	//Location blokc6
	$('.fl06').hover(function(){
		$(".fl-06").html($(this).attr('title'));
		
    },function(){
		$(".fl-06").html("");
    });
	
	//block7
	//Location blokc6
	$('.fl07').hover(function(){
		$(".fl-07").html($(this).attr('title'));
		
    },function(){
		$(".fl-07").html("");
    });
	$('.fl08').hover(function(){
		$(".fl-08").html($(this).attr('title'));
		
    },function(){
		$(".fl-08").html("");
    });
	
	
	$(".toggle-flyout").click( function(event){
		
		event.preventDefault();
		
		if ($(this).hasClass("isDown") ) {
			 $(this).siblings('.flyout').show('slide', { direction: 'right' }, 300);        
			$(this).removeClass("isDown");
		} else {
			 $(this).siblings('.flyout').hide('slide', { direction: 'right' }, 200); 
			$(this).addClass("isDown");
		}
		return false;
		
		 
	});
	$("#btn-small-thumb").click( function(event){
		
		event.preventDefault();
		
		if ($(this).hasClass("isDown") ) {
			$(this).removeClass("isDown");
			$(".meeting-overlay").addClass("isShow");
			$(".meeting-overlay").show('slide', { direction: 'right' }, 300);
			$("#nav-slide").hide();
			//$("#nav-bxslider").show();
			$("#fix-nav-right").css("top", "202px");
			 
		} else {
			$(this).addClass("isDown");
			$(".meeting-overlay").hide('slide', { direction: 'right' }, 300);
			$(".meeting-overlay").removeClass("isShow");
			$("#nav-slide").show();
			//$("#nav-bxslider").hide(); 
			$("#fix-nav-right").css("top", "100px");
		}
		return false; 
	});
	 
	
	
    //$('#toolbox .close').click(function () {
//        $(this).parents('.flyout').hide('slide', { direction: 'right' }, 300);
//        $(this).parents('.active').removeClass('active');
//        return false;
//    });
	
});

    
//$(document).mouseup(function (e)
//{
//	
//
//    var target = $(e.target);
//	if(!target.is('#thumb-tray') && !target.is('#thumb-button')) {
//		container.animate({bottom:"-92px"}, 200);  
//		$("#thumb-button").addClass("isDown");
//		$("#thumb-button").addClass("isclick");
//	}	 
//});
 
 