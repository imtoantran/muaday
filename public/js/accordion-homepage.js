$(function() {

	var $master = $('#landing-sba'),
		$boxs = $master.find('.box-acc');

	if (Modernizr.touch === true) {
		return;
	}

	$boxs.on('mouseenter', function(e) {

		var $box = $(e.currentTarget),
			$focus = $box.find('.focus'),
			$text = $box.find('.text'),
			$buttons = $box.find('.buttons .button'),
			$others = $boxs.not($box);

		if (Modernizr.csstransitions) {

			$box.addClass('open');
			$others.addClass('close');

		} else {
			
			$box.stop().animate({
				width: '40%'
			}, 200);

			$buttons.stop().animate({
				opacity: 1
			}, 200);

			$others.stop().animate({
				width: '30%'
			}, 200);

			$focus.stop().animate({
				opacity: 1
			}, 200);

		}

	});

	$boxs.on('mouseleave', function(e) {

		var $box = $(e.currentTarget),
			$focus = $box.find('.focus'),
			$text = $box.find('.text'),
			$buttons = $box.find('.buttons .button'),
			$others = $boxs.not($box);

		if (Modernizr.csstransitions) {

			$boxs.removeClass('open').removeClass('close');

		} else {
			
			$box.stop().animate({
				width: '33.4%'
			}, 200);

			$buttons.stop().animate({
				opacity: 0
			}, 200);

			$others.stop().animate({
				width: '33.3%'
			}, 200);

			$focus.stop().animate({
				opacity: 0
			}, 200);
			
		}

	});

});